#! /usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

set -euo pipefail

docker_image="registry.gitlab.com/morley-framework/morley/morley-debugger"

maybe_pull_image() {
    if [[ "$(docker images -q $docker_image 2> /dev/null)" == "" ||
              3600 -le $(($(date '+%s') - $(date '+%s' \
                --date="$(docker inspect -f '{{ .Created }}' $docker_image)")
              ))
        ]];
    then
        docker pull "$docker_image"
    fi
}

if ! docker -v > /dev/null 2>&1 ; then
    echo "Docker does not seem to be installed."
    exit 1
fi


# MORLEY_DEBUGGER_IMAGE env variable can be used to provide custom image for this script.
# This functionality is mostly used for testing this script.
if [[ -z "${MORLEY_DEBUGGER_IMAGE-}" ]];
then
    maybe_pull_image
else
    docker_image="$MORLEY_DEBUGGER_IMAGE"
fi


if [[ "$#" -eq 0 ]];
then
    docker run --rm "$docker_image" morley-debugger-console --help
    exit 1
fi

typeset -a args;

subcommand="$1"
shift
args+=("$subcommand")
while true;
do
  arg="${1-}"
  if [[ -z "$arg" ]];
  then
      break
  fi
  case $arg in
    --contract )
        contract_filepath="$2"
        contract_bn=$(basename "$contract_filepath")
        args+=("--contract" "/$contract_bn")
        shift 2
        ;;
    * )
        args+=("$arg")
        shift
  esac
done

if [[ -z ${contract_filepath-} ]];
then
    docker run --rm -i "$docker_image" morley-debugger-console "${args[@]}"
else
    docker run --rm -v "$(realpath "$contract_filepath"):/$contract_bn" -i "$docker_image" morley-debugger-console "${args[@]}"
fi
