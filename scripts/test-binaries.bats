#!/usr/bin/env bats

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

setup () {
  bin_dir="tmp"
}

@test "Morley multisig registry can print Multisig contracts" {
  $bin_dir/morley-multisig -- print -n Specialized
  $bin_dir/morley-multisig -- print -n Generic
}
