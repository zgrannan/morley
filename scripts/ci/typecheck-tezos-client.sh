#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# This script checks that all contracts in `contracts/` directory
# are correctly classified as well/ill-typed using tezos-client.
# As a first  argument it accepts path to `morley` executable.
# This script expects 'tezos-client' to be in PATH.
set -euo pipefail

morley=$1

well_typed_by_extension () {
    well_typed_contracts=()
    while IFS= read -r -d $'\0'; do
        well_typed_contracts+=("$REPLY")
    done < <(find ./ -path ./examples -prune -o \
      -path ./contracts/ill-typed -prune -o \
      -path ./contracts/unparsable -prune -o \
      -path ./code/indigo/test/contracts/golden -prune -o\
      -path ./contracts/tezos_examples/ill_typed -prune -o \
      -path ./contracts/tezos_examples/entrypoints -prune -o -name "$1" -print0)
    # ^ morley doesn't fully support entrypoints for now, should be resolved in
    # https://gitlab.com/morley-framework/morley/issues/35
    # see https://gitlab.com/morley-framework/morley/-/issues/212 about
    # ignoring contracts in ./code/indigo/test/contracts/golden
}

ill_typed_by_extension () {
    ill_typed_contracts=()
    while IFS= read -r -d $'\0'; do
        ill_typed_contracts+=("$REPLY")
    done < <(find ./contracts/ill-typed ./contracts/unparsable \
      ./contracts/tezos_examples/ill_typed -name "$1" -print0)
}

export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER=Y
set -e
well_typed_by_extension "*.tz"
for f in "${well_typed_contracts[@]}"; do
    echo "$f"
    tezos-client --mode mockup typecheck script "$f"
    "$morley" print --contract "$f" -o tmp.tz
    tezos-client --mode mockup typecheck script tmp.tz
done
well_typed_by_extension "*.mtz"
for f in "${well_typed_contracts[@]}"; do
    echo "$f"
    "$morley" print --contract "$f" -o tmp.tz
    tezos-client --mode mockup typecheck script tmp.tz
done
ill_typed_by_extension "*.tz"
set +e
well_typed_bug_contract="./contracts/ill-typed/annotation_mismatch_iter.tz"
for f in "${ill_typed_contracts[@]}"; do
    echo "$f"
      if [[ $f != "$well_typed_bug_contract" ]]; then
          if tezos-client --mode mockup typecheck script "$f"; then
              echo "$f treated as well-typed by tezos-client"
              exit 1
          fi
          "$morley" print --contract "$f" -o tmp.tz
          if tezos-client --mode mockup typecheck script tmp.tz; then
              echo "$f treated as well-typed by tezos-client after \"morley print --contract $f\""
              exit 1
          fi
      fi
done
# It doesn't make much sense to check ill-typed `*.mtz` contracts using `tezos-client`
# due to the fact, that type errors can be caused by the parts that are stripped by
# `morley print`.
