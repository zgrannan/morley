#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# Publish 'indigo-lang.gitlab.io' website by triggering pipeline for
# the corresponding project and waiting for it to finish.
#
# Expected variables:
# - $CI_JOB_TOKEN -- gitlab token (has the same access as the person who triggered the upstream pipeline)
# - $DOWNSTREAM_PROJECT_ID -- id of 'indigo-lang.gitlab.io' project
# - $DOWNSTREAM_BRANCH -- which downstream branch pipeline is triggered for
# - $ARTIFACTS_URL -- url where the downstream job can fetch artifacts archive for the website
# - $ARTIFACTS_PATH -- path to the website contents within the artifacts archive

set -euo pipefail

mkdir -p ./tmp
TOKEN_FILE=./tmp/token
builtin printf "%s" "$CI_JOB_TOKEN" > "$TOKEN_FILE"

echo "Triggering downstream pipeline"
PIPELINE_JSON=$(
  curl --fail --silent --show-error --request POST \
    --form "token=<$TOKEN_FILE" \
    --form "ref=$DOWNSTREAM_BRANCH" \
    --form "variables[ARTIFACTS_URL]=$ARTIFACTS_URL" \
    --form "variables[ARTIFACTS_PATH]=$ARTIFACTS_PATH" \
    "https://gitlab.com/api/v4/projects/$DOWNSTREAM_PROJECT_ID/trigger/pipeline"
)
DOWNSTREAM_PIPELINE_ID=$(jq '.id' <<< "$PIPELINE_JSON")
DOWNSTREAM_PIPELINE_URL=$(jq --raw-output '.web_url' <<< "$PIPELINE_JSON")

echo "Triggered downstream pipeline: $DOWNSTREAM_PIPELINE_URL"

# wait for downstream pipeline to finish
WAITED=0
while true; do
  # get downstream pipeline status
  PIPELINE_JSON=$(
    curl --location --fail --silent --show-error \
      "https://gitlab.com/api/v4/projects/$DOWNSTREAM_PROJECT_ID/pipelines/$DOWNSTREAM_PIPELINE_ID"
  )
  PIPELINE_STATUS=$(jq --raw-output '.status' <<< "$PIPELINE_JSON")

  case "$PIPELINE_STATUS" in
    "created" | "pending" | "running")
      echo "(${WAITED}s) Waiting for downstream pipeline to finish, current status: $PIPELINE_STATUS"
      sleep 10
      WAITED=$((WAITED + 10))
      ;;
    "success") echo "Downstream pipeline succeeded" && exit 0 ;;
    "failed") echo "Error: downstream pipeline failed" && exit 1 ;;
    "canceled") echo "Error: downstream pipeline cancelled" && exit 1 ;;
    *) echo "Error: unexpected pipeline status: $PIPELINE_STATUS" && exit 1 ;;
  esac
done
