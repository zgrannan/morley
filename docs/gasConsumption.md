<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Types of costs

To understand Tezos gas model, it is important to grasp the lifecycle of different types of data in Tezos.

Internally, Tezos stores and transmits all values (be it toplevel code, lambdas, types or data) as byte sequences.
The byte sequences can be [_deserialized_](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_repr.ml#L161) into an intermediate untyped [Micheline representation](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/lib_micheline/micheline.ml#L28), where every value can be:
- integer
- string
- byte sequence
- primitive (e.g., `DUP` or `nat`), possibly with annotations and arguments
- sequence of values

This intermediate representation is then _parsed_ by a function defined in the current protocol into a protocol-specific _typed_ representation (see `parse_*` [functions in Carthage](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_ir_translator.ml)).
The typed representation can then be interpreted (if it is a piece of code), passed as arguments to the interpreter, etc.

At the end of the transaction or upon invokation of the `PACK` instruction the data gets [_unparsed_](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_ir_translator.ml), i.e.
converted from the typed representation to a Micheline expression, and [_serialized_](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_repr.ml#L163) (converted from the Micheline representation to a byte sequence).
Different types can be [compared for equivalence](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_ir_translator.ml#L1093) during parsing, interpretation and unparsing.

Additionally, reading and writing big map values, contract code and storage cost gas as well – these are reading and writing costs.


There are eight types of costs:
1. Reading cost
2. Deserialization cost
3. Parsing cost
4. Type comparison cost
5. Interpreter cost
6. Unparsing cost
7. Serialization cost
8. Writng cost

Each of these costs is [internally represented](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/gas_limit_repr.ml#L30) as a record with the following numeric fields:
```
{ allocations
, steps
, reads
, writes
, bytes_read
, bytes_written
}
```

To obtain the cost in gas, the vector cost is [multiplied](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/gas_limit_repr.ml#L112) (using the scalar product) by the following weights:
```
allocation_weight = 2
step_weight = 1
read_base_weight = 100
write_base_weight = 160
byte_read_weight = 10
byte_written_weight = 15
```

Note that the gas values you see are in fact shifted 7 bits right (using `rescale`), so the _internal gas_ is actually `visible_gas * 128`.


# Terminology

Tezos uses some terms inconsistently, both in its source code and documentation.
This may be misleading for readers, so here I'll introduce a terminology that I will follow throughout the document, although some terms are not quite the same as Tezos uses internally.

<details>
<summary>Click to expand the definitions</summary>

* `Database` – an internal shared key-value storage that stores contract code, balances, delegates, etc. Tezos code refers to both "contract storage" and "database" as `storage`, which, while semantically correct, may cause confusion.
* `Storage` – some data associated with a particular contract.
* `Contract` – an originated account (Tezos uses the term `contract` for both originated and implicit accounts, here I will only refer to originated accounts as contracts).
* `Contract toplevel code` – the code of a contract _including_ the types of storage and parameter.
* `Contract code` – the code of a contract _**not** including_ the types of storage and parameter.
* `Reading` – a process of accessing the database to get some data or determine if such data exists.
* `Deserialization` – converting a value from a byte string representation to Micheline expression.
* `Parsing` – converting a Micheline expression to some typed `data`, `type` or a sequence of instructions.
* `Type comparison` – checking whether some type is equal to another type. I deliberately avoid the term ‘typechecking’ here because Tezos treats parsing as a part of typechecking.
* `Interpretation` – a process of running a code to determine its output.
* `Unparsing` – converting a value from a typed representation to a Micheline expression.
* `Serialization` – converting a Micheline expression to a byte string representation.
* `Writing` – putting some data to the database.
</details>

The term _typechecking_ is often used outside Tezos source code.
Typechecking usually means "verifying whether the expected types match the actual one".
In Tezos, typechecking in this sense may be performed at different phases:
1. During value parsing. When some value is parsed, the expected type is known in advance. There is usually no type comparison at this stage: the value either can be parsed as the value of the expected type (in this case parsing is successful), or in can not – in this case parsing fails.
2. During code parsing. The difference between code parsing and value parsing is that sometimes the parser needs to _compare_ the types against the ones that make sense for a particular instruction, i.e.:
   - Conditional statements such as `IF`, `IF_LEFT`, `IF_CONS` require both branches to have the correct initial and returning stack type.
   - Instructions like `ITER`, `LOOP`, `LOOP_LEFT`, `MAP` need to check whether the code block passed as an argument indeed operates on the values of the expected type and returns the correct stack type.
   - `MEM`, `GET`, `UPDATE`, `CONS` need to make sure that the container type matches the value type we're trying to operate on.
   - `COMPARE` needs the types of two top stack entries to be the same.
   - `EXEC`, `APPLY`, `CAST`, `TRANSFER_TOKENS`, `CREATE_CONTRACT`, etc. also perform type equality checks during parsing.
   - Counterintuitively, stack manipulation instructions such as `DIP` (`DIPN`), `DIG`, `DUG` _do not_ consume extra gas for checking the stack type equality.

Thus, the term _typechecking_ may be ambiguous: it can either refer to _type comparison_ (which may be performed during parsing), or the whole parsing process, as this is a conversion from an untyped representation to a typed representation.

Usually, by typechecking in the context of Tezos people mean _parsing_ (including _type parsing,_ which is intuitively far from typechecking in the common sense). I will refrain from using the word "typechecking" further in this document. Rather, I will use explicit "type comparison" and "parsing" terms instead.

# Contents list lifecycle
In Tezos, a manager (an entity who initiates the state change) signs a "contents list" — a list of operations she wants to perform.

There is a hard limit for each operation from the content list. [In Carthage](https://gitlab.com/nomadic-labs/tezos/-/merge_requests/117/diffs), each operation can consume no more than `1_040_000` gas, _including_ the internal operations that may be emitted. The whole contents list has no defined upper limit but since all the transactions from it must belong to the same _block,_ the block gas limit applies. In Carthage, the block gas limit is `10_400_000` gas.

Each operation in the list is one of the following:
1. Reveal
2. Transaction
3. Origination
4. Delegation

The operations of such "content list" are applied in sequence.
If one of the operations fails, the others are still executed (unlike internal operations chain).

The following happens for any operations (regardless of their type and whether they are internal or external)
1. The sender is checked for existence. This is free.
2. The cost of manager operation (**10 000** gas) is deduced.

The latter steps depend on the operation type.
We will focus on `Origination` and `Transaction` operations only.

## Origination

Originations can not execute any code or emit internal operations.

1. `toplevel code` and initial `storage` are [deserialized](#deserialization-gas-consumption).
2. The announced `parameter type` gets parsed (see [Type parsing](#type-parsing-parse_ty)).
3. The `storage type` gets parsed (see [Type parsing](#type-parsing-parse_ty)).
4. The `storage` itself gets parsed (see [Data parsing](#data-parsing-parse_data)).
5. The `code` is parsed (see [Code parsing](#code-parsing-parse_returning)).
6. The big maps are collected from `storage`.
7. The big map diff is extracted.
8. The new storage (with the big map extracted) is unparsed (see [Data unparsing](#unparsing-gas-consumption-unparse_data)).
9. The unparsed storage is [serialized](#serialization-gas-consumption).
10. Both `toplevel code` and serialized `storage` are [written to the database](#database-access-gas-consumption).


## Transaction

1. Transaction changes the balances of accounts.
First, explicit balance changes are applied (e.g., if `A` transfers 1 tez to `B`, 1 tez is debited from `A`, and 1 tez is credited to `B`).
During this step,
    * if the transaction value is zero, the implementation spends `100` gas for [reading the database](#database-access-gas-consumption) to check whether `B` is an originated contract,
    * if the value is non-zero, the check is not performed.
2. The node [reads](#database-access-gas-consumption) `toplevel code` and `storage` from the database. The cost breakdown is: `200 + 10 * code size` for reading the code, `200 + 10 * storage size` for reading the storage (the sizes are in bytes).
3. `toplevel code`, `parameter` and `storage` are deserialized. Some of these values are deserialized several times throughout the code but since deserialization is lazy, it is expected that the deserialization gas cost is consumed only once.
4. The script execution starts with the steps equivalent to those in origination:
    1. The announced `parameter type` gets parsed (see [Type parsing](#type-parsing-parse_ty)).
    2. The `storage type` gets parsed (see [Type parsing](#type-parsing-parse_ty)).
    3. The `storage` itself gets parsed (see [Data parsing](#data-parsing-parse_data)).
    4. The `code` is parsed (see [Code parsing](#code-parsing-parse_returning)).

4. After these, additional steps are performed:
    1. The `parameter` value is [parsed](#data-parsing-parse_data).
    2. The big maps are collected from the `parameter`.
    3. The big maps are collected from `storage`.
    4. The contract is [interpreted](#interpreter-gas-consumption).
    5. The big map diff is extracted.
    6. The storage is [unparsed](#unparsing-gas-consumption-unparse_data).
    7. The storage is [serialized](#serialization-gas-consumption).
    8. The updated storage [written to the database](#database-access-gas-consumption).
    9. The big map diff is applied.

5. The emitted internal operations are added to the end of the operations queue.
Internal operations do not differ (in terms of gas costs) from manager operations.
The internal operations are applied _before_ the consecutive operations in the contents list.
If internal operations are nested, new operations are added to the topmost queue, i.e., there is one internal operations queue for each manager-signed operation from the contents list.

# Database access gas consumption

Accessing the internal key-value database is expensive.
The gas cost is deduced for:
* Checking for existence of a value
* Reading a value
* Writing a value

Checking for existence costs `{ reads: scale 1 }`, which is `scale 100` of _internal_ gas (after [multiplying by weights](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/gas_limit_repr.ml#L112)), or `100` _visible_ gas (after rescaling).

Reading usually costs
```
{ reads: scale 2
, bytes_read: scale $ <length of the value in bytes>
}
```

Reads is `2` because we usually [check whether the value exists](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/storage_functors.ml#L1025) before trying to access it.

The reading gas consumption boils down to `scale $ 200 + 10 * bytes_read` per one read operation.

The cost of writing is defined as follows:
```
{ writes: 1
, bytes_written: scale $ <length of the value in bytes>
}
```

**NOTE: the _writes_ cost component is NOT scaled here.
It is [a bug](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/gas_limit_repr.ml#L212) in the current Tezos implementation that is being fixed now.**

Consequently, the writing cost (after [multiplying by weights](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/gas_limit_repr.ml#L112)) is essentially `160 + scale $ 15 * bytes_written`.

After the bug is fixed, it would probably become `scale $ 160 + 15 * bytes_written`.

Database reading and writing costs are applied to:
1. Contract storage
2. Contract code
3. Big maps

Accessing other data (like balances of accounts, delegates, etc.) is free.

The costs are defined in [`carbonated_map`](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/storage_functors.ml#L957) and [`indexed_carbonated_storage`](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/storage_functors.ml#L401).

# Deserialization gas consumption

Deserialization is a process of converting the data from `bytes` to a Micheline node — an internal representation of both code and data.

Unlike other costs, deserialization cost is applied lazily, meaning that if some value has already been deserialized, the cost for consecutive deserializations is zero.

Deserialization costs depend on the number of _blocks_ and _words_ being deserialized.
The number of blocks and words depends on the Micheline expression being deserialized and [is defined](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_repr.ml#L101) as follows:
* For `integer` nodes: `(1, (1 + words_ n))`, where `n` is the number being deserialized
* For primitives:
    - without annotations: `(1 + n_args, 2 + (2 * n_args))` + sum of arg sizes
    - with annotations: `(2 + n_args, 4 + (2 * n_args) + ((annots_length + 7) / 8))` + sum of arg sizes (`annots_length` is the sum of annotations lengths)
* For sequences: `(1 + n_args, 2 + (2 * n_args)` + sum of entry sizes
* For strings: `(1, 1 + ((len + 7) / 8))`
* For bytestrings: `(2, 1 + ((len + 7) / 8) + 12)`

The number of blocks and words is then transformed into the vector gas cost using the following [formula](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_repr.ml#L131):

```
{ allocations: scale (blocks + words + 1)
, steps: scale blocks
}
```

(Which boils down to `scale $ 3 * blocks + 2 * words + 2` after [multiplying by weights](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/gas_limit_repr.ml#L112))

# Parsing gas consumption

Parsing in Tezos terminology means translating from an intermediate Micheline representation to a protocol-specific typed representation.
The parsing logic is defined in [`Script_ir_translator` module](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_ir_translator.ml).

Parsing consists of the following parts:
* Type parsing (`parse_ty`) — converts untyped nodes to valid type expressions.
* Data parsing (`parse_data`) — matching untyped data against the type expressions.
* Code parsing (`parse_returning`) — matching the "before" and "after" stack types against the known types.

Although it's not a type of _parsing,_ type equivalence checks (`ty_eq`) occur mostly at parsing phase, and the logic is defined as a part of `Script_ir_translator`, so it's reasonable to treat type equivalence checks as the part of parsing as well.

## Type parsing (`parse_ty`)
Type parsing converts a Micheline node (Tezos stores everything as Micheline nodes internally) into some sane type, or reports an error in case the resulting type does not make sense due to some reason.

The cost of type parsing does not depend on any environment, and is computed using the following recursive formula:

```
G_ty T0 = cycle + type_ 0
G_ty (T1 a) = cycle + type_ 1 + G_ty a
G_ty (T2 a b) = cycle + type_ 2 + G_ty a + G_ty b
```

Here:
  * `T0` is a plain Michelson type (e.g., `address` or `int`),
  * `T1 a` is a type with one type parameter (e.g., `option a` or `contract p`),
  * `T2 a b` is a type with two type parameters (e.g., `pair a b` or `union a b`).

## Type comparison (`ty_eq`)
Each time Tezos compares types for equality, some amount of gas is burned.
Most of such checks occur during the code parsing phase but may happen during contract execution and unparsing as well.

The logic resembles [type parsing](#type-parsing-parse_ty), except that `type_ N` cost is deduced twice:

```
G_ty_eq T0 T0 = cycle + type_ 0
G_ty_eq (T1 a1) (T1 a2) = cycle + type_ 2 + (G_ty_eq a1 a2)
G_ty_eq (T2 a1 b1) (T2 a2 b2) = cycle + type_ 4 + (G_ty_eq a1 a2) + (G_ty_eq b1 b2)
```

In case of seeing different types on some level of recursion, the parser serializes the types for error, i.e.:

```
G_ty_eq t1 t2 = cycle + unparse_ty t1 + unparse_ty t2
```

## Data parsing (`parse_data`)

Data parsing converts a Micheline node into the value of some known type.
For most of the types, one can find the cost of data parsing using the following intuition:
> If `T` is a non-parametrized type, the cost for parsing data of type `T` is defined as `Typecheck_costs.T`.

The cost of parsing a `string` and `bytes` depends on the length of the corresponding string and byte sequence: `Typecheck_costs.string (length v)`.

The cost of parsing a `timestamp` depends on whether it is stored as an optimized (int-like) value, or in the human-readable (string-like) form.
The corresponding functions `Typecheck_costs.z v` and `Typecheck_costs.string_timestamp` are used.

Parsing cost for int-like values (`int`, `nat`, `mutez`, optimized `timestamp`) depends on the byte length of the value, and is defined as `Typecheck_costs.z v`.

For `address`, the cost of `Typecheck_costs.contract` is used (parsing a `contract` is, in turn, more expensive, see later).
Parsing an `operation` is disallowed.

Containers:
1. If `T` is a `list`, the cost for parsing is a sum (for all the elements of the list):
  `Typecheck_costs.list_element` plus cost of `parse_data element`.

2. If `T` is a `map` or a `big_map` passed by value, the cost for parsing is a sum (for all the elements of the container):
  `Typecheck_costs.map_element elements_count` plus cost of `parse_data k` plus cost of `parse_data v`.
  * Note that the `map_element` cost depends on the size of the container.

3. If `T` is a `set`, the cost for parsing is a sum (for all the elements of the set):
  `Typecheck_costs.set_element elements_count` plus cost of `parse_data element`.
  * Note that the `set_element` cost depends on the size of the set.
  * Due to some reason, `set` additionally consumes `Michelson_v1_gas.Cost_of.Legacy.set_update` gas during data parsing.

Other parametrized types:
* `pair ta tb` — `Typecheck_costs.pair` plus cost of `parse_data a` plus cost of `parse_data b`
* `union ta tb` — `Typecheck_costs.union` plus cost of `parse_data a` or `parse_data b` depending on the actual content.
* `option tv` — `Typecheck_costs.none` or `Typecheck_costs.some` plus `parse_data v` depending on the actual content.
* `lambda` — `Typecheck_costs.lambda` plus the cost of [code parsing](#code-parsing-parse_returning)

## Code parsing (`parse_returning`)
The return type of the code is computed by iterating over Micheline sequence of instructions (which is how code is internally represented).
Each instruction is applied to the current stack type, the resulting stack type is checked against the expected value.
Additional type equality checks may occur in instructions like `IF`, `LOOP`, etc., in case of type mismatch the parsing halts.

The cost of typecheck for most of the instructions is equal to: `Typecheck_costs.cycle + Typecheck_costs.instr instr` (where `instr` is the id of the instruction, e.g., `I_IMPLICIT_ACCOUNT`).

For some of the instructions, however, additional costs are applied:
1. `PUSH` — type parsing and data parsing of the argument.
2. `IF`, `IF_NONE`, `IF_CONS`, `IF_LEFT` — typechecking both branches, comparing and merging types.
3. `LEFT`, `RIGHT`, `NIL`, `EMPTY_SET`, `EMPTY_MAP`, `EMPTY_BIG_MAP`, `UNPACK`, `CONTRACT` — argument type parsing.
4. `CONS`, `MEM`, `GET`, `UPDATE`, `EXEC`, `APPLY`, `COMPARE`, `TRANSFER_TOKENS` — comparing and merging types.
5. `LAMBDA` — type parsing of the argument and the return value, code typechecking (via `parse_returning`).
6. `CREATE_CONTRACT` — type parsing of the parameter, code typechecking (via `parse_returning`), comparing and merging parameter, code and storage types.
7. `DIP` — typechecking the argument.
8. `CAST` — type parsing, comparing and merging types.


# Interpreter gas consumption

Interpreter gas consumption is significantly lower than the cost of other gas consuming operations.

At the beginning of each execution step, `Interp_costs.cycle` is deduced (currently defined as `10` internal gas, or `rescale 10 = 10/128` visible gas).
Additional costs depend on the instruction being executed; you can see the gas consumption logic for each operation [in the interpreter](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_interpreter.ml#L294).
The constants referenced there (such as `Interp_costs.cycle`) are defined in [michelson_v1_gas](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/michelson_v1_gas.ml#L99).

Although the interpretation is cheap, there are some instructions that are expensive:
1. `CONTRACT` costs `10 000` visible gas.
Additionally, upon invokation of the `CONTRACT` instruction, the following [steps](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_ir_translator.ml#L5010) happen:
   * In case the target address is an originated contract:
      * [`Typecheck_costs.get_script`](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/michelson_v1_gas.ml#L403) is deduced.
      * The target contract toplevel code is [read](#database-access-gas-consumption) from the database.
      * The toplevel code is [deserialized](#deserialization-gas-consumption).
      * The parameter type of the target contract is [parsed](#type-parsing-parse_ty).
      * The expected parameter type is [compared for equivalence](#type-comparison-ty_eq) with different entrypoints of the target contract (specified, default, root – until the match is found).
   * In case the target address is an implicit account:
      * The parameter type is [compared](#type-comparison-ty_eq) with `unit`.
   * In case the target contract does not exist:
      * Overapproximation of the typechecking cost is deduced by [comparing](#type-comparison-ty_eq) the expected parameter type with itself.
2. `PACK` instruction:
   * Value [unparsing](#unparsing-gas-consumption-unparse_data) and [serialization](#serialization-gas-consumption).
3. `UNPACK` instruction:
   * Value [deserialization](#deserialization-gas-consumption) and [parsing](#data-parsing-parse_data) in case of successful unpacking.
   * [`Interp_costs.unpack_failed`](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/michelson_v1_gas.ml#L280) in case of failure.
4. Big map `GET`:
   * If the value is **not** in the diff yet (i.e., it has not been updated in the current transaction):
      * The cost of [diff access](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/michelson_v1_gas.ml#L153).
      * The cost of [hashing](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_ir_translator.ml#L5746) the key (i.e., [unparsing](#unparsing-gas-consumption-unparse_data) + [serialization](#serialization-gas-consumption) + [`Michelson_v1_gas.Cost_of.Legacy.hash`](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/michelson_v1_gas.ml#L91))
      * The cost of [reading](#database-access-gas-consumption) the value from the database.
      * [Deserialization](#deserialization-gas-consumption) cost.
      * [Parsing](#data-parsing-parse_data) cost.
   * If the value is in the diff (i.e., it has been updated in the current transaction):
      * The cost of [diff access](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/michelson_v1_gas.ml#L153).
5. Big map `MEM` – same as big map `GET`, except for:
   * Value reading costs fixed `100` visible gas.
   * Value deserialization and parsing costs are not deduced in `MEM`.

Big map updates are applied _after_ the transaction finishes its execution, so the consecutive writes to the same key are cheap (same as updating a regular map).
Applying a diff is described in a [separate section](#big-map-diff).

# Unparsing gas consumption (`unparse_data`)

Data unparsing converts some typed data into an untyped Micheline node representation (which can later be serialized).
Unparsing costs for most of the data types are defined as `Unparse_costs.cycle + Unparse_costs.T`, where `T` is the type.

For parametrized types, the inner values are also unparsed, e.g., for `pair ta tb`, the cost of unparsing `a` and `b` is deduced.

Unparsing a `big_map` is free if it resides in storage, and equal to unparsing a `map` if it is transferred to another contract.

Unparsing a `lambda` invokes a function `unparse_code`, which has a "non-perfect gas accounting" because it was supposed to be "only called by RPCs".
However, lambdas are unparsed every time the storage is saved, so the comment is hardly true.
The "non-perfect gas accouning" is described by the following recursive definition:

1. `PUSH` — type parsing, data parsing, data unparsing of the argument plus `Unparse_costs.prim_cost 2 annot`.
2. Some other (single) instruction — the cost of inner instructions plus `Unparse_costs.prim_cost 3 annot`.
3. The sequences of instructions — the sum of the instruction costs plus `Unparse_costs.seq_cost (List.length items)`.


# Serialization gas consumption

Serialization gas costs consist of [two components](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_repr.ml#L195):
1. Traversal cost
2. Byte serialization cost

Traversal cost [is defined](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_repr.ml#L127) in terms of blocks and words (much like in [deserialization](#deserialization-gas-consumption)):
```
{ step: scale blocks }
```

(Which boils down to `scale blocks` after [multiplying by weights](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/gas_limit_repr.ml#L112))

Byte serialization cost [is calculated](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_repr.ml#L163) based on the lenght of the resulting byte sequence:

```
{ alloc: 12 + ((len + 7) / 8) }
```

(Which boils down to `scale .
(*2) $ 12 + ((len + 7) / 8)` after [multiplying by weights](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/gas_limit_repr.ml#L112))


# Big map diff

When some contract is originated, or when a transaction finishes its execution, there is a _big map diff._
When the diff is applied, the folowing costs are deduced:
1. For each big map in the diff:
   * [`Michelson_v1_gas.Cost_of.Legacy.map_to_list`](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/michelson_v1_gas.ml#L83).
2. For each key-value pair from the diff:
   * [`Typecheck_costs.cycle`](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/michelson_v1_gas.ml#L374)
   * The cost of [hashing](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_ir_translator.ml#L5746) the key (i.e., [unparsing](#unparsing-gas-consumption-unparse_data) + [serialization](#serialization-gas-consumption) + [`Michelson_v1_gas.Cost_of.Legacy.hash`](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/michelson_v1_gas.ml#L91))
   * The cost of [unparsing](#unparsing-gas-consumption-unparse_data) the key (yes, [once again](https://gitlab.com/tezos/tezos/-/blob/4c31ae32bcb17a784e693f13cb6dc1842b3a9716/src/proto_006_PsCARTHA/lib_protocol/script_ir_translator.ml#L5849)) and the value.
   * The cost of serializing the value.
3. If the diff is then written to the database, [writing costs](#database-access-gas-consumption) apply.


# Example cost breakdown

Let's take the simplest unit contract:

```
parameter unit;
storage unit;
code { CDR; NIL operation; PAIR; };
```

It can be represented as a Micheline expression:
```
Sequence (3) {
  Prim "parameter" [Prim "unit"],
  Prim "storage" [Prim "unit"],
  Prim "code" [
    Sequence (3) {
      Prim "CDR" [],
      Prim "NIL" [Prim "operation" []],
      Prim "PAIR" []
    }
  ]
}
```

Such Micheline expression can be _serialized_ into a byte sequence of length 28: `02000000170500036c0501036c050202000000080317053d036d0342`.

The initial storage of this contract – `Unit` – can be unparsed into a Micheline expression `Prim "unit" []`, and _serialized_ into a byte sequence of length `2` (`0x030b`).

When the contract is originated, the following happens:

1. `toplevel code` and initial `storage` are [deserialized](#deserialization-gas-consumption).
   * The storage has 1 _block_ and 2 _words,_ thus the deserialization cost is **9**.
   * The toplevel code has 21 blocks and 42 words, thus deserialization cost is **149**.
2. The announced `parameter type` gets parsed (see [Type parsing](#type-parsing-parse_ty)).
   * `Prim "unit" []` becomes `unit`, and we pay **9** gas for this.
3. The `storage type` gets parsed (see [Type parsing](#type-parsing-parse_ty)).
   * `Prim "unit" []` becomes `unit`, and we pay **9** gas for this.
4. The `storage` itself gets parsed (see [Data parsing](#data-parsing-parse_data)).
   * `Prim "Unit" []` becomes `Unit`, and we pay **1** gas for this (yes, data parsing may be cheaper than type parsing).
5. The `code` is parsed (see [Code parsing](#code-parsing-parse_returning)).
   * Code parsing consumes **127** gas.
6. The big maps are collected from `storage`.
   * This costs 1 visible gas per big map but here we do not pay anything.
7. The big map diff is extracted.
   * Since we're not populating the big map, this step is free.
8. The new storage (with the big map extracted) is unparsed (see [Data unparsing](#unparsing-gas-consumption-unparse_data)).
   * However, we _do_ pay for unparsing the exact same storage: unparsing a `Unit` costs **10** gas.
9. The unparsed storage is [serialized](#serialization-gas-consumption).
   * We're _traversing_ 1 block and _allocating_ an array of 2 bytes.
Thus, we pay **27** (1 + 26) gas for serialization.
10. Both `toplevel code` and serialized `storage` are [written to the database](#database-access-gas-consumption).
   * Each database write costs **1.25** gas (it should have been 160 but due to the bug in the implementation the value is not scaled, so the actual gas for write op is `160 / 128 = 1.25`).
   * There are two writes: the code (28 bytes) and the initial storage (2 bytes).
Thus, writing costs `(1.25 + 15 * 18) + (1.25 + 15 * 2)`.
The resulting gas consumption for writing is **452** gas.

There calculation leaves 118 gas unaccounted.
However, this value does not seem to increase with more complicated contracts, so figuring out the precise model is not our top priority.

The transaction breakdown is the following (if the transaction value is zero):

1. The implementation spends `100` gas for [reading the database](#database-access-gas-consumption) to check whether the callee is an originated contract.
2. The node [reads](#database-access-gas-consumption) `toplevel code` and `storage` from the database.
   * `200 + 10 * 28` for reading the code, `200 + 10 * 2`, **700** gas total.
3. `toplevel code`, `parameter` and `storage` are deserialized.
   * Contrary to origination, here we need to deserialize the _parameter_ as well, so we pay **9** gas more, **167** gas total for this step.
4. Steps equivalent to (2) – (5) from origination breakdown are performed.
   * This consumes **304** gas.
5. The `parameter` value is [parsed](#data-parsing-parse_data).
   * Parsing of `Unit` costs **1** gas.
2. The big maps are collected from the `parameter`.
   * We're not transferring any big maps here, so this is free.
3. The big maps are collected from `storage`.
   * We do not have any big maps in storage, so this is free as well.
4. The contract is [interpreted](#interpreter-gas-consumption).
   * This costs **1** gas.
5. The big map diff is extracted.
   * We're not updating any big maps, so we don't pay for this.
6. The storage is [unparsed](#unparsing-gas-consumption-unparse_data).
   * Although we haven't updated the storage, we **unparse it nevertheless** and pay **10** gas for this (same as in origination).
7. The storage is [serialized](#serialization-gas-consumption).
   * Although we haven't updated the storage, **27** gas is deduced for storage serialization.
8. The updated storage [written to the database](#database-access-gas-consumption).
   * Although we haven't updated the storage, we still pay **31** (`1.25 + 15 * 2`) gas for writing it to the database.
9. The big map diff is applied.
   * Since our diff is empty, we don't pay anything here.
