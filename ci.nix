# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

rec {
  sources = import ./nix/sources.nix;
  pkgs = import ./nix/nixpkgs-with-haskell-nix.nix;
  xrefcheck = import sources.xrefcheck;
  weeder-hacks = import sources.haskell-nix-weeder { inherit pkgs; };
  tezos-client = (import "${sources.tezos-packaging}/pkgs.nix" {}).ocamlPackages.tezos-client;

  patched-tezos-node = (import ./local-chain).patched-tezos-node;

  haskell-nix = pkgs.haskell-nix;

  mkdocs = pkgs.callPackage ./nix/mkdocs.nix { };

  # all local packages and their subdirectories
  # we need to know subdirectories for weeder and for cabal check
  local-packages = [
    { name = "indigo"; subdirectory = "code/indigo"; }
    { name = "indigo-tutorial"; subdirectory = "code/indigo/tutorial"; }
    { name = "lorentz"; subdirectory = "code/lorentz"; }
    { name = "morley"; subdirectory = "code/morley"; }
    { name = "morley-client"; subdirectory = "code/morley-client"; }
    { name = "cleveland"; subdirectory = "code/cleveland"; }
    { name = "morley-multisig"; subdirectory = "code/morley-multisig"; }
    { name = "morley-prelude"; subdirectory = "code/morley-prelude"; }
    { name = "morley-upgradeable"; subdirectory = "code/morley-upgradeable"; }
    { name = "morley-debugger"; subdirectory = "code/morley-debugger"; }
    { name = "tasty-hunit-compat"; subdirectory = "code/tasty-hunit-compat"; }
  ];

  # names of all local packages
  local-packages-names = map (p: p.name) local-packages;

  # source with gitignored files filtered out
  projectSrc = haskell-nix.haskellLib.cleanGit {
    name = "morley";
    src = ./.;
  };

  # haskell.nix package set
  # parameters:
  # - release -- 'true' for master and producion branches builds, 'false' for all other builds.
  #   This flag basically disables weeder related files production, haddock and enables stripping
  # - commitSha, commitDate -- git revision info used during morley-multisig compilation
  # - optimize -- 'true' to enable '-O1' ghc flag, we intend to use it only in production branch
  hs-pkgs = { release, optimize ? false, commitSha ? null, commitDate ? null }: haskell-nix.stackProject {
    src = projectSrc;

    # use .cabal files for building because:
    # 1. haskell.nix fails to work for package.yaml with includes from the parent directory
    # 2. with .cabal files haskell.nix filters out source files for each component, so only the changed components will rebuild
    ignorePackageYaml = true;

    modules = [
      # common options for all local packages:
      {
        packages = pkgs.lib.genAttrs local-packages-names (packageName: {
          package.ghcOptions = with pkgs.lib; concatStringsSep " " (
            # we use O1 for production binaries in order to improve their performance
            # for end-users
            [ (if optimize then "-O1" else "-O0") "-Werror"]
            # produce *.dump-hi files, required for weeder:
            ++ optionals (!release) ["-ddump-to-file" "-ddump-hi"]
          );
          dontStrip = !release;  # strip in release mode, reduces closure size
          doHaddock = !release;  # don't haddock in release mode

          # in non-release mode collect all *.dump-hi files (required for weeder)
          postInstall = if release then null else weeder-hacks.collect-dump-hi-files;
        });
      }

      {
        # don't haddock dependencies
        doHaddock = false;

        # provide commit sha and date for morley-multisig in release mode:
        packages.morley-multisig = {
          preBuild = ''
            export MORLEY_DOC_GIT_COMMIT_SHA=${if release then pkgs.lib.escapeShellArg commitSha else "UNSPECIFIED"}
            export MORLEY_DOC_GIT_COMMIT_DATE=${if release then pkgs.lib.escapeShellArg commitDate else "UNSPECIFIED"}
          '';
        };
      }
    ];
  };

  hs-pkgs-development = hs-pkgs { release = false; };

  # component set for all local packages like this:
  # { morley = { library = ...; exes = {...}; tests = {...}; ... };
  #   morley-prelude = { ... };
  #   ...
  # }
  packages = pkgs.lib.genAttrs local-packages-names (packageName: hs-pkgs-development."${packageName}".components);

  # returns a list of all components (library + exes + tests + benchmarks) for a package
  get-package-components = pkg: with pkgs.lib;
    optional (pkg ? library) pkg.library
    ++ attrValues pkg.exes
    ++ attrValues pkg.tests
    ++ attrValues pkg.benchmarks;

  # per-package list of components
  components = pkgs.lib.mapAttrs (pkgName: pkg: get-package-components pkg) packages;

  # a list of all components from all packages in the project
  all-components = with pkgs.lib; flatten (attrValues components);

  # run morley-multisig to produce contract documents
  contracts-doc = { release, commitSha ? null, commitDate ? null }@releaseArgs: pkgs.runCommand "contracts-doc" {
    buildInputs = [
      (hs-pkgs releaseArgs).morley-multisig.components.exes.morley-multisig
    ];
  } ''
    mkdir $out
    cd $out
    mkdir autodoc
    morley-multisig document --name GenericWithCustomErrors --output \
      autodoc/GenericWithCustomErrors.md
  '';

  # release version of morley executable
  morley-release = { production ? false }:
    (hs-pkgs { release = true; optimize = production; }).morley.components.exes.morley;

  # docker image for morley executable
  # 'creationDate' -- creation date to put into image metadata
  morleyDockerImage = { creationDate, tag ? null, production ? false }:
    pkgs.dockerTools.buildImage {
    name = "morley";
    contents = morley-release { inherit production; };
    created = creationDate;
    inherit tag;
  };

  morleyDebuggerDockerImage = { creationDate, tag ? null }:
    pkgs.dockerTools.buildImage {
      name = "morley-debugger";
      contents = (hs-pkgs { release = true; }).morley-debugger.components.exes.morley-debugger-console;
      created = creationDate;
      inherit tag;
    };

  # nixpkgs has weeder 2, but we use weeder 1
  weeder-legacy = pkgs.haskellPackages.callHackageDirect {
    pkg = "weeder";
    ver = "1.0.9";
    sha256 = "0gfvhw7n8g2274k74g8gnv1y19alr1yig618capiyaix6i9wnmpa";
  } {};

  # a derivation which generates a script for running weeder
  weeder-script = weeder-hacks.weeder-script {
    hs-pkgs = hs-pkgs-development;
    local-packages = local-packages;
    weeder = weeder-legacy;
  };

  # checks if all packages are appropriate for uploading to hackage
  run-cabal-check = pkgs.runCommand "cabal-check" { buildInputs = [ pkgs.cabal-install ]; } ''
    ${pkgs.lib.concatMapStringsSep "\n" ({ name, subdirectory }: ''
      echo 'Running `cabal check` for ${name}'
      cd ${projectSrc}/${subdirectory}
      cabal check
    '') local-packages}

    touch $out
  '';

  # generate Indigo's website
  indigo-website = pkgs.runCommand "indigo-website" { buildInputs = [ mkdocs ]; }
  ''
    mkdir $out
    mkdir "$out/indigo-website"
    cd ${projectSrc}/code/indigo/tutorial/
    mkdocs build -d "$out/indigo-website"
  '';

  # stack2cabal cannot be built with newer HsYAML, override HsYAML version
  # see https://gitlab.com/tseenshe/stack2cabal/-/merge_requests/18
  stack2cabal = pkgs.haskellPackages.callHackageDirect {
    pkg = "stack2cabal";
    ver = "1.0.6";
    sha256 = "1wisg3kr3a14q5ch7pqqscrdhpaqgkyylhh2vcwsxm5jdii94z0y";
  } {
    "HsYAML" = pkgs.haskell.lib.doJailbreak (pkgs.haskellPackages.callHackageDirect {
      pkg = "HsYAML";
      ver = "0.1.2.0";
      sha256 = "0wksjmd7gppgrr72fdhj552prczh88mgcmkk6d671ilvfs3hbd8s";
    } {});
  };
}
