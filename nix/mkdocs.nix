# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

# Updated version of mkdocs (and the python packages to build it), that also
# includes pymdown-extensions as well as our custom markdown extensions.

{ stdenv, lib, python38, python38Packages, fetchFromGitHub }:

let
  click = python38Packages.buildPythonPackage rec {
    pname = "click";
    version = "7.1.2";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "d2b5255c7c6349bc1bd1e59e08cd12acbbd63ce649f2588755783aa94dfb6b1a";
    };

    propagatedBuildInputs = [ ];
  };

  markup-safe = python38Packages.buildPythonPackage rec {
    pname = "MarkupSafe";
    version = "1.1.1";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "29872e92839765e546828bb7754a68c418d927cd064fd4708fab9fe9c8bb116b";
    };

    propagatedBuildInputs = [ ];
  };

  jinja2 = python38Packages.buildPythonPackage rec {
    pname = "Jinja2";
    version = "2.11.2";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "89aab215427ef59c34ad58735269eb58b1a5808103067f7bb9d5836c651b3bb0";
    };

    propagatedBuildInputs = [ markup-safe ];
  };

  tornado = python38Packages.buildPythonPackage rec {
    pname = "tornado";
    version = "6.0.4";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "0fe2d45ba43b00a41cd73f8be321a44936dc1aba233dee979f17a042b83eb6dc";
    };

    propagatedBuildInputs = [ ];

    doCheck = false;
  };

  livereload = python38Packages.buildPythonPackage rec {
    pname = "livereload";
    version = "2.6.1";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "89254f78d7529d7ea0a3417d224c34287ebfe266b05e67e51facaf82c27f0f66";
    };

    propagatedBuildInputs = [ python38Packages.six tornado ];
  };

  lunr = python38Packages.buildPythonPackage rec {
    pname = "lunr";
    version = "0.5.8";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "c4fb063b98eff775dd638b3df380008ae85e6cb1d1a24d1cd81a10ef6391c26e";
    };

    propagatedBuildInputs = with python38Packages; [ future six ];

    doCheck = false;
  };

  pygments = python38Packages.buildPythonPackage rec {
    pname = "Pygments";
    version = "2.6.1";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "647344a061c249a3b74e230c739f434d7ea4d8b1d5f3721bc0f3558049b38f44";
    };

    propagatedBuildInputs = [ ];

    doCheck = false;
  };

  markdown = python38Packages.buildPythonPackage rec {
    pname = "Markdown";
    version = "3.2.2";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "1fafe3f1ecabfb514a5285fca634a53c1b32a81cb0feb154264d55bf2ff22c17";
    };

    propagatedBuildInputs = [ ];

    checkInputs = [ python38Packages.nose pyyaml ];
  };

  pyyaml = python38Packages.buildPythonPackage rec {
    pname = "PyYAML";
    version = "5.3.1";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "b8eac752c5e14d3eca0e6dd9199cd627518cb5ec06add0de9d32baeee6fe645d";
    };

    propagatedBuildInputs = [ ];
  };

  pymdown-extensions = python38Packages.buildPythonPackage rec {
    pname = "pymdown-extensions";
    version = "7.1";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "5bf93d1ccd8281948cd7c559eb363e59b179b5373478e8a7195cf4b78e3c11b6";
    };

    propagatedBuildInputs = [ markdown ];

    doCheck = false;
  };

  include-code = python38Packages.buildPythonPackage rec {
    pname = "include_code";
    version = "0.1";
    src = ../code/indigo/tutorial/docs/markdown-ext;
    propagatedBuildInputs = [ pyyaml markdown ];
  };

in python38Packages.buildPythonApplication rec {
    pname = "mkdocs";
    version = "1.1.2";

    src = python38Packages.fetchPypi {
      inherit pname version;
      sha256 = "f0b61e5402b99d7789efa032c7a74c90a20220a9c81749da06dbfbcbd52ffb39";
    };

    propagatedBuildInputs = [
      click jinja2 livereload lunr pygments markdown pyyaml tornado

      python38Packages.setuptools python38Packages.nltk

      include-code pymdown-extensions
    ];

    doCheck = false;
  }
