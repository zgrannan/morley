<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Morley: Developer tools for the Michelson Language

[![pipeline status](https://gitlab.com/morley-framework/morley/badges/master/pipeline.svg)](https://gitlab.com/morley-framework/morley/-/commits/master)

Morley consists of a set of tools and libraries to make writing smart contracts
in Michelson pleasant and effective.

This repository consists of multiple packages for better granularity.
They are in the [code/](/code/) directory, please refer to the corresponding `README.md` files for more information.
A brief overview:
* [`morley`](/code/morley/) is where it all started.
It has data types representing everything present in Michelson, implementation of Michelson typechecker and interpreter.
It implements a dialect of Michelson that we call the Morley language.
It is a superset of vanilla Michelson.
* [`lorentz`](/code/lorentz) is a Haskel eDSL that provides a more convenient way to write Michelson contracts.
* [`indigo`](/code/indigo) is a higher level language based on top of Lorentz. It has variables and eliminates the burden of manual stack management.
* [`cleveland`](/code/cleveland) is a testing framework that allows one to write tests for
Michelson/Morley/Lorentz contracts and run them in either an emulated environment or in a real network (usually testnet).
* [`morley-client`](/code/morley-client) provides bindings to Tezos RPC and `tezos-client`.
* [`morley-prelude`](/code/morley-prelude) and [`tasty-hunit-compat`](/code/tasty-hunit-compat) are auxiliary packages that we use for internal needs.

There are other packages with smart contracts written in Lorentz and utilities for writing them.
More packages may be added later.

The name Morley is ambiguous, it may refer to:
1. This repository.
2. The `morley` package.
3. The Morley language which extends Michelson.

## Michelson version

`master` and `production` branches are maintained to be compatible with version of Michelson running on mainnet.
We use separate branches to support other versions.
More information about our branching strategy can be found [here](/docs/branching.md).

## Issue Tracker

We are using built-in issue tracker on GitLab.

We used to use [YouTrack](https://issues.serokell.io/issues/TM) as our primary issue tracker.
You may see that commit messages up to some date are prefixed with `[TM-X]`.
This prefix refers to an issue in our YouTrack.
YouTrack issues are public, so you can open any of them.

## For Contributors

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information.
