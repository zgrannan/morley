-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Re-exports typed Value, CValue, some core types, some helpers and
-- defines aliases for constructors of typed values.
--
module Lorentz.Value
  ( Value
  , IsoValue (..)
  , WellTypedIsoValue

    -- * Primitive types
  , Integer
  , Natural
  , MText
  , Bool (..)
  , ByteString
  , Address
  , EpAddress (..)
  , Mutez
  , Timestamp
  , ChainId
  , KeyHash
  , PublicKey
  , Signature
  , Set
  , Map
  , M.BigMap (..)
  , M.Operation
  , Maybe (..)
  , List
  , ContractRef (..)
  , TAddress (..)
  , FutureContract (..)

  , M.EpName
  , pattern M.DefEpName
  , EntrypointCall
  , SomeEntrypointCall

    -- * Constructors
  , toMutez
  , zeroMutez
  , oneMutez
  , mt
  , timestampFromSeconds
  , timestampFromUTCTime
  , timestampQuote

    -- * Conversions
  , M.coerceContractRef
  , callingTAddress
  , callingDefTAddress
  , ToAddress (..)
  , ToTAddress (..)
  , ToContractRef (..)
  , FromContractRef (..)
  , convertContractRef

    -- * Misc
  , Default (..)
  , Label (..)

  -- * Re-exports
  , module ReExports
  ) where

import Data.Default (Default(..))

import Lorentz.Address
import Michelson.Text
import Michelson.Typed (EntrypointCall, IsoValue(..), SomeEntrypointCall, Value, WellTypedIsoValue)
import qualified Michelson.Typed as M
import Tezos.Core
  (ChainId, Mutez, Timestamp, timestampFromSeconds, timestampFromUTCTime, timestampQuote, toMutez,
  zeroMutez, oneMutez)
import Tezos.Crypto (KeyHash, PublicKey, Signature)
import Util.CustomGeneric as ReExports
import Util.Label (Label(..))

type List = []
