-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Lorentz.TestScenario
  {-# DEPRECATED "Use cleveland instead" #-}
  ( TestScenario
  , showTestScenario
  ) where

import Fmt (Buildable, Builder, fmt, (+|), (|+))

import Lorentz.Constraints (KnownValue, NicePrintedValue, NoOperation)
import Lorentz.Print (printLorentzValue)
import Michelson.Typed.Haskell.Value (IsoValue)
import Tezos.Address (Address)

-- | Type that represents test scenario for Lorentz contract.
-- Simply put, it is sequence of pairs (`sender`, `parameter`).
-- Using this sequence we can perform transfers to the desired contract.
type TestScenario param = [(Address, param)]

-- | Function to get textual representation of @TestScenario@, each Parameter
-- is printed as a raw Michelson value.
-- This representation can later be used in order to run test scenario
-- on real network.
--
-- The format for a single contract call is the following:
-- # `printed Lorentz parameter` (actually comment)
-- `sender address`
-- `printed raw Michelson parameter`
showTestScenario
  :: (Buildable param, NicePrintedValue param)
  => TestScenario param -> Text
showTestScenario = fmt . foldMap formatParam
  where
     formatParam
       :: (Buildable param, KnownValue param, NoOperation param)
       => (Address, param) -> Builder
     formatParam (addr, param) =
       "# " +| param |+ "\n" +|
       addr |+ "\n" +|
       printLorentzValue True param |+ "\n"

data Parameter
  = Param1 Integer Bool
  | Param2
  | Param3 Natural Natural
  deriving stock Generic
  deriving anyclass IsoValue

_mkTestScenarioExample :: Address -> TestScenario Parameter
_mkTestScenarioExample owner =
  [ (owner, Param1 5 False)
  , (owner, Param2)
  , (owner, Param3 2 2)
  ]
