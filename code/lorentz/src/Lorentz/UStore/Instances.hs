-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}

module Lorentz.UStore.Instances () where

import Lorentz.StoreClass
import Lorentz.UStore.Instr
import Lorentz.UStore.Types

instance HasUField fname ftype templ =>
         StoreHasField (UStore templ) fname ftype where
  storeFieldOps = StoreFieldOps
    { sopToField = ustoreToField
    , sopSetField = ustoreSetField
    }

instance HasUStore mname key value templ =>
         StoreHasSubmap (UStore templ) mname key value where
  storeSubmapOps = StoreSubmapOps
    { sopMem = ustoreMem
    , sopGet = ustoreGet
    , sopUpdate = ustoreUpdate
    , sopDelete = Just ustoreDelete
    , sopInsert = Just ustoreInsert
    }
