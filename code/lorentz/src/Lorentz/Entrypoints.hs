-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE UndecidableSuperClasses #-}
{-# OPTIONS_GHC -Wno-redundant-constraints #-}

-- | Entrypoints utilities for Lorentz
module Lorentz.Entrypoints
  ( -- * Typeclasses
    EntrypointsDerivation (..)
  , ParameterHasEntrypoints (..)
  , ParameterDeclaresEntrypoints

    -- * Entrypoints API
  , AllParameterEntrypoints
  , LookupParameterEntrypoint
  , parameterEntrypointsToNotes
  , GetEntrypointArg
  , parameterEntrypointCall
  , GetDefaultEntrypointArg
  , parameterEntrypointCallDefault
  , ForbidExplicitDefaultEntrypoint
  , NoExplicitDefaultEntrypoint
  , sepcCallRootChecked
  , EntrypointRef (..)
  , eprName
  , GetEntrypointArgCustom
  , HasEntrypointArg (..)
  , HasDefEntrypointArg
  , HasEntrypointOfType
  , ParameterContainsEntrypoints
  , TrustEpName (..)
  , parameterEntrypointCallCustom
  , RequireAllUniqueEntrypoints
  , (:>)

    -- * Implementations
  , EpdNone
  , EpdPlain
  , EpdRecursive
  , EpdDelegate
  , EpdWithRoot

    -- * Behaviour modifiers
  , ParameterWrapper (..)
  , ShouldHaveEntrypoints (..)
  ) where

import Lorentz.Entrypoints.Core
import Lorentz.Entrypoints.Helpers
import Lorentz.Entrypoints.Impl
import Lorentz.Entrypoints.Manual
