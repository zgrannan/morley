Unreleased
==========
<!-- Append new entries here -->
* [!528](https://gitlab.com/morley-framework/morley/-/merge_requests/528)
  The generated documentation now contains a sample value of each entrypoint.
  + Allow modification of sample value via `mkDEntrypointExample`.
* [!493](https://gitlab.com/morley-framework/morley/-/merge_requests/493)
  Add support for root entrypoint.
  + A contract can now specify whether or not the root annotation should be generated
    and which name it should use.
  + `self` and `contract` instructions are able to call the root entrypoint.

0.5.0
=====
* [!530](https://gitlab.com/morley-framework/morley/-/merge_requests/530)
  Field annotation are now generated for sum-type in `parameter` and `storage`.
  + It also ensures that it will not generate field annotations that would introduce
  extra entrypoints.
* [!371](https://gitlab.com/morley-framework/morley/-/merge_requests/371)
  Make `view_` argument accept 2 arguments rather than a pair.
* [!486](https://gitlab.com/morley-framework/morley/-/merge_requests/486)
  Add `customGeneric` to derive `Generic` instances with a custom tree structure
  and, consequently, `IsoValue` instances with such a structure.
* [!483](https://gitlab.com/morley-framework/morley/-/merge_requests/483)
  Add a way to generate field annotations for entrypoint parameter.
* [!470](https://gitlab.com/morley-framework/morley/-/merge_requests/470)
  Move `Lorentz.Test` and  Hedgehog generators to a new package: `cleveland`.
* [!357](https://gitlab.com/morley-framework/morley/-/merge_requests/357)
  Remove deprecated `Lorentz.Store` module and deprecated custom error functionality.

0.4.0
=====
* [!449](https://gitlab.com/morley-framework/morley/-/merge_requests/449)
  + Improve `Lorentz.Test.Integrational` interface.
  + Remove `IntegrationalValidator`, `SuccessValidator`, `validate`,
    `composeValidators`, `composeValidatorsList`, `expectAnySuccess`.
  + Add `unexpectedInterpreterError`, `attempt`, `catchExpectedError`.
  + Change type of `expect*`/`tExpect*`/`lExpect*` functions.
    They now return `IntegrationalScenario` instead of `SuccessValidator` / `Bool`.
  + Rename `ValidationError` to `TestError`
  + Replace `TestError`'s `UnexpectedExecutorError` constructor with
    `InterpreterError` and `UnexpectedInterpreterError`.
* [!432](https://gitlab.com/morley-framework/morley/-/merge_requests/432)
  Add `whenNone` to Lorentz.
* [!398](https://gitlab.com/morley-framework/morley/-/merge_requests/398)
  Add `StoreHasEntrypoint` class to conveniently use entrypoints as lambdas
  stored into `big_map`(s) to make use of lazy evaluation.
* [!409](https://gitlab.com/morley-framework/morley/-/merge_requests/409)
  + Add hedgehog support
* [!303](https://gitlab.com/morley-framework/morley/merge_requests/303)
  + Functions for working with `UStore` from Haskell world (e.g. `fillUStore`) now have a different constraint.
  + `UStore` templates are now documented and thus may require instance of the respective typeclass.

0.3.0
=====
* [!417](https://gitlab.com/morley-framework/morley/-/merge_requests/417)
Change the license to MIT.
* [!360](https://gitlab.com/morley-framework/morley/-/merge_requests/360)
  + Add `compileLorentzWithOptions` function that accepts optional Michelson
  optimizer configuration and string transformer function. Changed `compileLorentz` to call the
  optimizer with the default configuration.
  + Add `Lorentz.Contract` data type as a wrapper for `Lorentz.ContractCode` and
    `Lorentz.CompilationOptions`. Require that all contracts are wrapped in this data type.
* [!354](https://gitlab.com/morley-framework/morley/-/merge_requests/354)
  + Add `constructStack` function that constructs a datatype from fields on the stack.
  + Add `deconstruct` function that deconstructs a datatype to its fields on the stack.
* [!358](https://gitlab.com/morley-framework/morley/-/merge_requests/358)
  Removed deprecated `STEPS_TO_QUOTA` instruction.
* [!337](https://gitlab.com/morley-framework/morley/-/merge_requests/337)
  Added `comment`, `commentAroundFun` and `commentAroundStmt` to generate comments in Michelson code.

0.2.0
=====
* [!346](https://gitlab.com/morley-framework/morley/-/merge_requests/346)
  Added `docStorage` and `contractGeneralDefault`.
* [!306](https://gitlab.com/morley-framework/morley/-/merge_requests/306)
  Deprecated `mapMigrationCode` in favor of `MapLorentzInstr`.
* [!326](https://gitlab.com/morley-framework/morley/-/merge_requests/326)
Updated contract registry (`Lorentz.ContractRegistry`):
  + Now it can print initial storage apart from contract and documentation.
  + Some extra fields were added to `ContractInfo`.
  + Logic is captured in the `runContractRegistry` function.
  + If you don't specify output file, we will use a file with name constructed from contract name. Pass `-` if you want `stdout`.
* [!245](https://gitlab.com/morley-framework/morley/-/merge_requests/245) Added `HasTypeAnn` instance for `FutureContract arg`.
* [!294](https://gitlab.com/morley-framework/morley/-/merge_requests/294)
  + Added `Paths_*` modules to `autogen-modules` in cabal files.  Removed `-O0`
  + from default GHC options. Please set `ghc-options` in your `stack.yaml` or
  `cabal.project.local`.
* [!271](https://gitlab.com/morley-framework/morley/merge_requests/271) Renamed
  'Contract' to 'ContractCode', and appended "Code" to the names of two functions:
  'convertContract' and 'printTypedContract'
* [!267](https://gitlab.com/morley-framework/morley/-/merge_requests/267)
  + Retain type annotations in entrypoints derivation.
  + Remove overlappable `HasTypeAnn` instance defined for nearly each type.
    Add `default` `getTypeAnn` method definition instead and manually define `HasTypeAnn` instance for each type instead (trivially).
    When you create a new data type with `IsoValue` instance, you usually have to derive `HasTypeAnn` as well.

0.1.0
=====

Initial release extracted from the [morley](https://hackage.haskell.org/package/morley) package.
