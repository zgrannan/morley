-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Main
  ( main
  ) where

import qualified Data.Map as Map
import Data.Version (showVersion)
import qualified Options.Applicative as Opt
import Options.Applicative.Help.Pretty (Doc, linebreak)
import Paths_morley_multisig (version)

import qualified Lorentz as L
import Lorentz.ContractRegistry
import Lorentz.Contracts.Multisig.Generic
import Lorentz.Contracts.Multisig.Specialized
import Util.IO

contracts :: ContractRegistry
contracts = ContractRegistry $ Map.fromList
  [ "Generic" ?:: ContractInfo
    { ciContract = genericMultisigContract @'BaseErrors
    , ciIsDocumented = True
    , ciStorageParser = Nothing
    , ciStorageNotes = Nothing
    }
  , "GenericWithCustomErrors" ?:: ContractInfo
    { ciContract = genericMultisigContract @'CustomErrors
    , ciIsDocumented = True
    , ciStorageParser = Nothing
    , ciStorageNotes = Nothing
    }
  , "Specialized" ?:: ContractInfo
    { ciContract = specializedMultisigContract @Natural @Natural @'CustomErrors L.CallDefault
    , ciIsDocumented = False
    , ciStorageParser = Nothing
    , ciStorageNotes = Nothing
    }
  ]

programInfo :: L.DGitRevision -> Opt.ParserInfo CmdLnArgs
programInfo gitRev = Opt.info (Opt.helper <*> versionOption <*> argParser contracts gitRev) $
  mconcat
  [ Opt.fullDesc
  , Opt.progDesc "Multisig contracts registry"
  , Opt.header "Multisig contracts for Michelson"
  , Opt.footerDoc usageDoc
  ]
  where
    versionOption = Opt.infoOption ("morley-multisig-" <> showVersion version)
      (Opt.long "version" <> Opt.help "Show version.")

usageDoc :: Maybe Doc
usageDoc = Just $ mconcat
   [ "You can use help for specific COMMAND", linebreak
   , "EXAMPLE:", linebreak
   , "  morley-multisig print --help", linebreak
   ]

main :: IO ()
main = do
  hSetTranslit stdout
  hSetTranslit stderr
  let gitRev = $(L.mkDGitRevision) L.morleyRepoSettings
  cmdLnArgs <- Opt.execParser (programInfo gitRev)
  runContractRegistry contracts cmdLnArgs `catchAny` (die . displayException)
