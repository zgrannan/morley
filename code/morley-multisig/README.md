# Morley Multisig

This package contains contracts implemented using Haskell eDSL for Michelson based on Morley.
The contracts reimplement common Michelson multisig contracts:
* `Generic` multisig contract contract provides an ability to perform arbitrary contract
call via multisig.
* `GenericWithCustomErrors` version of `Generic` with human-readable errors

## Contract registry

To read contracts defined in this package one can use `morley-multisig` executable.

Example (from this directory):
```sh
make
stack exec morley-multisig -- list
```
Shows names of the contracts.

```sh
stack exec morley-multisig -- print -n Generic
#or
stack exec morley-multisig -- print -n GenericWithCustomErrors
```
Prints the code of the `Generic` multisig contract or its version with human-readable
errors.

## Contracts documentation

Some contracts have their documentation published and automatically updated by CI:
* [GenericWithCustomErrors](https://gitlab.com/morley-framework/morley/blob/autodoc/master/autodoc/GenericWithCustomErrors.md)
