-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Lorentz.Contracts.Multisig.Specialized
  ( test_specializedMultisig
  ) where

import Prelude hiding (drop)

import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Lorentz
import qualified Lorentz.Contracts.Multisig.Generic as Generic
import Lorentz.Contracts.Multisig.Specialized as Specialized
import Lorentz.Test.Consumer
import Lorentz.Test.Integrational
import Michelson.Runtime (prepareContract)
import Michelson.Test (originate)
import Michelson.Typed.Convert (untypeValue)
import Tezos.Core (dummyChainId)
import Tezos.Crypto
import qualified Tezos.Crypto.Ed25519 as Ed22519
import Util.Named ((.!))

type TargetParameter = Natural

originateTarget
  :: forall param. (NiceParameterFull param) => IntegrationalScenarioM (TAddress param)
originateTarget =
  lOriginate (defaultContract $ cdr # nil # pair) "TargetContract" () (toMutez 0)

originateMultisig
  :: forall param mainparam mname.
  (NiceMultisigParam param mainparam mname) => Natural -> Natural -> [PublicKey]
  -> EntrypointRef mname
  -> IntegrationalScenarioM (TAddress (Parameter param mainparam))
originateMultisig counter threshold pKeys epref =
  lOriginate (specializedMultisigContract
      @param @mainparam @'Generic.CustomErrors epref) "SpecializedMultisigContract"
    (mkStorage counter threshold pKeys) (toMutez 0)

prepareContracts
  :: Natural
  -> Natural
  -> [PublicKey]
  -> IntegrationalScenarioM (TAddress (Parameter TargetParameter TargetParameter), TAddress TargetParameter)
prepareContracts counter threshold pKeys = do
  target <- originateTarget @TargetParameter
  msig <- originateMultisig @TargetParameter @TargetParameter @'Nothing counter threshold pKeys CallDefault
  pure (msig, target)

bsToSign :: forall param mainparam.
     (NicePackedValue (GenericMultisigAction param mainparam))
  => TAddress (Parameter param mainparam)
  -> Natural
  -> GenericMultisigAction param mainparam
  -> ByteString
bsToSign msigAddr counter pl =
  lPackValue ((dummyChainId, toAddress msigAddr), (counter, pl))

test_specializedMultisig :: TestTree
test_specializedMultisig = testGroup "tests to check genericMultisig contract functionality"
  [ testCase "Call with enough amount of correct signatures \
             \with correct counter and 0 amount transfer" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, target)
        bs = bsToSign msig counter action
      lCallEP msig (Call @"MainParameter") $
        mkMultisigParam counter action $
          Generic.Signatures $
            Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]

  , testCase "Call with insufficient signatures" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 3
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, target)
        bs = bsToSign msig counter action
      err <- expectError $ lCallEP msig (Call @"MainParameter") $
        mkMultisigParam counter action $
          Generic.Signatures $
            Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectCustomError #insufficientSignatures () err

  , testCase "Call with incorrect counter" $
    integrationalTestExpectation $ do
      let
        counter = 1
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, target)
        bs = bsToSign msig counter action
      err <- expectError $ lCallEP msig (Call @"MainParameter") $
        mkMultisigParam (counter + 1) action $
          Generic.Signatures $
            Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectCustomError #counterDoesntMatch () err

  , testCase "Provide invalid signature" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, target)
        bs = bsToSign msig counter action
      err <- expectError $ lCallEP msig (Call @"MainParameter") $
        mkMultisigParam counter action $
          Generic.Signatures $
            Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [aliceSK, carlosSK]
      lExpectCustomError #invalidSignature () err

  , testCase "Provide less signatures than public keys in storage" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, target)
        bs = bsToSign msig counter action
      err <- expectError $ lCallEP msig (Call @"MainParameter") $
        mkMultisigParam counter action $
        Generic.Signatures $ Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [aliceSK, bobSK]
      lExpectCustomError #fewerSignaturesThanKeys () err

  , testCase "Provide more signatures than public keys in storage" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, target)
        bs = bsToSign msig counter action
      err <- expectError $ lCallEP msig (Call @"MainParameter") $ mkMultisigParam counter action $
        Generic.Signatures $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK, aliceSK]
      lExpectCustomError #uncheckedSignaturesLeft () err

  , testCase "Non-zero transfer with Operation action updates target balance" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, target) <- prepareContracts counter threshold masterPKList
      let
        action = Operation (10, target)
        bs = bsToSign msig counter action
      lTransfer (#from .! genesisAddress) (#to .! msig) (toMutez 10) (Call @"MainParameter") $
        mkMultisigParam counter action $
        Generic.Signatures $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectBalance target (toMutez 10)

  , testCase "Non-zero transfer to Default updates multisig balance" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig @TargetParameter @TargetParameter counter threshold masterPKList CallDefault
      lTransfer (#from .! genesisAddress) (#to .! msig) (toMutez 10) CallDefault ()
      lExpectBalance msig (toMutez 10)

  , testCase "Simple contract interaction example test" $ do
      idContract <- prepareContract
        (Just "../../contracts/tezos_examples/attic/id.tz")
      integrationalTestExpectation $ do
        let
          counter = 0
          threshold = 2
        msig <- originateMultisig @MText @MText counter threshold masterPKList CallDefault
        idAddr <- originate idContract "idString"
          (untypeValue $ toVal [mt|kek|]) (toMutez 0)
        let
          newStorage = [mt|mda|]
          action = Operation ([mt|mda|], (TAddress @MText idAddr))
          bs = bsToSign msig counter action
        lCallEP msig (Call @"MainParameter") $
          mkMultisigParam counter action $
          Generic.Signatures $
            Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
          [bobSK, carlosSK]
        lExpectStorageConst idAddr newStorage

  , testCase "Bad key update with threshold > key count is rejected" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      let
        action = ChangeKeys (10, Generic.Keys masterPKList)
        bs = bsToSign msig counter action
      err <- expectError $ lCallEP msig (Call @"MainParameter") $ mkMultisigParam counter action $
        Generic.Signatures $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectCustomError #thresholdLargerThanKeysSize () err

  , testCase "Bad key update with threshold = 0 is rejected" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      let
        action = ChangeKeys (0, Generic.Keys masterPKList)
        bs = bsToSign msig counter action
      err <- expectError $ lCallEP msig (Call @"MainParameter") $ mkMultisigParam counter action $
        Generic.Signatures $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectCustomError #zeroThresholdUpdate () err

  , testCase "Key update with good threshold is accepted" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      let
        action = ChangeKeys (3, Generic.Keys masterPKList)
        bs = bsToSign msig counter action
      lCallEP msig (Call @"MainParameter") $ mkMultisigParam counter action $
        Generic.Signatures $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]

  , testCase "Non-zero transfer with ChangeKeys action prohibited" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      let
        action = ChangeKeys (3, Generic.Keys masterPKList)
        bs = bsToSign msig counter action
      err <- expectError $ lTransfer (#from .! genesisAddress) (#to .! msig) (toMutez 10) (Call @"MainParameter") $
        mkMultisigParam counter action $
        Generic.Signatures $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectCustomError_ #nonZeroTransfer err

  , testCase "Mutez transfer updates target balance with sufficient amount of valid \
             \signatures and fails otherwise" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      consumer <- lOriginateEmpty contractConsumer "consumer"
      let
        action = TransferTokens (consumer, toMutez 80)
        bs = bsToSign msig counter action
        bs1 = bsToSign msig (counter + 1) action
      lTransfer (#from .! genesisAddress) (#to .! msig) (toMutez 100) CallDefault ()
      lCallEP msig (Call @"MainParameter") $ mkMultisigParam counter action $
        Generic.Signatures $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      branchout
        [ "target balance is updated" ?-
            lExpectBalance consumer (toMutez 80)
        , "fails when not enough signatures" ?- do
            err <- expectError $  lCallEP msig (Call @"MainParameter") $ mkMultisigParam (counter + 1) action $
              Generic.Signatures $
              Nothing : Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs1)
              [carlosSK]
            lExpectCustomError_ #insufficientSignatures err

        ]
  , testCase "Non-zero transfer with TransferTokens action permitted" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      (msig, _) <- prepareContracts counter threshold masterPKList
      consumer <- lOriginateEmpty contractConsumer "consumer"
      let
        -- TODO: ensure that BALANCE includes AMOUNT once https://gitlab.com/morley-framework/morley/-/issues/121
        -- is resolved
        action = TransferTokens (consumer, toMutez 80)
        bs = bsToSign msig counter action
      lTransfer (#from .! genesisAddress) (#to .! msig) (toMutez 100) CallDefault ()
      lTransfer (#from .! genesisAddress) (#to .! msig) (toMutez 10) (Call @"MainParameter") $
        mkMultisigParam counter action $
        Generic.Signatures $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectBalance consumer (toMutez 80)
  ]
  where
    mkMultisigParam
      :: forall p mainparam. Natural
      -> GenericMultisigAction p mainparam
      -> Generic.Signatures
      -> (MainParams p mainparam)
    mkMultisigParam counter action (Generic.Signatures sigs) =
      ((Counter counter, action) , sigs)

    aliceSK = Ed22519.detSecretKey "\001\002\003\004"
    bobSK = Ed22519.detSecretKey "\005\006\007\008"
    carlosSK = Ed22519.detSecretKey "\009\010\011\012"

    alicePK = Ed22519.toPublic aliceSK
    bobPK = Ed22519.toPublic bobSK
    carlosPK = Ed22519.toPublic carlosSK

    masterPKList = Prelude.map PublicKeyEd25519 [alicePK, bobPK, carlosPK]
