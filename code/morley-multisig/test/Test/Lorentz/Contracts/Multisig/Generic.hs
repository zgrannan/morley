-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Lorentz.Contracts.Multisig.Generic
  ( test_genericMultisig
  , test_documentation
  ) where

import Prelude hiding (drop)

import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import Lorentz
import Lorentz.Contracts.Multisig.Generic as MSig
import Lorentz.Test (testLorentzDoc)
import Lorentz.Test.Integrational
import Michelson.Interpret.Pack (packValue')
import Michelson.Runtime (prepareContract)
import Michelson.Test (originate, runDocTests)
import Michelson.Typed.Convert (untypeValue)
import Tezos.Core (dummyChainId)
import Tezos.Crypto
import qualified Tezos.Crypto.Ed25519 as Ed22519
import Util.Named ((.!))

originateMultisig
  :: Natural -> Natural -> [PublicKey]
  -> IntegrationalScenarioM (TAddress MSig.Parameter)
originateMultisig counter threshold pKeys =
  lOriginate (genericMultisigContract @'CustomErrors) "MultisigContract"
  (mkStorage counter threshold pKeys) (toMutez 0)

data IdStringParameter = IdStringParameter MText
  deriving stock Generic
  deriving anyclass IsoValue

test_genericMultisig :: TestTree
test_genericMultisig = testGroup "tests to check genericMultisig contract functionality"
  [ testCase "Call with enough amount of correct signatures \
             \with correct counter and 0 amount transfer" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let bs = bsToSign msig counter simpleLambda
      lCallEP msig (Call @"Main") $
        mkMultisigParam counter simpleLambda $
        Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
  , testCase "Call with insufficient signatures" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 3
      msig <- originateMultisig counter threshold masterPKList
      let bs = bsToSign msig counter simpleLambda
      err <- expectError $ lCallEP msig (Call @"Main") $
        mkMultisigParam counter simpleLambda $
        Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectCustomError #insufficientSignatures () err

  , testCase "Call with incorrect counter" $
    integrationalTestExpectation $ do
      let
        counter = 1
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let bs = bsToSign msig counter simpleLambda
      err <- expectError $ lCallEP msig (Call @"Main") $
        mkMultisigParam (counter + 1) simpleLambda $
        Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectCustomError #counterDoesntMatch () err

  , testCase "Provide invalid signature" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let bs = bsToSign msig counter simpleLambda
      err <- expectError $ lCallEP msig (Call @"Main") $
        mkMultisigParam counter simpleLambda $
        Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [aliceSK, carlosSK]
      lExpectCustomError #invalidSignature () err

  , testCase "Provide less signatures than public keys in storage" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let bs = bsToSign msig counter simpleLambda
      err <- expectError $ lCallEP msig (Call @"Main") $
        mkMultisigParam counter simpleLambda $
        Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [aliceSK, bobSK]
      lExpectCustomError #fewerSignaturesThanKeys () err

  , testCase "Provide more signatures than public keys in storage" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let bs = bsToSign msig counter simpleLambda
      err <- expectError $ lCallEP msig (Call @"Main") $ mkMultisigParam counter
        simpleLambda $
        Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK, aliceSK]
      lExpectCustomError #uncheckedSignaturesLeft () err

  , testCase "Non-zero transfer to ParameterMain" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let bs = bsToSign msig counter simpleLambda
      err <- expectError $ lTransfer (#from .! genesisAddress) (#to .! msig) (toMutez 1) (Call @"Main") $
        mkMultisigParam counter simpleLambda $
        Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
        [bobSK, carlosSK]
      lExpectCustomError #nonZeroTransfer () err

  , testCase "Non-zero transfer to Default" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      lTransfer (#from .! genesisAddress) (#to .! msig) (toMutez 1) CallDefault ()

  , testCase "Simple contract interaction example test" $ do
      idContract <- prepareContract
        (Just "../../contracts/tezos_examples/attic/id.tz")
      integrationalTestExpectation $ do
        let
          counter = 0
          threshold = 2
        msig <- originateMultisig counter threshold masterPKList
        idAddr <- originate idContract "idString"
          (untypeValue $ toVal [mt|kek|]) (toMutez 0)
        let
          newStorage = [mt|mda|]
          lambdaParam = contractToLambda @MText (EpAddress idAddr DefEpName)
                        newStorage
          bs = bsToSign msig counter $ Operation lambdaParam
        lCallEP msig (Call @"Main") $
          mkMultisigParam counter (Operation lambdaParam) $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
          [bobSK, carlosSK]
        lExpectStorageConst idAddr newStorage

  , testCase "Update threshold to zero is prohibited" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let
        keyRotation = ChangeKeys (Threshold 0, Keys updatedPKList)
        bs = bsToSign msig counter keyRotation
      err <- expectError $ lCallEP msig (Call @"Main") $
          mkMultisigParam counter keyRotation $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
          [bobSK, carlosSK]
      lExpectCustomError #zeroThresholdUpdate () err
  , testCase "Update to threshold larger than keys list size is prohibited" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let
        keyRotation = ChangeKeys (Threshold 4, Keys updatedPKList)
        bs = bsToSign msig counter keyRotation
      err <- expectError $ lCallEP msig (Call @"Main") $
          mkMultisigParam counter keyRotation $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
          [bobSK, carlosSK]
      lExpectCustomError #thresholdLargerThanKeysSize () err
  , testCase "Valid key rotation" $
    integrationalTestExpectation $ do
      let
        counter = 0
        threshold = 2
      msig <- originateMultisig counter threshold masterPKList
      let
        keyRotation = ChangeKeys (Threshold 2, Keys updatedPKList)
        bs = bsToSign msig counter keyRotation
      lCallEP msig (Call @"Main") $
          mkMultisigParam counter keyRotation $
          Nothing : Prelude.map (\sk -> Just $ SignatureEd25519 $ Ed22519.sign sk bs)
          [bobSK, carlosSK]
      lExpectStorageConst msig (Counter 1, (Threshold 2, Keys updatedPKList))
  ]
  where
    mkMultisigParam
      :: Natural -> ParamAction
      -> [Maybe Signature] -> ParamMain
    mkMultisigParam counter action sigs =
      ( #payload .! (Counter counter, #action .! action)
      , Signatures sigs
      )
    simpleLambda :: ParamAction
    simpleLambda = Operation $ drop # nil
    bsToSign
      :: TAddress MSig.Parameter -> Natural
      -> ParamAction
      -> ByteString
    bsToSign msigAddr counter action =
      packValue' $ toVal ((dummyChainId, toAddress msigAddr), (counter, action))

    aliceSK = Ed22519.detSecretKey "\001\002\003\004"
    bobSK = Ed22519.detSecretKey "\005\006\007\008"
    carlosSK = Ed22519.detSecretKey "\009\010\011\012"
    zyabaSK = Ed22519.detSecretKey "\013\014\015\016"
    zyobaSK = Ed22519.detSecretKey "\017\018\019\020"

    alicePK = Ed22519.toPublic aliceSK
    bobPK = Ed22519.toPublic bobSK
    carlosPK = Ed22519.toPublic carlosSK
    zyabaPK = Ed22519.toPublic zyabaSK
    zyobaPK = Ed22519.toPublic zyobaSK

    masterPKList = Prelude.map PublicKeyEd25519 [alicePK, bobPK, carlosPK]
    updatedPKList = Prelude.map PublicKeyEd25519 [alicePK, zyabaPK, zyobaPK]

test_documentation :: [TestTree]
test_documentation =
  [ testGroup "Base errors" $
      runTests $ genericMultisigContract @'BaseErrors
  , testGroup "Custom errors" $
      runTests $ genericMultisigContract @'CustomErrors
  ]
  where
    runTests = runDocTests testLorentzDoc . buildLorentzDoc . cCode
