-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS -Wno-orphans #-}

module Lorentz.Contracts.Multisig.Specialized.Types
  ( ChangeKeyParams
  , GMSig.Counter (..)
  , GenericMultisigAction (..)
  , MainParams
  , Parameter (..)
  , Payload
  , Storage
  , ToSign
  , GMSig.Threshold (..)
  , NiceMultisigParam
  , mkStorage
  ) where

import Data.Typeable
import Text.Show (Show(..))

import Lorentz hiding (concat)
import qualified Lorentz.Contracts.Multisig.Generic as GMSig

type ToSign a mainparam = ((ChainId, Address), (GMSig.Counter, GenericMultisigAction a mainparam))

----------------------------------------------------------------------------
-- Parameter
----------------------------------------------------------------------------

-- Note: @threshold@ is also known as @quorum@
type ChangeKeyParams =
  ( GMSig.Threshold     -- "threshold" :!
  , GMSig.Keys -- "keys"      :!
  )

-- | Either perform an `Operation` with the included contract or
-- use `ChangeKeys` to update the key list and threshold (quorum)
-- @
--  type GenericMultisigAction a = Either a ChangeKeyParams
-- @
data GenericMultisigAction a mainparam
  = Operation !(a, TAddress mainparam)
  -- This field is a TAddress instead of a ContractRef because this is a part
  -- of the data that is going to be packed and signed as part of the multisig
  -- operation.  So, given a sequence of packed bytes, we might want to decode
  -- it and check the contents, to make sure the operation that we are signing
  -- is legit. If the TAddress field is a ContractRef field, it prevents us
  -- from decoding the packed representation to this type, because right now
  -- morley forbids unpacking of bytes into values containing `ContractRef`
  -- inside it. So this field is kepts as a TAddress field itself.
  | TransferTokens (TAddress (), Mutez)
  | ChangeKeys !ChangeKeyParams
  deriving stock Generic
  deriving stock Show

deriving anyclass instance (WellTypedIsoValue a, WellTypedIsoValue r) => IsoValue (GenericMultisigAction a r)
deriving anyclass instance (HasAnnotation a, WellTypedIsoValue a, WellTypedIsoValue r) => HasAnnotation (GenericMultisigAction a r)

instance Show (TAddress cp) where
  show (TAddress a) = show a

type Payload a mainparam =
    ( GMSig.Counter  -- "counter" :!
    , GenericMultisigAction a mainparam -- "action"  :!
    )

type MainParams a mainparam =
  ( Payload a mainparam
  , [Maybe Signature]       -- "sigs"    :!
  )

-- | Use `Default` to send tokens to the contract.
-- Otherwise, use `MainParameter`
data Parameter a mainparam
  = Default
  | MainParameter (MainParams a mainparam)
  deriving stock Generic
  deriving stock Show

deriving anyclass instance (WellTypedIsoValue a, WellTypedIsoValue mainparam) => IsoValue (Parameter a mainparam)

instance (HasAnnotation a, WellTypedIsoValue a, WellTypedIsoValue mainparam) => ParameterHasEntrypoints (Parameter a mainparam) where
  type ParameterEntrypointsDerivation (Parameter a mainparam) = EpdPlain

type Storage =
  ( GMSig.Counter -- "storedCounter" :!
  , ( GMSig.Threshold     -- "threshold" :!
    , GMSig.Keys -- "keys"      :!
    )
  )

mkStorage :: Natural -> Natural -> [PublicKey] -> Storage
mkStorage counter threshold keys_ =
  (GMSig.Counter counter, (GMSig.Threshold threshold, GMSig.Keys keys_))

----------------------------------------------------------------------------
-- Errors
----------------------------------------------------------------------------

type instance ErrorArg "contractConversionError" = ()

type instance ErrorArg "nonUnitReceiverEntrypoint" = ()

-- Documentation
----------------------------------------------------------------------------

instance CustomErrorHasDoc "contractConversionError" where
  customErrDocMdCause =
    "Error during converison of an address to contract reference"
  customErrClass = ErrClassActionException

instance CustomErrorHasDoc "nonUnitReceiverEntrypoint" where
  customErrDocMdCause =
    "Trying to transfer tokens to non-unit entrypoint of the contract"
  customErrClass = ErrClassActionException


type NiceMultisigParam param mainparam mname =
  ( HasEntrypointArg mainparam (EntrypointRef mname) param, HasAnnotation param
  , NiceParameterFull param, Typeable param, NicePackedValue param
  , NiceParameter mainparam, Typeable mainparam)
