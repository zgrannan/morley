-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Lorentz implementation of the specialized multisig contract.
-- Based on the following implementation:
-- https://github.com/tqtezos/lorentz-contract-multisig/blob/b69248a46e99a749cc767df5d37dde53e009d162/src/Lorentz/Contracts/GenericMultisig.hs
module Lorentz.Contracts.Multisig.Specialized.Contract
  ( specializedMultisigContract
  ) where

import Prelude hiding (and, drop, show, swap, unwords, (>>))

import Data.Typeable as T

import Lorentz hiding (concat)

import Lorentz.Contracts.Multisig.Generic (ErrorHandler(..), ErrorsKind(..), Keys, ensureChangeKeys)
import Lorentz.Contracts.Multisig.Specialized.Types


----------------------------------------------------------------------------
-- Implementation
----------------------------------------------------------------------------

-- | The default action: do nothing
genericMultisigContractDefault :: b : s :-> ([Operation], b) : s
genericMultisigContractDefault = do
  nil @Operation
  pair

-- | Assert no token was sent:
-- to send tokens, the default entry point should be used
assertNoTokensSent :: forall (e :: ErrorsKind) s. (ErrorHandler e) => s :-> s
assertNoTokensSent = do
  --   # Assert no token was sent:
  --   # to send tokens, the default entry point should be used
  push zeroMutez >> amount >> (ifEq nop (handleNonZeroTransfer @e))

type MultisigParamConstrains a mainparam =
  ( NicePackedValue (GenericMultisigAction a mainparam)
  , NiceParameterFull (Parameter a mainparam))

-- | Pair the payload with the current contract address, to ensure signatures
-- | can't be replayed accross different contracts if a key is reused.
preparePayload
  :: forall a mainparam b c s. MultisigParamConstrains a mainparam
  => ((Counter, GenericMultisigAction a mainparam), b) : c : s
     :-> c : Counter : ByteString : b : GenericMultisigAction a mainparam : c : s
preparePayload = do
  swap >> dup >> dip swap
  dip $ do
    unpair
    dup >> selfCalling @(Parameter a mainparam) CallDefault >> address >> chainId
    pair >> pair
    pack @(ToSign a mainparam)
    dip (unpair @Counter >> dip swap) >> swap

-- | `assertEq` on the parameter counter and storage counter
checkCountersMatch
  :: forall (e :: ErrorsKind) b s. (ErrorHandler e)
  => (Counter, b) : Counter : s :-> b : s
checkCountersMatch = do
  -- # Check that the counters match
  unpair >> dip swap
  ifEq nop (handleCounterMismatch @e)

-- | Compute the number of valid signatures
countValidSignatures
  :: forall e a s. (ErrorHandler e)
  => (a, Keys) : ByteString : List (Maybe Signature) : s
     :-> a : Natural : List (Maybe Signature) : ByteString : s
countValidSignatures = do
  -- # Compute the number of valid signatures
  dip swap >> unpair -- @Natural @[PublicKey]
  dip $ do
    coerceUnwrap
    push (0 :: Natural) >> swap
    iter $ do
      dip swap >> swap
      ifCons
        (ifSome
          (do
            swap
            dip $ do
              swap >> (dip $ dip $ duupX @2)
              duupX @3 >> dip checkSignature >> swap >> if_ drop (handleInvalidSignature @e)
              push (1 :: Natural) >> add
          )
          (swap >> drop)
        )
        (handleFewerSignaturesThanKeys @e)
      swap


-- | Assert that the threshold is less than or equal to the
-- number of valid signatures.
assertQuorumPresent
  :: forall (e :: ErrorsKind) s. (ErrorHandler e) => Threshold : Natural : s :-> s
assertQuorumPresent = do
  -- # Assert that the threshold is less than or equal to the
  -- # number of valid signatures.
  coerceUnwrap
  ifLe nop (handleInsufficientSignatures @e)

-- | Assert no unchecked signature remains
assertAllSignaturesChecked :: forall e b c. (ErrorHandler e) => [Maybe Signature] : b : c :-> c
assertAllSignaturesChecked = do
  -- # Assert no unchecked signature remains
  ifCons (handleUncheckedSignaturesLeft @e) nop
  drop

-- | Increment counter and place in storage
incrementAndStoreCounter :: a : (Counter, b) : s :-> a : (Counter, b) : s
incrementAndStoreCounter = do
  -- # Increment counter and place in storage
  dip $ do
    unpair
    push (Counter 1)
    add
    pair

multisigSetup
  :: forall (e :: ErrorsKind) a mainparam.
  (MultisigParamConstrains a mainparam, ErrorHandler e)
  => '[ ((Counter, GenericMultisigAction a mainparam), [Maybe Signature]) -- MainParams
      , (Counter, (Threshold, Keys)) -- Storage
      ]
  :-> '[GenericMultisigAction a mainparam, (Counter, (Threshold, Keys))]
multisigSetup = do
  preparePayload @a @mainparam
  checkCountersMatch @e
  countValidSignatures @e
  assertQuorumPresent @e
  assertAllSignaturesChecked @e
  incrementAndStoreCounter

genericMultisigContractSimpleStorageMain
  :: forall (e :: ErrorsKind) a mainparam mname.
  (NiceMultisigParam a mainparam mname, ErrorHandler e)
  => '[(a, TAddress mainparam)] :-> '[[Operation]]
  -> '[MainParams a mainparam, Storage] :-> '[([Operation], Storage)]
genericMultisigContractSimpleStorageMain runParam = do
  multisigSetup @e @a @mainparam

  -- # We have now handled the signature verification part,
  -- # produce the operation requested by the signers.
  caseT @(GenericMultisigAction a mainparam)
    ( #cOperation /-> (swap >> dip runParam >> swap)
    , #cTransferTokens /-> do
        unpair
        contract @()
        if IsSome then nop else failCustom_ #nonUnitReceiverEntrypoint
        swap
        unit
        transferTokens
        dip nil
        cons
    , #cChangeKeys /-> do
        assertNoTokensSent @e
        ((dup >> ensureChangeKeys @e) >> dip car >> swap >> pair >> nil)
    )
  pair

genericMultisigContractSimpleStorage
  :: forall a mainparam e mname.
  (NiceMultisigParam a mainparam mname, ErrorHandler e)
  => '[(a, TAddress mainparam)] :-> '[[Operation]]
  -> ContractCode (Parameter a mainparam) Storage
genericMultisigContractSimpleStorage runParam = do
  unpair
  caseT @(Parameter a mainparam)
    -- Default entry point: do nothing
    -- This entry point can be used to send tokens to this contract
    ( #cDefault /->
      genericMultisigContractDefault
    , #cMainParameter /->
      genericMultisigContractSimpleStorageMain @e @a @mainparam @mname runParam
    )

failWithChoice
  :: forall (e :: ErrorsKind) name s any. (Typeable e, CustomErrorHasDoc name, ErrorArg name ~ ())
  => Label name
  -> (s :-> any)
failWithChoice l = case T.eqT @'CustomErrors @e of
  Just Refl -> failCustom_ l
  Nothing -> case T.eqT @'BaseErrors @e of
    Just Refl -> unit # failWith
    Nothing -> error "Unknown error kind"

specializedMultisigContract
  :: forall a mainparam (e :: ErrorsKind) mname.
  (NiceMultisigParam a mainparam mname, Typeable e, ErrorHandler e)
  => EntrypointRef mname
  -> Contract (Parameter a mainparam) Storage
specializedMultisigContract epref = defaultContract $
  genericMultisigContractSimpleStorage @a @mainparam @e @mname $ do
    unpair
    dip $ do
      dip nil
      amount

    -- Convert TAddress into ContractRef
    dip $ do
      dip $ do
        contractCalling epref
        ifSome nop (failWithChoice @e #contractConversionError)

    transferTokens
    cons
