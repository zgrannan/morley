-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Lorentz implementation of generic multisig contract.
-- Based on the following Michelson implementation:
-- https://gitlab.com/nomadic-labs/mi-cho-coq/blob/8b65f3304b17153e5fa77ece4d67095ecbf14b9c/src/contracts/arthur/generic_multisig.tz
{-# OPTIONS_GHC -Wno-orphans #-}
module Lorentz.Contracts.Multisig.Generic
  ( Counter (..)
  , ErrorsKind (..)
  , ErrorHandler(..)
  , Keys (..)
  , ParamAction(..)
  , Parameter(..)
  , ParamMain
  , Signatures(..)
  , Storage
  , Threshold (..)
  , ToSign
  , contractToLambda
  , ensureChangeKeys
  , genericMultisigContract
  , mkStorage
  )
where

import Prelude (Show, bool, (==))

import GHC.Num (Num)

import Lorentz

import Michelson.Typed.Arith (Add)

data Parameter
  = Default ()
  | Main ParamMain
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

instance ParameterHasEntrypoints Parameter where
  type ParameterEntrypointsDerivation Parameter = EpdPlain

type Storage
  = (Counter, (Threshold, Keys))

newtype Counter = Counter Natural
  deriving stock Generic
  deriving stock Show
  deriving newtype (Num, IsoValue)
  deriving anyclass HasAnnotation

instance ArithOpHs Add Counter Counter where
  type ArithResHs Add Counter Counter = Counter

instance TypeHasDoc Counter where
  typeDocName _ = "Multisig.Counter"
  typeDocMdDescription =
    "Counter which prevents from applying same lambda twice. \
    \It increases on each succesfull multisig action."

newtype Threshold = Threshold Natural
  deriving stock Generic
  deriving stock Show
  deriving newtype (Num, IsoValue)
  deriving anyclass (HasAnnotation, Wrappable)

instance TypeHasDoc Threshold where
  typeDocName _ = "Multisig.Threshold"
  typeDocMdDescription =
    "Minimal amount of signatures required for multisig action."

instance ArithOpHs Add Threshold Threshold where
  type ArithResHs Add Threshold Threshold = Threshold

type ParamMain
  = ("payload" :! Payload, Signatures)

type Payload
  = (Counter, "action" :! ParamAction)

type ToSign = ((ChainId, Address), (Counter, ParamAction))

data ParamAction
  = Operation (Lambda () [Operation])
  | ChangeKeys (Threshold, Keys)
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

instance TypeHasDoc ParamAction where
  typeDocName _ = "Multisig.ParamAction"
  typeDocMdDescription =
    "Generic multisig contract parameter that can be used for either \
    \calling multisig action or changing the threshold and list of public \
    \key signers."

newtype Keys = Keys [PublicKey]
  deriving stock Generic
  deriving stock Show
  deriving newtype (IsoValue)
  deriving anyclass (HasAnnotation, Wrappable)

instance TypeHasDoc Keys where
  typeDocName _ = "Multisig.Keys"
  typeDocMdDescription =
    "Signer public keys."

newtype Signatures = Signatures [Maybe Signature]
  deriving stock Generic
  deriving newtype (IsoValue)
  deriving anyclass HasAnnotation

instance TypeHasDoc Signatures where
  typeDocName _ = "Multisig.Signatures"
  typeDocMdDescription =
    "Signatures obtained from the signers. Each signer can optionally provide \
    \a signature. Optional signatures in this list should be in the same order \
    \as the signers public keys in the contract storage."

-- | Function to create Lambda value from entrypoint call of the contract.
-- This lambda (paired with counter and multisig contract address)
-- later needs to be signed by the signers, obtained signatures
-- should be provided to multisig contract
contractToLambda
  :: forall epParameter. (NiceParameter epParameter, NiceConstant epParameter)
  => EpAddress -> epParameter -> Lambda () [Operation]
contractToLambda epAddr epParam = do
  drop
  push epAddr
  epAddressToContract
  if IsNone
  then failUsing [mt|Invalid contract type|]
  else do
    push $ toMutez 0
    push epParam
    stackType @(epParameter : Mutez : ContractRef epParameter : '[])
    transferTokens @epParameter
    dip nil
    cons

mkStorage :: Natural -> Natural -> [PublicKey] -> Storage
mkStorage counter threshold keys_ =
  (Counter counter, (Threshold threshold, Keys keys_))

-- | Generic multisig contract prohibits mutez transfer using not
-- `Default` entrypoint
type instance ErrorArg "nonZeroTransfer" = ()

-- | Error when provided counter doesn't
-- match with the counter in contract storage
type instance ErrorArg "counterDoesntMatch" = ()

-- | Error when provided signature doesn't match required public key
type instance ErrorArg "invalidSignature" = ()

-- | Error when number of provided signatures is less than number of
-- public keys
type instance ErrorArg "fewerSignaturesThanKeys" = ()

-- | Error when number of provided signatures is less than threshold
type instance ErrorArg "insufficientSignatures" = ()

-- | Error to throw when there are unused signatures left after check
type instance ErrorArg "uncheckedSignaturesLeft" = ()

-- | Error to throw when changing threshold to zero
type instance ErrorArg "zeroThresholdUpdate" = ()

-- | Error to throw when new threshold is larger that new keys size.
type instance ErrorArg "thresholdLargerThanKeysSize" = ()

instance CustomErrorHasDoc "nonZeroTransfer" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Unable to transfer tokens to the generic multisig contract \
    \using not default entrypoint."

instance CustomErrorHasDoc "counterDoesntMatch" where
  customErrClass = ErrClassActionException
  customErrDocMdCause = "Cannot perform multisig action because parameter counter \
                        \doesn't match with counter in storage."

instance CustomErrorHasDoc "invalidSignature" where
  customErrClass = ErrClassActionException
  customErrDocMdCause = "Failed to verify signature with the public key."

instance CustomErrorHasDoc "fewerSignaturesThanKeys" where
  customErrClass = ErrClassActionException
  customErrDocMdCause = "Provided less signatures than public keys."

instance CustomErrorHasDoc "insufficientSignatures" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Provided less signatures than it's required by the threshold."

instance CustomErrorHasDoc "uncheckedSignaturesLeft" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "There are unchecked signatures left after the checks."

instance CustomErrorHasDoc "zeroThresholdUpdate" where
  customErrClass = ErrClassActionException
  customErrDocMdCause =
    "Trying to update threshold to zero value."

instance CustomErrorHasDoc "thresholdLargerThanKeysSize" where
  customErrClass = ErrClassActionException
  customErrDocMdCause=
    "New threshold is larger than new keys list size. \
    \Contract is unable to perform any operations in this case."

data ErrorsKind = BaseErrors | CustomErrors

class ErrorHandler (k :: ErrorsKind) where
  handleNonZeroTransfer :: s :-> any
  handleCounterMismatch :: s :-> any
  handleInvalidSignature :: s :-> any
  handleFewerSignaturesThanKeys :: s :-> any
  handleInsufficientSignatures :: s :-> any
  handleUncheckedSignaturesLeft :: s :-> any
  handleZeroThresholdUpdate :: s :-> any
  handleThresholdLargerThanKeysSize :: s :-> any

instance ErrorHandler 'BaseErrors where
  handleNonZeroTransfer = unit # failWith
  handleCounterMismatch = unit # failWith
  handleInvalidSignature = unit # failWith
  handleFewerSignaturesThanKeys = unit # failWith
  handleInsufficientSignatures = unit # failWith
  handleUncheckedSignaturesLeft = unit # failWith
  handleZeroThresholdUpdate = unit # failWith
  handleThresholdLargerThanKeysSize = unit # failWith

instance ErrorHandler 'CustomErrors where
  handleNonZeroTransfer = failCustom_ #nonZeroTransfer
  handleCounterMismatch = failCustom_ #counterDoesntMatch
  handleInvalidSignature = failCustom_ #invalidSignature
  handleFewerSignaturesThanKeys = failCustom_ #fewerSignaturesThanKeys
  handleInsufficientSignatures = failCustom_ #insufficientSignatures
  handleUncheckedSignaturesLeft = failCustom_ #uncheckedSignaturesLeft
  handleZeroThresholdUpdate = failCustom_ #zeroThresholdUpdate
  handleThresholdLargerThanKeysSize = failCustom_ #thresholdLargerThanKeysSize

contractDoc :: Markdown
contractDoc =
  "This contract reimplements \
  \[generic multisig contract](https://gitlab.com/nomadic-labs/mi-cho-coq/blob/master/src/contracts/arthur/generic_multisig.tz) \
  \using Lorentz."

unpairWithVarAnnots :: Text -> Text -> (a, b) : s :-> a : b : s
unpairWithVarAnnots va1 va2 = do
  dup; bool (iWithVarAnnotations [va1] car) car (va1 == "")
  dip $ bool (iWithVarAnnotations [va2] cdr) cdr (va2 == "")

genericMultisigContract
  :: forall (e :: ErrorsKind). ErrorHandler e
  => Contract Parameter Storage
genericMultisigContract = defaultContract $ contractName "Generic multisig" $ do
  contractGeneralDefault
  docStorage @Storage
  doc $ DDescription contractDoc
  unpair
  entryCaseSimple @Parameter
    ( #cDefault /-> do
        doc $ DDescription
          "Entry point that can be used in order to transfer tokens to this contract."
        drop; nil; pair
    , #cMain /-> do
        forcedCoerce_
        doc $ DDescription
          "Entry point that can be used in order to run multisig action or \
          \change the threshold and signers public keys."
        push (toMutez 0)
        amount
        if IsEq then nop else handleNonZeroTransfer @e
        swap; dup; dip swap
        dip $ do
          unpairWithVarAnnots "counter" ""
          dup; selfCalling @Parameter CallDefault; address; chainId; pair; pair
          pack @ToSign
          dip $ do unpair; dip swap
          swap
        -- Check that the counters match
        unpairWithVarAnnots "stored_counter" ""; dip swap
        if IsEq then nop else handleCounterMismatch @e

        dip swap; unpairWithVarAnnots "threshold" "keys"
        dip $ do
          coerceUnwrap
          iWithVarAnnotations ["valid"] (push @Threshold 0); swap
          iter $ do
            dip swap; swap
            if IsCons
            then do
              if IsSome
              then do
                swap
                dip $ do
                  swap; dipN @2 $ (do dip dup; swap)
                  duupX @3; dip $ checkSignature; swap
                  -- Checks signature, fails if invalid
                  if_ drop (do drop; handleInvalidSignature @e)
                  push @Threshold 1; iWithVarAnnotations ["valid"] add
              else do swap; drop
            else handleFewerSignaturesThanKeys @e
            swap
        if IsLe then nop else handleInsufficientSignatures @e
        if IsCons then handleUncheckedSignaturesLeft @e else nop
        drop
        -- Increment counter
        dip $ do unpair; push @Counter 1; iWithVarAnnotations ["new_counter"] add; pair
        -- Produce operation
        caseT @ParamAction
          ( #cOperation /-> do
            unit; exec
          , #cChangeKeys /-> do
            stackType @'[(Threshold, Keys), (Counter, (Threshold, Keys))]
            dup; ensureChangeKeys @e
            dip car; swap; pair; nil
          )
        pair
    )

ensureChangeKeys
  :: forall (e :: ErrorsKind) s. ErrorHandler e
  => (Threshold, Keys) : s :-> s
ensureChangeKeys = do
  dup; car; push @Threshold 0
  if IsEq then handleZeroThresholdUpdate @e else nop
  unpair
  coerceUnwrap
  dip $ do coerceUnwrap; size
  if IsGt then handleThresholdLargerThanKeysSize @e else nop
