-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Michelson.Printer
  ( RenderDoc(..)
  , printDoc
  , printUntypedContract
  , printTypedContractCode
  , printTypedContract
  , printSomeContract
  , printTypedValue
  , printUntypedValue
  ) where

import Data.Constraint (withDict)
import Data.Singletons (SingI)
import qualified Data.Text.Lazy as TL

import Michelson.Printer.Util (RenderDoc(..), doesntNeedParens, printDoc)
import Michelson.TypeCheck.Types (SomeContract(..))
import qualified Michelson.Typed as T
import qualified Michelson.Untyped as U

-- | Convert an untyped contract into a textual representation which
-- will be accepted by the OCaml reference client.
printUntypedContract :: (RenderDoc op) => Bool -> U.Contract' op -> TL.Text
printUntypedContract forceSingleLine = printDoc forceSingleLine . renderDoc doesntNeedParens

-- | Convert a typed contract into a textual representation which
-- will be accepted by the OCaml reference client.
printTypedContractCode :: (SingI p, SingI s) => Bool -> T.ContractCode p s -> TL.Text
printTypedContractCode forceSingleLine =
  printUntypedContract forceSingleLine . T.convertContractCode

-- | Convert typed contract into a textual representation which
-- will be accepted by the OCaml reference client.
printTypedContract :: Bool -> T.Contract p s -> TL.Text
printTypedContract forceSingleLine fc@T.Contract{} =
  printUntypedContract forceSingleLine $ T.convertContract fc

-- | Convert typed value into a textual representation which
-- will be accepted by the OCaml reference client.
printTypedValue
  :: forall t.
      (T.ProperPrintedValBetterErrors t)
  => Bool -> T.Value t -> TL.Text
printTypedValue forceSingleLine =
  withDict (T.properPrintedValEvi @t) $
  printUntypedValue forceSingleLine . T.untypeValue

-- | Convert untyped value into a textual representation which
-- will be accepted by the OCaml reference client.
printUntypedValue :: (RenderDoc op) => Bool -> U.Value' op -> TL.Text
printUntypedValue forceSingleLine =
  printDoc forceSingleLine . renderDoc doesntNeedParens

-- | Convert 'SomeContract' into a textual representation which
-- will be accepted by the OCaml reference client.
printSomeContract :: Bool -> SomeContract -> TL.Text
printSomeContract forceSingleLine (SomeContract fc) =
  printTypedContract forceSingleLine fc
