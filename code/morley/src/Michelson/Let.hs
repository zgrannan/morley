-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Michelson.Let
  ( LetType (..)
  , LetValue (..)
  ) where

import Data.Aeson.TH (deriveJSON)
import qualified Data.Text as T

import Michelson.Macro (ParsedOp)
import Michelson.Untyped (Type, Value')
import Util.Aeson

-- | A programmer-defined constant
data LetValue = LetValue
  { lvName :: T.Text
  , lvSig :: Type
  , lvVal :: (Value' ParsedOp)
  } deriving stock (Eq, Show)

-- | A programmer-defined type-synonym
data LetType = LetType
  { ltName :: T.Text
  , ltSig :: Type
  } deriving stock (Eq, Show)

deriveJSON morleyAesonOptions ''LetValue
deriveJSON morleyAesonOptions ''LetType
