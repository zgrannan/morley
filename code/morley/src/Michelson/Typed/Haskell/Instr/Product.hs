-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-redundant-constraints #-}
{-# LANGUAGE InstanceSigs #-}

-- | Instructions working on product types derived from Haskell ones.
module Michelson.Typed.Haskell.Instr.Product
  ( InstrGetFieldC
  , InstrSetFieldC
  , InstrConstructC
  , instrGetField
  , instrSetField
  , instrConstruct
  , instrConstructStack
  , instrDeconstruct
  , InstrDeconstructC

  , GetFieldType
  , ConstructorFieldTypes
  , ConstructorFieldNames
  , FieldConstructor (..)
  , CastFieldConstructors (..)
  ) where

import qualified Data.Kind as Kind
import Data.Vinyl.Core (Rec(..))
import GHC.Generics ((:*:)(..), (:+:)(..))
import qualified GHC.Generics as G
import GHC.TypeLits (ErrorMessage(..), Symbol, TypeError)
import Named ((:!), (:?), NamedF(..))

import Michelson.Text
import Michelson.Typed.Haskell.Instr.Helpers
import Michelson.Typed.Haskell.Value
import Michelson.Typed.Instr
import Util.Label (Label)
import Util.Named (NamedInner)
import Util.Type

-- Fields lookup (helper)
----------------------------------------------------------------------------

-- | Result of field lookup - its type and path to it in
-- the tree.
data LookupNamedResult = LNR Kind.Type Path

type family LnrFieldType (lnr :: LookupNamedResult) where
  LnrFieldType ('LNR f _) = f
type family LnrBranch (lnr :: LookupNamedResult) :: Path where
  LnrBranch ('LNR _ p) = p

-- | Find field of some product type by its name.
--
-- Name might be either field record name, or one in 'NamedF' if
-- field is wrapped using '(:!)' or '(:?)'.
type GetNamed name a = LNRequireFound name a (GLookupNamed name (G.Rep a))

-- | Force result of 'GLookupNamed' to be 'Just'
type family LNRequireFound
  (name :: Symbol)
  (a :: Kind.Type)
  (mf :: Maybe LookupNamedResult)
    :: LookupNamedResult where
  LNRequireFound _ _ ('Just lnr) = lnr
  LNRequireFound name a 'Nothing = TypeError
    ('Text "Datatype " ':<>: 'ShowType a ':<>:
     'Text " has no field " ':<>: 'ShowType name)

type family GLookupNamed (name :: Symbol) (x :: Kind.Type -> Kind.Type)
          :: Maybe LookupNamedResult where
  GLookupNamed name (G.D1 _ x) = GLookupNamed name x
  GLookupNamed name (G.C1 _ x) = GLookupNamed name x

  GLookupNamed name (G.S1 ('G.MetaSel ('Just recName) _ _ _) (G.Rec0 a)) =
    If (name == recName)
      ('Just $ 'LNR a '[])
      'Nothing
  GLookupNamed name (G.S1 _ (G.Rec0 (NamedF f a fieldName))) =
    If (name == fieldName)
      ('Just $ 'LNR (NamedInner (NamedF f a fieldName)) '[])
      'Nothing
  GLookupNamed _ (G.S1 _ _) = 'Nothing

  GLookupNamed name (x :*: y) =
    LNMergeFound name (GLookupNamed name x) (GLookupNamed name y)
  GLookupNamed name (_ :+: _) = TypeError
    ('Text "Cannot seek for a field " ':<>: 'ShowType name ':<>:
     'Text " in sum type")
  GLookupNamed _ G.U1 = 'Nothing
  GLookupNamed _ G.V1 = TypeError
    ('Text "Cannot access fields of void-like type")

type family LNMergeFound
  (name :: Symbol)
  (f1 :: Maybe LookupNamedResult)
  (f2 :: Maybe LookupNamedResult)
    :: Maybe LookupNamedResult where
  LNMergeFound _ 'Nothing 'Nothing = 'Nothing
  LNMergeFound _ ('Just ('LNR a p)) 'Nothing = 'Just $ 'LNR a ('L ': p)
  LNMergeFound _ 'Nothing ('Just ('LNR a p)) = 'Just $ 'LNR a ('R ': p)
  LNMergeFound name ('Just _) ('Just _) = TypeError
    ('Text "Ambigous reference to datatype field: " ':<>: 'ShowType name)

-- | Get type of field by datatype it is contained in and field name.
type GetFieldType dt name = LnrFieldType (GetNamed name dt)

----------------------------------------------------------------------------
-- Value accessing instruction
----------------------------------------------------------------------------

-- | Make an instruction which accesses given field of the given datatype.
instrGetField
  :: forall dt name st.
     InstrGetFieldC dt name
  => Label name -> Instr (ToT dt ': st) (ToT (GetFieldType dt name) ': st)
instrGetField _ =
  gInstrGetField @name @(G.Rep dt) @(LnrBranch (GetNamed name dt))
    @(GetFieldType dt name)

-- | Constraint for 'instrGetField'.
type InstrGetFieldC dt name =
  ( GenericIsoValue dt
  , GInstrGet name (G.Rep dt)
      (LnrBranch (GetNamed name dt))
      (LnrFieldType (GetNamed name dt))
  )

{- Note about bulkiness of `instrGetField` type signature:

Read this only if you are going to modify the signature qualitatively.

It may seem that you can replace 'LnrBranch' and 'LnrFieldType' getters since
their result is already assigned to a type variable, but if you do so,
on attempt to access non-existing field GHC will prompt you a couple of huge
"cannot deduce type" errors along with desired "field is not present".
To make error messages clearer we prevent GHC from deducing 'GInstrAccess' when
no field is found via using those getters.
-}

-- | Generic traversal for 'instrAccess'.
class GIsoValue x =>
  GInstrGet
    (name :: Symbol)
    (x :: Kind.Type -> Kind.Type)
    (path :: Path)
    (fieldTy :: Kind.Type) where
  gInstrGetField
    :: GIsoValue x
    => Instr (GValueType x ': s) (ToT fieldTy ': s)

instance GInstrGet name x path f => GInstrGet name (G.M1 t i x) path f where
  gInstrGetField = gInstrGetField @name @x @path @f

instance (IsoValue f, ToT f ~ ToT f') =>
         GInstrGet name (G.Rec0 f) '[] f' where
  gInstrGetField = Nop

instance (GInstrGet name x path f, GIsoValue y) => GInstrGet name (x :*: y) ('L ': path) f where
  gInstrGetField = CAR `Seq` gInstrGetField @name @x @path @f

instance (GInstrGet name y path f, GIsoValue x) => GInstrGet name (x :*: y) ('R ': path) f where
  gInstrGetField = CDR `Seq` gInstrGetField @name @y @path @f

-- Examples
----------------------------------------------------------------------------

type MyType1 = ("int" :! Integer, "bytes" :! ByteString, "bytes2" :? ByteString)

_getIntInstr1 :: Instr (ToT MyType1 ': s) (ToT Integer ': s)
_getIntInstr1 = instrGetField @MyType1 #int

_getTextInstr1 :: Instr (ToT MyType1 ': s) (ToT (Maybe ByteString) ': s)
_getTextInstr1 = instrGetField @MyType1 #bytes2

data MyType2 = MyType2
  { getInt :: Integer
  , getText :: MText
  , getUnit :: ()
  , getMyType1 :: MyType1
  } deriving stock (Generic)
    deriving anyclass (IsoValue)

_getIntInstr2 :: Instr (ToT MyType2 ': s) (ToT () ': s)
_getIntInstr2 = instrGetField @MyType2 #getUnit

_getIntInstr2' :: Instr (ToT MyType2 ': s) (ToT Integer ': s)
_getIntInstr2' =
  instrGetField @MyType2 #getMyType1 `Seq` instrGetField @MyType1 #int

----------------------------------------------------------------------------
-- Value modification instruction
----------------------------------------------------------------------------

-- | For given complex type @dt@ and its field @fieldTy@ update the field value.
instrSetField
  :: forall dt name st.
     InstrSetFieldC dt name
  => Label name -> Instr (ToT (GetFieldType dt name) ': ToT dt ': st) (ToT dt ': st)
instrSetField _ =
  gInstrSetField @name @(G.Rep dt) @(LnrBranch (GetNamed name dt))
    @(GetFieldType dt name)

-- | Constraint for 'instrSetField'.
type InstrSetFieldC dt name =
  ( GenericIsoValue dt
  , GInstrSetField name (G.Rep dt)
      (LnrBranch (GetNamed name dt))
      (LnrFieldType (GetNamed name dt))
  )

-- | Generic traversal for 'instrSetField'.
class GIsoValue x =>
  GInstrSetField
    (name :: Symbol)
    (x :: Kind.Type -> Kind.Type)
    (path :: Path)
    (fieldTy :: Kind.Type) where
  gInstrSetField
    :: Instr (ToT fieldTy ': GValueType x ': s) (GValueType x ': s)

instance GInstrSetField name x path f => GInstrSetField name (G.M1 t i x) path f where
  gInstrSetField = gInstrSetField @name @x @path @f

instance (IsoValue f, ToT f ~ ToT f') =>
         GInstrSetField name (G.Rec0 f) '[] f' where
  gInstrSetField = DIP DROP

instance (GInstrSetField name x path f, GIsoValue y) => GInstrSetField name (x :*: y) ('L ': path) f where
  gInstrSetField =
    DIP (DUP `Seq` DIP CDR `Seq` CAR) `Seq`
    gInstrSetField @name @x @path @f `Seq`
    PAIR

instance (GInstrSetField name y path f, GIsoValue x) => GInstrSetField name (x :*: y) ('R ': path) f where
  gInstrSetField =
    DIP (DUP `Seq` DIP CAR `Seq` CDR) `Seq`
    gInstrSetField @name @y @path @f `Seq`
    SWAP `Seq` PAIR

-- Examples
----------------------------------------------------------------------------

_setIntInstr1 :: Instr (ToT Integer ': ToT MyType2 ': s) (ToT MyType2 ': s)
_setIntInstr1 = instrSetField @MyType2 #getInt

----------------------------------------------------------------------------
-- Value construction instruction
----------------------------------------------------------------------------

-- | Way to construct one of the fields in a complex datatype.
newtype FieldConstructor (st :: [k]) (field :: Kind.Type) =
  FieldConstructor (Instr (ToTs' st) (ToT field ': ToTs' st))

-- | Ability to pass list of fields with the same ToTs.
-- It may be useful if you don't want to work with NamedF in ConstructorFieldTypes.
class ToTs xs ~ ToTs ys => CastFieldConstructors xs ys where
  castFieldConstructorsImpl :: Rec (FieldConstructor st) xs -> Rec (FieldConstructor st) ys

instance CastFieldConstructors '[] '[] where
  castFieldConstructorsImpl RNil = RNil

instance (CastFieldConstructors xs ys, ToTs xs ~ ToTs ys, ToT x ~ ToT y)
    => CastFieldConstructors (x ': xs) (y ': ys) where
  castFieldConstructorsImpl (FieldConstructor fctr :& xs) =
    FieldConstructor fctr :& (castFieldConstructorsImpl @xs @ys xs)

-- | For given complex type @dt@ and its field @fieldTy@ update the field value.
instrConstruct
  :: forall dt st. InstrConstructC dt
  => Rec (FieldConstructor st) (ConstructorFieldTypes dt)
  -> Instr st (ToT dt ': st)
instrConstruct = gInstrConstruct @(G.Rep dt)

instrConstructStack
  :: forall dt stack st .
  ( InstrConstructC dt
  , stack ~ ToTs (ConstructorFieldTypes dt)
  , KnownList stack
  )
  => Instr (stack ++ st) (ToT dt ': st)
instrConstructStack =
  FrameInstr (Proxy @st) (gInstrConstructStack @(G.Rep dt))

-- | Types of all fields in a datatype.
type ConstructorFieldTypes dt = GFieldTypes (G.Rep dt)

-- | Names of all fields in a datatype.
type ConstructorFieldNames dt = GFieldNames (G.Rep dt)

-- | Constraint for 'instrConstruct' and 'gInstrConstructStack'.
type InstrConstructC dt = (GenericIsoValue dt, GInstrConstruct (G.Rep dt))

-- | Retrieve field names of a constructor.
type family GFieldNames x :: [Symbol] where
  GFieldNames (G.D1 _ x) = GFieldNames x
  GFieldNames (G.C1 _ x) = GFieldNames x
  GFieldNames (x :*: y) = GFieldNames x ++ GFieldNames y
  GFieldNames (G.S1 ('G.MetaSel ('Just fieldName) _ _ _) _) = '[fieldName]
  GFieldNames (G.S1 _ (G.Rec0 (NamedF _ _ fieldName))) = '[fieldName]

  GFieldNames (G.S1 ('G.MetaSel _ _ _ _) _) = TypeError ('Text "All fields have to be named")
  GFieldNames (_ :+: _) = TypeError ('Text "Cannot get field names of sum type")
  GFieldNames G.V1 = TypeError ('Text "Must be at least one constructor")

-- | Generic traversal for 'instrConstruct'.
class GIsoValue x => GInstrConstruct (x :: Kind.Type -> Kind.Type) where
  type GFieldTypes x :: [Kind.Type]
  gInstrConstruct :: Rec (FieldConstructor st) (GFieldTypes x) -> Instr st (GValueType x ': st)
  gInstrConstructStack :: Instr (ToTs (GFieldTypes x)) '[GValueType x]

instance GInstrConstruct x => GInstrConstruct (G.M1 t i x) where
  type GFieldTypes (G.M1 t i x) = GFieldTypes x
  gInstrConstruct = gInstrConstruct @x
  gInstrConstructStack = gInstrConstructStack @x

instance ( GInstrConstruct x, GInstrConstruct y
         , RSplit (GFieldTypes x) (GFieldTypes y)
         , KnownList (ToTs (GFieldTypes x)), KnownList (ToTs (GFieldTypes y))
         , ToTs (GFieldTypes x) ++ ToTs (GFieldTypes y) ~ ToTs (GFieldTypes x ++ GFieldTypes y)
         ) => GInstrConstruct (x :*: y) where
  type GFieldTypes (x :*: y) = GFieldTypes x ++ GFieldTypes y
  gInstrConstruct fs =
    let (lfs, rfs) = rsplit fs
        linstr = gInstrConstruct @x lfs
        rinstr = gInstrConstruct @y rfs
    in linstr `Seq` DIP rinstr `Seq` PAIR

  gInstrConstructStack =
    let linstr = gInstrConstructStack @x
        rinstr = gInstrConstructStack @y
    in FrameInstr (Proxy @(ToTs (GFieldTypes y))) linstr `Seq` DIP rinstr `Seq` PAIR

instance GInstrConstruct G.U1 where
  type GFieldTypes G.U1 = '[]
  gInstrConstruct RNil = UNIT
  gInstrConstructStack = UNIT

instance ( TypeError ('Text "Cannot construct sum type")
         , GIsoValue x, GIsoValue y
         ) => GInstrConstruct (x :+: y) where
  type GFieldTypes (x :+: y) = '[]
  gInstrConstruct = error "impossible"
  gInstrConstructStack = error "impossible"

instance IsoValue a => GInstrConstruct (G.Rec0 a) where
  type GFieldTypes (G.Rec0 a) = '[a]
  gInstrConstruct (FieldConstructor i :& RNil) = i
  gInstrConstructStack = Nop

-- Examples
----------------------------------------------------------------------------

_constructInstr1 :: Instr (ToT MyType1 ': s) (ToT MyType2 ': ToT MyType1 ': s)
_constructInstr1 =
  instrConstruct @MyType2 $
    FieldConstructor (PUSH (toVal @Integer 5)) :&
    FieldConstructor (PUSH (toVal @MText [mt||])) :&
    FieldConstructor UNIT :&
    FieldConstructor DUP :&
    RNil

_constructInstr2 :: Instr s (ToT MyType1 ': s)
_constructInstr2 =
  instrConstruct @MyType1 $
    FieldConstructor (PUSH (toVal @Integer 5)) :&
    FieldConstructor (PUSH (toVal @ByteString "bytestring1")) :&
    FieldConstructor (PUSH (toVal @(Maybe ByteString) (Just "bytestring2"))) :&
    RNil

----------------------------------------------------------------------------
-- Value deconstruction
----------------------------------------------------------------------------

-- | Constraint for 'instrConstruct'.
type InstrDeconstructC dt = (GenericIsoValue dt, GInstrDeconstruct (G.Rep dt))

-- | For given complex type @dt@ deconstruct it to its field types.
instrDeconstruct
  :: forall dt stack st .
  (InstrDeconstructC dt
  , stack ~ ToTs (ConstructorFieldTypes dt)
  , KnownList stack
  )
  => Instr (ToT dt ': st) (stack ++ st)
instrDeconstruct = FrameInstr (Proxy @st) $ gInstrDeconstruct @(G.Rep dt)

-- | Generic traversal for 'instrDeconstruct'.
class GIsoValue x => GInstrDeconstruct (x :: Kind.Type -> Kind.Type) where
  gInstrDeconstruct :: Instr '[GValueType x] (ToTs (GFieldTypes x))

instance GInstrDeconstruct x => GInstrDeconstruct (G.M1 t i x) where
  gInstrDeconstruct = gInstrDeconstruct @x

instance ( GInstrDeconstruct x, GInstrDeconstruct y
         , t ~ (x :*: y)
         , KnownList (ToTs (GFieldTypes x)), KnownList (ToTs (GFieldTypes y))
         , ToTs (GFieldTypes x) ++ ToTs (GFieldTypes y) ~ ToTs (GFieldTypes x ++ GFieldTypes y)
         ) => GInstrDeconstruct (x :*: y) where
  gInstrDeconstruct =
    let linstr = gInstrDeconstruct @x
        rinstr = gInstrDeconstruct @y
    in UNPAIR `Seq` DIP rinstr `Seq` FrameInstr (Proxy @(ToTs (GFieldTypes y))) linstr

instance GInstrDeconstruct G.U1 where
  gInstrDeconstruct = DROP

instance ( TypeError ('Text "Cannot deconstruct sum type")
         , GIsoValue x, GIsoValue y
         ) => GInstrDeconstruct (x :+: y) where
  gInstrDeconstruct = error "impossible"

instance IsoValue a => GInstrDeconstruct (G.Rec0 a) where
  gInstrDeconstruct = Nop
