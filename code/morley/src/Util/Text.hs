-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Util.Text
  ( headToLower
  , surround
  ) where

import Data.Char (toLower)
import qualified Data.Text as T

-- | Leads first character of text to lower case.
--
-- For empty text this will throw an error.
headToLower :: HasCallStack => Text -> Text
headToLower txt = case T.uncons txt of
  Nothing -> error "Empty text"
  Just (c, cs) -> T.cons (toLower c) cs

surround :: Semigroup a => a -> a -> a -> a
surround pre post content = pre <> content <> post
