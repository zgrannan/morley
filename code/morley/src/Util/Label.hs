-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Definition of the Label type and utilities
module Util.Label
  ( -- * Definitions
    Label (..)

  -- * Utilities
  , labelToText

  -- * Re-exports
  , IsLabel (..)
  ) where

import Fmt (Buildable(..), pretty)
import Text.Show (Show(..))

import Util.TypeLits

--------------------------------------------------------------------------------
-- Definitions
--------------------------------------------------------------------------------

-- | Proxy for a label type that includes the 'KnownSymbol' constraint
data Label (name :: Symbol) where
  Label :: KnownSymbol name => Label name

deriving stock instance Eq (Label name)

instance Show (Label name) where
  show label = "Label " <> pretty label

instance (KnownSymbol name, s ~ name) => IsLabel s (Label name) where
  fromLabel = Label

instance Buildable (Label name) where
  build Label = build $ symbolVal (Proxy @name)

--------------------------------------------------------------------------------
-- Utilities
--------------------------------------------------------------------------------

-- | Utility function to get the 'Text' representation of a 'Label'
labelToText :: Label name -> Text
labelToText = pretty
