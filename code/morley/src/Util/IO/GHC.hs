-- SPDX-FileCopyrightText: 2002 The University Court of the University of Glasgow
--
-- SPDX-License-Identifier: LicenseRef-BSD-3-Clause-GHC

module Util.IO.GHC
  ( hSetTranslit
  ) where

import GHC.IO.Encoding (textEncodingName)
import System.IO (hGetEncoding, hSetEncoding, mkTextEncoding)


-- This function was copied (with slight modifications) from
-- <https://gitlab.haskell.org/ghc/ghc/blob/7105fb66a7bacf822f7f23028136f89ff5737d0e/libraries/ghc-boot/GHC/HandleEncoding.hs>

-- | Change the character encoding of the given Handle to transliterate
-- on unsupported characters instead of throwing an exception.
hSetTranslit :: Handle -> IO ()
hSetTranslit h = do
    menc <- hGetEncoding h
    case fmap textEncodingName menc of
        Just name | '/' `notElem` name -> do
            enc' <- mkTextEncoding $ name ++ "//TRANSLIT"
            hSetEncoding h enc'
        _ -> pass
