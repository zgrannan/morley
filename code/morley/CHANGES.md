Unreleased
==========
<!-- Append new entries here -->
* [!323](https://gitlab.com/morley-framework/morley/-/merge_requests/323)
  Add `parseSecretKey` which allows parsing all types of `SecretKey`.
* [!537](https://gitlab.com/morley-framework/morley/-/merge_requests/537)
  Permit `SELF %default` instruction.
* [!522](https://gitlab.com/morley-framework/morley/-/merge_requests/522)
  + allow calling the interpreter with a typed transfer parameter and
  avoid unnecessary typechecking.
* [!495](https://gitlab.com/morley-framework/morley/-/merge_requests/495)
  Add source location to typed `Instr` AST.
* [!521](https://gitlab.com/morley-framework/morley/-/merge_requests/521)
  Document generater can now generate table of contents.

1.5.0
=====
* [!509](https://gitlab.com/morley-framework/morley/-/merge_requests/509)
  Implement missing parts of `secp256k1` and `P256`.
  + `checkSignature` now works correctly with all signature formats.
* [!511](https://gitlab.com/morley-framework/morley/-/merge_requests/511)
  Add `zeroMutez` and `oneMutez` for safe initial values.
* [!500](https://gitlab.com/morley-framework/morley/-/merge_requests/500)
  Add ability to parse optimized formats of `signature`, `contract`, `key`, and `key_hash`.
* [!486](https://gitlab.com/morley-framework/morley/-/merge_requests/486)
  Add `Util.CustomGeneric` to derive `Generic` instances with a custom tree structure.
* [!466](https://gitlab.com/morley-framework/morley/-/merge_requests/466)
  Add sub-command 'repl' to Morley executable which starts an REPL that
  can execute Morley instructions.
* [!492](https://gitlab.com/morley-framework/morley/-/merge_requests/492)
  + `TypeHasDoc` instance for `newtype` now has its field remove by default
  without having to use `haskellRepNoFields`.
  + Add `haskellAddNewtypeField` function to add field name for `newtype`
  if desired.
* [!487](https://gitlab.com/morley-framework/morley/-/merge_requests/487)
  Fix typechecking error message pointing to the whole `map unit nat` as not comprable.
  + It now only points to `unit` type as not comparable.
* [!457](https://gitlab.com/morley-framework/morley/-/merge_requests/457)
  + Allow using the same position when defining `DocItem`.
  + When two `DocItem`s have the same position, they will be sorted alphabetically by their names.
* [!480](https://gitlab.com/morley-framework/morley/-/merge_requests/480)
  Arithmetic instructions (ADD, MUL, EDIV, LSL, ...) now take annotations into account.
    + Results from the instructions are also properly annotated.
* [!470](https://gitlab.com/morley-framework/morley/-/merge_requests/470)
  Move `Michelson.Doc.Test`, `Michelson.Test` and  Hedgehog generators to
  a new package: `cleveland`.
* [!453](https://gitlab.com/morley-framework/morley/-/merge_requests/453)
  Order of top-level entries in contract are now preserved.
  + `PACK`/`UNPACK` now serialize contracts according their top-level entries order.
  + Pretty printer now display contracts according to their given top-level entries order.
* [!445](https://gitlab.com/morley-framework/morley/merge_requests/445)
  Remove unnecessary `TcOriginatedContracts` usages and split `typeCheckTopLevelValue`
  to `typeCheckParameter` (the only case where `TcOriginatedContracts` is useful)
  and `typeCheckStorage`.

1.4.0
=====
* [!446](https://gitlab.com/morley-framework/morley/-/merge_requests/446)
  Include source locations and let-binding stacks into typecheck errors.
* [!449](https://gitlab.com/morley-framework/morley/-/merge_requests/449)
  + Improve `Michelson.Test.Integrational` interface.
  + Remove `IntegrationalValidator`, `SuccessValidator`, `validate`,
    `composeValidators`, `composeValidatorsList`, `expectAnySuccess`.
  + Add `unexpectedInterpreterError`, `attempt`, `catchExpectedError`.
  + Change type of `expect*`/`tExpect*` functions.
    They now return `IntegrationalScenario` instead of `SuccessValidator` / `Bool`.
  + Rename `ValidationError` to `TestError`
  + Replace `TestError`'s `UnexpectedExecutorError` constructor with
    `InterpreterError` and `UnexpectedInterpreterError`.
* [!441](https://gitlab.com/morley-framework/morley/merge_requests/441)
  Remove `tezos-bake-monitor-lib dependency`, and define value to json conversion
  using binary round trip locally instead.
* [!448](https://gitlab.com/morley-framework/morley/merge_requests/448)
  Remove `base-noprelude` dependency and use mixins to hide `Prelude` instead.
* [!367](https://gitlab.com/morley-framework/morley/merge_requests/367)
  Values and instructions now carry some extra constraints in order to avoid
  construction of illegally typed values from within Haskell.
  `WellTyped` now also requires `Typeable` and `SingI` constraints.
* [!409](https://gitlab.com/morley-framework/morley/-/merge_requests/409)
  + Add hedgehog support
* [!438](https://gitlab.com/morley-framework/morley/-/merge_requests/438)
  + Fixed parsing bug: support semicolons after block comments.

* [!424](https://gitlab.com/morley-framework/morley/-/merge_requests/424) Changed
  contract address computation logic in the interpreter to match the
  reference implementation a bit more closely by including a global counter and
  an origination index (nonce) to contract's hash. Specifically, now the same
  contract may be originated several times, leading to different addresses,
  just like in Tezos.

1.3.0
=====
* [!417](https://gitlab.com/morley-framework/morley/-/merge_requests/417)
Change the license to MIT.
* [!386](https://gitlab.com/morley-framework/morley/-/merge_requests/386)
  + Add a way to describe constructors and fields of ADTs in Markdown documentation.
  + Require that all contracts include documentation for the storage type.
* [!396](https://gitlab.com/morley-framework/morley/merge_requests/396) Fixed
  typed `Notes` to permit pair annotations for container keys.
* [!359](https://gitlab.com/morley-framework/morley/merge_requests/359) Renamed
  'FullContract' to 'Contract', removed the prefix `f` from its record fields
  and removed "Full" from the names of the functions:
  `convertFullContract`, `printTypedFullContract` and `mapFullContractCode`.
* [!354](https://gitlab.com/morley-framework/morley/-/merge_requests/354)
  + Fix bug: add `ChainId` to `IsPrimitiveValue` type family.
  + Add `GInstrDeconstruct` type class and related functions
    which serve to deconstruct datatype into its fields on the stack.
  + Add `gInstrConstructStack` to `GInstrConstruct` type class that serves
    to construct a datatype from its fields on the stack.
  + Add `GFieldNames` type family returning list of `Symbol`s:
    the names of direct fields of datatype.

1.2.0
=====
* [!343](https://gitlab.com/morley-framework/morley/-/merge_requests/343)
  + Make pairs comparable and available to use as map keys and in sets.
  + CT type has been integrated into T.
  + A `WellTyped` type class has been added to ensure that a type
    represented by  T is valid as per Michelson's rules.
* [!358](https://gitlab.com/morley-framework/morley/-/merge_requests/358)
  Remove deprecated `STEPS_TO_QUOTA` instruction.
* [!368](https://gitlab.com/morley-framework/morley/-/merge_requests/368)
  + Update JSON encoding of types.
  + Rename fields of the untyped `Contract'`.
* [!286](https://gitlab.com/morley-framework/morley/-/merge_requests/286)
  Morley supports field annotations for parameter root now.
* [!344](https://gitlab.com/morley-framework/morley/-/merge_requests/344)
  Fixed a bug in `Michelson.Analyzer` and added `linearizeLeftDeep`.

1.1.0
=====
* [!337](https://gitlab.com/morley-framework/morley/-/merge_requests/337)
  Added an extension for Michelson comment to typed and untyped contract representation.
* [!306](https://gitlab.com/morley-framework/morley/-/merge_requests/306)
  Added PAIR/UNPAIR rule to optimizer.
* [!314](https://gitlab.com/morley-framework/morley/-/merge_requests/314)
  Fixed a bug in the implementation of `MAP` operation: it did not preserve modifications to
  stack.
* [!261](https://gitlab.com/morley-framework/morley/merge_requests/261)
  Slightly modified interpreter API.
* [!313](https://gitlab.com/morley-framework/morley/-/merge_requests/313)
  Made `typeCheckValue` polymorphic in desired type, instead of using
  existential wrappers.
* [!310](https://gitlab.com/morley-framework/morley/merge_requests/232)
  + Add `DGeneralInfoSection` documentation section with git revision and
    potentially other info.
  + Add `buildLorentzDocWithGitRev` and `buildInstrDocWithGitRev` to
    API to add a git revision to contract docs from the executable.
* [!121](https://gitlab.com/morley-framework/morley/merge_requests/302)
  `BALANCE` instruction now returns the balance with funds from incoming
  transaction.
* [!294](https://gitlab.com/morley-framework/morley/-/merge_requests/294)
  + Added `Paths_*` modules to `autogen-modules` in cabal files.  Removed `-O0`
  + from default GHC options. Please set `ghc-options` in your `stack.yaml` or
  `cabal.project.local`.
* [!271](https://gitlab.com/morley-framework/morley/merge_requests/271) Renamed
  'Contract' to 'ContractCode', and appended "Code" to the names of two functions:
  'convertContract' and 'printTypedContract'
* [!278](https://gitlab.com/morley-framework/morley/merge_requests/278)
  Added some utilities for command line option parsing, see `Util.CLI` and `Morley.CLI` modules.
* [!268](https://gitlab.com/morley-framework/morley/merge_requests/268)
  Test functions which import typed contract now return `FullContract` instead
  of `Contract`, thus preserving parameter and storage annotations. In case you
  don't need this behaviour, use `fcCode` for conversion.
  Test functions which import Lorentz contracts have been removed because they
  cannot be implemented sanely, and Lorentz is assumed to be used to generate
  code, do not use it for work with textual Michelson contracts.
* [!212](https://gitlab.com/morley-framework/morley/merge_requests/212)
  + Fix `AND` instruction return type.
  + Add `DUP n` macro support.
  + Fix `LAMBDA` instruction printer.
* [!265](https://gitlab.com/morley-framework/morley/merge_requests/265)
  The semicolons between instructions are now optional.

1.0.0
=====

* [!215](https://gitlab.com/morley-framework/morley/merge_requests/215)
  Major change: all Lorentz functionality was moved into `lorentz` package.
  A small portion of testing library code was moved around (from `Lorentz.*` to `Michelson.*` or vice versa).

0.7.0
=====

* [!237](https://gitlab.com/morley-framework/morley/merge_requests/237)
  The `option (nat %foo)` is now accepted syntax. The `%foo` is ignored.
* [!241](https://gitlab.com/morley-framework/morley/merge_requests/241)
  Derive `Show` instance for `Void_`.
* [!238](https://gitlab.com/morley-framework/morley/merge_requests/238)
  + `NoExplicitDefaultEntrypoint` is now GHC-understandable constraint,
    for previous behaviour with nice error message use
    `ForbidExplicitDefaultEntrypoint`.
  + `CanHaveEntrypoints` is made stricter, now it returns true only for
     Michelson sum types. If that's a problem for you, consider using
     `ShouldHaveEntrypoints` wrapper.
  + Added pseudo `Empty` type.
* [!219](https://gitlab.com/morley-framework/morley/merge_requests/219)
  Various entrypoints-related updates.
  + Integrational tests now carry `TAddress` as a reference to contract.
    Make sure that parameters of the originated contracts are clear;
    e.g. origination of `consumer` contract may require explicit type
    annotation to specify parameter type.
    If you passed a reference to the originated contract to `View` constructor,
    consider using `mkView` instead or applying `callingDefTAddress` to the
    reference.
  + Generally it is encouraged to use `TAddress` instead of `Address`
    where it is used to refer to a contract.
  + `fromContractAddr` renamed to `fromContractRef`.
  + `Default` instances for `{Some,}EntrypointCall` are removed, use
     one of explicit versions: `epcPrimitive`, `epcCallRootUnsafe`.
  + Reworked `FutureContract` type.

0.6.0
=====

* [!127](https://gitlab.com/morley-framework/morley/merge_requests/127)
  Limit annotations' allowed characters
* [!184](https://gitlab.com/morley-framework/morley/merge_requests/184)
  Fix `LAMBDA` instruction type converter bug
* [!173](https://gitlab.com/morley-framework/morley/merge_requests/173)
  [!197](https://gitlab.com/morley-framework/morley/merge_requests/197)
  Add support for lightweight entrypoints in Lorentz, as well as in
  the interpreter runner and morley executable.
  Support entrypoint references in `SELF` instruction.
  Minor refactorings related to entrypoints.
* [!201](https://gitlab.com/morley-framework/morley/merge_requests/201)
  + Add entrypoint argument to methods in testing eDSL. Preserve the existing type of `lCall` and deprecate it.
  + Deprecate `Lorentz.TestScenario`.
* [!208](https://gitlab.com/morley-framework/morley/merge_requests/208)
  Resolve an ambiguity within integrational and unit interpreters
  by renaming interpreter in `Michelson.Runtime` to `executor`.
* [!180](https://gitlab.com/morley-framework/morley/merge_requests/180)
  Remove `coerce_` in favor of more safe variants.
  Migration guide: pick one of the functions from 'Lorentz.Coercions' module.
  In most cases you need one of the following ones:
    * `checkedCoerce_`
    * `coerceWrap` / `coerceUnwrap`
    * `forcedCoerce_` (aka old `coerce_` - this variant provides the least safety).
* [!180](https://gitlab.com/morley-framework/morley/merge_requests/180)
  Type arguments of `coerceWrap` and `coerceUnwrap` are flipped.
  This change concerns you if you used type application on these functions.

0.5.0
=====

* Require `morley-prelude` to be ≥ 0.3.0 to make Hackage happy.
* [!156](https://gitlab.com/morley-framework/morley/merge_requests/156)
  Consider annotations in PACK and UNPACK.

0.4.0
=====

* Implemented most of Babylon changes: new instructions, elimination of `big_map` restructions, new restrictions for the `contract` type, partial support for entrypoints. Some instructions have been removed/deprecated.
* Many updates of helper data types for upgradeable contracts: `UParam` and `UStore`.
* Michelson printer can produce pretty output, not just one line.
* Added utilities for contract processing and analysis: optimizer, string transformer, errors transformer, analyzer.
* Added `tasty` helpers to the testing engine.
* Added annotations to typed representation and Lorentz.
* Added automatic documentation generator.
* Modified standard errors in Lorentz: now they are based on `CustomError`.
* Added unpacking from readable representation.
* Removed `parameter` and `storage` aliases, they were not very useful and complicated the code.
* Extended cryptographic types and `Address` to work with other curves (tz2 and tz3 addresses).
* Made it nearly impossible to generate dead code (which is illegal in Michelson) using Lorentz.
* Various bug fixes.

0.3.0.1
=======

* Update maintainer.

0.3.0
=====

* [TM-68](https://issues.serokell.io/issue/TM-68) Lorentz DSL which allows one to write contracts directly in Haskell.
May be moved to a separate package later.
* [TM-132](https://issues.serokell.io/issue/TM-132) Names for contracts in integrational tests.
* [TM-35](https://issues.serokell.io/issue/TM-35) `PACK` and `UNPACK` instructions.
* [TM-27](https://issues.serokell.io/issue/TM-27) Proper handling of `FAILWITH`.
* [TM-44](https://issues.serokell.io/issue/TM-44) [TM-124](https://issues.serokell.io/issue/TM-124) Reorganization of modules.
* Bug fixes.

0.2.0.1
=======

* Update documentation and metadata.

0.2.0
=====

Initial release.

* Typechecker and interpreter for Michelson.
* Morley extensions:
  - syntax sugar
  - let-blocks
  - inline assertions
* EDSL for unit testing and integrational testing
