-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Morley client initialization.
module Morley.Client.Init
  ( MorleyClientConfig (..)
  , mkMorleyClientEnv
  ) where

import Colog (cmap, fmtMessage, logTextStderr, msgSeverity)
import Colog.Core (Severity(..), filterBySeverity)
import System.Environment (lookupEnv)

import Morley.Client.Env
import Morley.Client.Logging (logFlush)
import Morley.Client.RPC.HttpClient
import Morley.Client.TezosClient.Impl (getTezosClientConfig)
import Morley.Client.TezosClient.Types
import qualified Tezos.Crypto.Ed25519 as Ed25519

-- | Data necessary for morley client initialization.
data MorleyClientConfig = MorleyClientConfig
  { mccAliasPrefix :: Maybe Text
  -- ^ Optional prefix for aliases that will be passed to @tezos-client@.
  , mccNodeAddress :: Maybe Text
  -- ^ Address of tezos node on which operations are performed
  , mccNodePort :: Maybe Word16
  -- ^ Port of tezos node
  , mccTezosClientPath :: FilePath
  -- ^ Path to @tezos-client@ binary through which operations are
  -- performed
  , mccMbTezosClientDataDir :: Maybe FilePath
  -- ^ Path to @tezos-client@ data directory.
  , mccNodeUseHttps :: Bool
  -- ^ If it is 'True', TLS will be forced, otherwise the setting from
  -- @tezos-client@ config will be used.
  , mccVerbosity :: Word
  -- ^ Verbosity level. @0@ means that only important messages will be
  -- printed. The greater this value is, the more messages will be
  -- printed during execution. After some small unspecified limit
  -- increasing this value does not change anything.
  , mccSecretKey :: Maybe Ed25519.SecretKey
  -- ^ Custom secret key to use for signing.
  } deriving stock Show

-- | Construct 'MorleyClientEnv'.
--
-- * @tezos-client@ path is taken from 'MorleyClientConfig', but can be
-- overridden using @MORLEY_TEZOS_CLIENT@ environment variable.
-- * Node data is taken from @tezos-client@ config and can be overridden
-- by 'MorleyClientConfig'.
-- * The rest is taken from 'MorleyClientConfig' as is.
mkMorleyClientEnv :: MonadIO m => MorleyClientConfig -> IO (MorleyClientEnv' m)
mkMorleyClientEnv MorleyClientConfig {..} = do
  envTezosClientPath <- lookupEnv "MORLEY_TEZOS_CLIENT"
  let tezosClientPath = fromMaybe mccTezosClientPath envTezosClientPath
  TezosClientConfig {..} <- getTezosClientConfig tezosClientPath mccMbTezosClientDataDir
  let
    nodeAddress = fromMaybe tcNodeAddr mccNodeAddress
    nodePort = fromMaybe tcNodePort mccNodePort
    useHttps = tcUseTls || mccNodeUseHttps
    tezosClientEnv = TezosClientEnv
      { tceAliasPrefix = mccAliasPrefix
      , tceNodeAddress = nodeAddress
      , tceNodePort = nodePort
      , tceTezosClientPath = tezosClientPath
      , tceMbTezosClientDataDir = mccMbTezosClientDataDir
      , tceNodeUseHttps = useHttps
      }
  let
    severity = case mccVerbosity of
      0 -> Warning
      1 -> Info
      _ -> Debug
    logTextStderrFlush = logTextStderr <> logFlush stderr
    logAction =
      filterBySeverity severity msgSeverity (fmtMessage `cmap` logTextStderrFlush)

  clientEnv <- newClientEnv nodeAddress nodePort useHttps
  pure MorleyClientEnv
    { mceTezosClient = tezosClientEnv
    , mceLogAction = logAction
    , mceSecretKey = mccSecretKey
    , mceClientEnv = clientEnv
    }
