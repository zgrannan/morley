-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Client.RPC.HttpClient
  ( newClientEnv
  ) where

import Network.HTTP.Client (ManagerSettings(..), Request(..), defaultManagerSettings, newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant.Client (BaseUrl(..), ClientEnv, Scheme(..), mkClientEnv)

-- | Make servant client environment from morley client config.
--
-- Note: Creating a new servant manager is a relatively expensive
-- operation, so this function is not supposed to be called often.
newClientEnv :: Text -> Word16 -> Bool -> IO ClientEnv
newClientEnv nodeAddress nodePort useHttps = do
  manager' <- newManager $ bool
    (defaultManagerSettings { managerModifyRequest = fixRequest })
    (tlsManagerSettings { managerModifyRequest = fixRequest })
    useHttps
  let nodeUrl = BaseUrl (bool Http Https useHttps)
                 (toString nodeAddress) (fromIntegral nodePort) ""
  return $ mkClientEnv manager' nodeUrl

-- | Add header, required by the Tezos RPC interface
fixRequest :: Request -> IO Request
fixRequest req = return $
  req { requestHeaders = ("Content-Type", "application/json") :
        filter (("Content-Type" /=) . fst) (requestHeaders req)
      }
