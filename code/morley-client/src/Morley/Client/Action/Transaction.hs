-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
-- | Functions to submit transactions via @tezos-client@ and node RPC.

module Morley.Client.Action.Transaction
  ( runTransactions
  , lRunTransactions

  -- * Transfer
  , transfer
  , lTransfer

  -- Datatypes for batch transactions
  , TD (..)
  , LTransactionData (..)
  , TransactionData (..)
  ) where

import qualified Data.Kind as Kind
import Data.List (zipWith3)
import qualified Data.Text as T
import Fmt (Buildable(..), blockListF', listF, (+|), (|+))

import Lorentz.Constraints
import qualified Michelson.Typed as T
import Michelson.Typed.Scope
import Michelson.Untyped.Entrypoints
import Morley.Client.Action.Common
import Morley.Client.Logging
import Morley.Client.RPC.Class
import Morley.Client.RPC.Types
import Morley.Client.TezosClient.Class
import Morley.Client.TezosClient.Types
import Morley.Client.Util
import Morley.Micheline
import Tezos.Address
import Tezos.Core (Mutez, unMutez)
import Util.ByteString

-- | Helper for 'TransactionData' and 'LTransactionData'.
data TD (t :: Kind.Type) = TD
  { tdReceiver :: Address
  , tdAmount :: Mutez
  , tdEpName :: EpName
  , tdParam :: t
  }

-- | Data for a single transaction in a batch.
data TransactionData where
  TransactionData ::
    forall (t :: T.T). ParameterScope t =>
    TD (T.Value t) -> TransactionData

instance Buildable TransactionData where
  build (TransactionData TD {..}) =
    "To: " +| tdReceiver |+ ". EP: " +| tdEpName |+ ". Parameter: " +| tdParam |+
    ". Amount: " +| tdAmount |+ ""

toParametersInternals
  :: ParameterScope t
  => EpName
  -> T.Value t
  -> ParametersInternal
toParametersInternals epName epParam = ParametersInternal
  { piEntrypoint = epNameToTezosEp epName
  , piValue = toExpression epParam
  }

-- | Perform sequence of transactions to the contract, each transactions should be
-- defined via @EntrypointParam t@. Returns operation hash and block in which
-- this operation appears.
runTransactions
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => Address -> [TransactionData]
  -> m Text
runTransactions sender transactions = do
  logInfo $ T.strip $ -- strip trailing newline
    "Running transactions from " +| sender |+ ":\n" +|
    blockListF' "-" build transactions
  pp@ProtocolParameters{..} <- getProtocolParameters

  OperationConstants{..} <- preProcessOperation sender
  let opsToRun = zipWith (\(TransactionData TD {..}) i ->
        TransactionOperation
        { toDestination = tdReceiver
        , toCommonData =
          mkCommonOperationData sender (ocCounter + i) pp
        , toAmount = fromIntegral $ unMutez tdAmount
        , toParameters = toParametersInternals tdEpName tdParam
        }) transactions [(1 :: TezosInt64)..]
  let runOp = RunOperation
        { roOperation =
          RunOperationInternal
          { roiBranch = ocLastBlockHash
          , roiContents = map Left opsToRun
          , roiSignature = stubSignature
          }
        , roChainId = bcChainId ocBlockConstants
        }

  -- Perform run_operation with dumb signature in order
  -- to estimate gas cost, storage size and paid storage diff
  results <- getAppliedResults (Left runOp)
  -- All three lists must have the same length here.
  -- @opsToRun@ is constructed directly from @params@.
  -- The length of @results@ is checked in @getAppliedResults@.
  updOps <- sequenceA $ zipWith3
        (\opToRun@TransactionOperation{..} (TransactionData TD {..}) ar@AppliedResult{..} -> do
            let
              storageLimit = arPaidStorageDiff +
                arAllocatedDestinationContracts * (fromIntegral ppOriginationSize) +
                (fromIntegral $ length arOriginatedContracts) * (fromIntegral ppOriginationSize)
            fee <- calcTransferFee $ CalcTransferFeeData
                   { ctfdFrom = AddressResolved sender
                   , ctfdTo = AddressResolved tdReceiver
                   , ctfdParam = tdParam
                   , ctfdEp = tdEpName
                   , ctfdAmount = toAmount
                   , ctfdBurnCap = storageLimit
                   }
            return $ Left $ opToRun
              { toCommonData = updateCommonData toCommonData ar storageLimit fee }
        ) opsToRun transactions results
  -- Forge operation with given limits and get its hexadecimal representation
  HexJSONByteString hex <- forgeOperation $ ForgeOperation
    { foBranch = ocLastBlockHash
    , foContents = updOps
    }

  signature' <-
    signBytes (AddressResolved sender) (addOperationPrefix hex)

  -- Operation still can fail due to insufficient gas or storage limit, so it's required
  -- to preapply it before injecting
  let preApplyOp = PreApplyOperation
        { paoProtocol = bcProtocol ocBlockConstants
        , paoBranch = ocLastBlockHash
        , paoContents = updOps
        , paoSignature = signature'
        }
  originatedContracts <- concatMap arOriginatedContracts <$>
    getAppliedResults (Right preApplyOp)
  unless (null originatedContracts) $ logInfo $ T.strip $
    "The following contracts were originated during transactions: " +|
    listF originatedContracts |+ ""

  operationHash <- injectOperation $ prepareOpForInjection hex signature'

  operationHash <$ waitForOperation operationHash

-- | Lorentz version of 'TransactionData'.
data LTransactionData where
  LTransactionData ::
    forall (t :: Kind.Type). NiceParameter t =>
    TD t -> LTransactionData

-- | Lorentz version of 'runTransactions'
lRunTransactions
  :: forall m env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    )
  => Address
  -> [LTransactionData]
  -> m Text
lRunTransactions sender transactions =
  runTransactions sender $ map convertLTransactionData transactions
  where
    convertLTransactionData :: LTransactionData -> TransactionData
    convertLTransactionData (LTransactionData (TD {..} :: TD t))=
      withDict (niceParameterEvi @t) $
      TransactionData TD
      { tdReceiver = tdReceiver
      , tdEpName = tdEpName
      , tdAmount = tdAmount
      , tdParam = T.toVal tdParam
      }

transfer
  :: forall m t env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    , ParameterScope t
    )
  => Address
  -> Address
  -> Mutez
  -> EpName
  -> T.Value t
  -> m Text
transfer from to amount epName param =
  runTransactions from $
  [TransactionData TD
    { tdReceiver = to
    , tdAmount = amount
    , tdEpName = epName
    , tdParam = param
    }
  ]

lTransfer
  :: forall m t env.
    ( HasTezosRpc m
    , HasTezosClient m
    , WithClientLog env m
    , NiceParameter t
    )
  => Address
  -> Address
  -> Mutez
  -> EpName
  -> t
  -> m Text
lTransfer from to amount epName param =
  withDict (niceParameterEvi @t) $
    transfer from to amount epName (T.toVal param)
