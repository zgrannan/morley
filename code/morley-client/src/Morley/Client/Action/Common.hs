-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module with functions that used in both transaction sending and contract
-- origination.
module Morley.Client.Action.Common
  ( OperationConstants(..)
  , addOperationPrefix
  , getAppliedResults
  , preProcessOperation
  , stubSignature
  , prepareOpForInjection
  , updateCommonData
  ) where

import Control.Lens (Prism')
import Data.ByteString (cons)
import Fmt (pretty)
import Text.Hex (encodeHex)

import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types
import Morley.Micheline (TezosInt64)
import Morley.Micheline.Expression (Expression(ExpressionString))
import Tezos.Address
import Tezos.Crypto

-- | Datatype that contains various values required for
-- chain operations.
data OperationConstants = OperationConstants
  { ocLastBlockHash :: Text
  -- ^ Block in which operations is going to be injected
  , ocBlockConstants :: BlockConstants
  -- ^ Information about block: chain_id and protocol
  , ocCounter :: TezosInt64
  -- ^ Sender counter
  }

-- | Preprocess chain operation in order to get required constants.
preProcessOperation
  :: (HasTezosRpc m) => Address -> m OperationConstants
preProcessOperation sourceAddr = do
  ocLastBlockHash <- getHeadBlock
  ocBlockConstants <- getBlockConstants ocLastBlockHash
  ocCounter <- getCounter sourceAddr
  pure OperationConstants{..}

-- | Perform runOperation or preApplyOperations and combine the results.
--
-- If an error occurs, this function tries to turn errors returned by RPC
-- into 'ClientRpcError'. If it can't do the conversion, 'UnexpectedErrors'
-- will be thrown.
getAppliedResults
  :: (HasTezosRpc m)
  => Either RunOperation PreApplyOperation -> m [AppliedResult]
getAppliedResults op = do
  (runResult, expectedContentsSize) <- case op of
    Left runOp ->
      (, length $ roiContents $ roOperation runOp) <$> runOperation runOp
    Right preApplyOp -> do
      results <- preApplyOperations [preApplyOp]
      -- There must be exactly one result because we pass a list
      -- consisting of 1 item.
      case results of
        [result] -> pure (result, length $ paoContents preApplyOp)
        _ -> throwM $ RpcUnexpectedSize 1 (length results)

  handleOperationResult runResult expectedContentsSize
  where
    handleOperationResult ::
      MonadThrow m => RunOperationResult -> Int -> m [AppliedResult]
    handleOperationResult RunOperationResult{..} expectedContentsSize = do
      when (length rrOperationContents /= expectedContentsSize) $
        throwM $ RpcUnexpectedSize expectedContentsSize (length rrOperationContents)

      mapM (\(OperationContent (RunMetadata res internalOps)) ->
              let internalResults = map unInternalOperation internalOps in
                case foldr combineResults res internalResults of
                  OperationApplied appliedRes -> pure appliedRes
                  OperationFailed errors -> handleErrors errors
           ) rrOperationContents

    -- When an error happens, we will get a list of 'RunError' in response.
    -- This list often contains more than one item.
    -- We tested which errors are returned in certain scenarios and added
    -- handling of such scenarios here.
    -- We don't rely on any specific order of errors and on the number of errors.
    -- For example, in case of bad parameter this number can be different.
    handleErrors :: MonadThrow m => [RunError] -> m a
    handleErrors errs
      | Just address <- findError _RuntimeError
      , Just expr <- findError _ScriptRejected
      = throwM $ ContractFailed address expr
      | Just address <- findError _BadContractParameter
      , Just (_, expr) <- findError _InvalidSyntacticConstantError
      = throwM $ BadParameter address expr
      | Just address <- findError _BadContractParameter
      , Just notation <- findError _InvalidContractNotation
      = throwM $ BadParameter address (ExpressionString notation)
      | Just address <- findError _REEmptyTransaction
      = throwM $ EmptyTransaction address
      | Just address <- findError _RuntimeError
      , Just _ <- findError _ScriptOverflow
      = throwM $ ShiftOverflow address
      | otherwise = throwM $ UnexpectedRunErrors errs
      where
        findError :: Prism' RunError a -> Maybe a
        findError prism = fmap head . nonEmpty . mapMaybe (preview prism) $ errs

-- | Update common operation data based on preliminary run which estimates storage and
-- gas limits and fee.
--
-- Reference implementation adds 100 gas and 20 bytes to the limits for safety.
updateCommonData
  :: CommonOperationData -> AppliedResult -> TezosInt64 ->  TezosInt64
  -> CommonOperationData
updateCommonData commonData ar storageLimit fee =
  commonData
  { codGasLimit = arConsumedGas ar + 100
  , codStorageLimit = storageLimit + 20
  , codFee = fee
  }

stubSignature :: Signature
stubSignature = unsafeParseSignature
  "edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q"
  where
    unsafeParseSignature :: HasCallStack => Text -> Signature
    unsafeParseSignature = either (error . pretty) id . parseSignature

addOperationPrefix :: ByteString -> ByteString
addOperationPrefix = cons 0x03

prepareOpForInjection :: ByteString -> Signature -> Text
prepareOpForInjection operationHex signature' =
  encodeHex operationHex <> (encodeHex . signatureToBytes) signature'
