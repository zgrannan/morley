-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Functions to originate smart contracts via @tezos-client@ and node RPC.
module Morley.Client.Action.Origination
  ( originateContract
  , originateContracts
  , originateUntypedContract
  -- Lorentz version
  , lOriginateContract
  , lOriginateContracts

  -- Datatypes for batch originations
  , LOriginationData (..)
  , OriginationData (..)
  ) where

import Data.List (zipWith3)
import Fmt (Buildable(..), blockListF', pretty, (+|), (|+))

import qualified Lorentz as L
import Lorentz.Constraints
import Michelson.TypeCheck (SomeContract(..), typeCheckContract, typeVerifyStorage)
import Michelson.Typed (Contract(..), IsoValue(..), Value)
import Michelson.Typed.Scope
import qualified Michelson.Untyped as U
import Morley.Client.Action.Common
import Morley.Client.Logging
import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types
import Morley.Client.TezosClient.Class
import Morley.Client.TezosClient.Types
import Morley.Micheline
import Tezos.Address
import Tezos.Core
import Util.ByteString
import Util.Exception

-- | Data for a single origination in a batch
data OriginationData = forall cp st. StorageScope st => OriginationData
  { odReplaceExisting :: Bool
  , odName :: Text
  , odBalance :: Mutez
  , odContract :: Contract cp st
  , odStorage :: Value st
  }

-- | Originate given contracts with given initial storages. Returns
-- operation hash and originated contracts' addresses.
originateContracts
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => AddressOrAlias
  -> [OriginationData]
  -> m (Text, [Address])
originateContracts sender' originations = do
  senderAddress <- resolveAddress sender'

  logInfo $ "Starting origination of following contracts by " +| sender'
    |+ ":\n" +| blockListF' "-" (build . odName) originations

  pp@ProtocolParameters{..} <- getProtocolParameters
  OperationConstants{..} <- preProcessOperation senderAddress
  let
    opsToRun = zipWith (\OriginationData{..} i -> OriginationOperation
                         { ooCommonData =
                           mkCommonOperationData senderAddress (ocCounter + i) pp
                         , ooBalance = fromIntegral $ unMutez odBalance
                         , ooScript = mkOriginationScript odContract odStorage
                         }
                       ) originations [(1 :: TezosInt64)..]

    runOp = RunOperation
      { roOperation =
        RunOperationInternal
          { roiBranch = ocLastBlockHash
          , roiContents = map Right opsToRun
          , roiSignature = stubSignature
          }
      , roChainId = bcChainId ocBlockConstants
      }

  -- Estimate limits and paid storage diff
  results <- getAppliedResults (Left runOp)
  -- Update limits and fee
  updOps <- sequenceA $ zipWith3
    (\opToRun@OriginationOperation{..} OriginationData{..} ar@AppliedResult{..} ->
       do
         let storageLimit = arPaidStorageDiff + fromIntegral ppOriginationSize
         fee <- calcOriginationFee $ CalcOriginationFeeData
           { cofdFrom = sender'
           , cofdBalance = fromIntegral $ unMutez odBalance
           , cofdContract = odContract
           , cofdStorage = odStorage
           , cofdBurnCap = storageLimit
           }
         return $ Right $ opToRun
           {ooCommonData = updateCommonData ooCommonData ar storageLimit fee}
    ) opsToRun originations results

  HexJSONByteString hex <- forgeOperation $ ForgeOperation
    { foBranch = ocLastBlockHash
    , foContents = updOps
    }

  signature <- signBytes sender' (addOperationPrefix hex)

  let preApplyOp = PreApplyOperation
        { paoProtocol = bcProtocol ocBlockConstants
        , paoBranch = ocLastBlockHash
        , paoContents = updOps
        , paoSignature = signature
        }

  -- Perform operations preapplying in order to obtain contract address
  ars2 <- getAppliedResults (Right preApplyOp)

  contractAddrs <- forM ars2 $ \ar -> case arOriginatedContracts ar of
    [contractAddr'] -> pure contractAddr'
    contracts -> throwM $ RpcOriginatedMoreContracts contracts

  operationHash <- injectOperation $ prepareOpForInjection hex signature

  waitForOperation operationHash

  forM_ @[(Address, OriginationData)] @m @()
    (zip contractAddrs originations) $ \(addr, OriginationData{..}) -> do
    logDebug $ "Saving " <> pretty addr <> " for " <> odName <> "\n"
    rememberContract odReplaceExisting addr odName

  return (operationHash, contractAddrs)

-- | Originate single contract
originateContract
  :: forall m cp st env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     , StorageScope st
     )
  => Bool
  -> Text
  -> AddressOrAlias
  -> Mutez
  -> Contract cp st
  -> Value st
  -> m (Text, Address)
originateContract replaceExisting name sender' balance contract initialStorage = do
  (hash, contracts) <- originateContracts sender' $
    [OriginationData replaceExisting name balance contract initialStorage]
  case contracts of
    [addr] -> pure (hash, addr)
    _ -> throwM $ RpcOriginatedMoreContracts contracts

-- | Originate a single untyped contract
originateUntypedContract
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => Bool
  -> Text
  -> AddressOrAlias
  -> Mutez
  -> U.Contract
  -> U.Value
  -> m (Text, Address)
originateUntypedContract replaceExisting name sender' balance contract initialStorage = do
  SomeContract (fullContract@Contract{} :: Contract cp st) <-
    throwLeft . pure $ typeCheckContract contract
  storage <- throwLeft . pure $ typeVerifyStorage @st initialStorage
  originateContract replaceExisting name sender' balance fullContract storage

-- | Lorentz version of 'OriginationData'
data LOriginationData = forall cp st. (NiceParameterFull cp, NiceStorage st)
  => LOriginationData
  { lodReplaceExisting :: Bool
  , lodName :: Text
  , lodBalance :: Mutez
  , lodContract :: L.Contract cp st
  , lodStorage :: st
  }

-- | Lorentz version of 'originateContracts'
lOriginateContracts
  :: forall m env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     )
  => AddressOrAlias
  -> [LOriginationData]
  -> m (Text, [Address])
lOriginateContracts sender' originations =
  originateContracts sender' $ map convertLOriginationData originations
  where
    convertLOriginationData :: LOriginationData -> OriginationData
    convertLOriginationData LOriginationData {..} = case lodStorage of
      (_ :: st) -> withDict (niceStorageEvi @st) $ OriginationData
        { odReplaceExisting = lodReplaceExisting
        , odName = lodName
        , odBalance = lodBalance
        , odContract = L.compileLorentzContract lodContract
        , odStorage = toVal lodStorage
        }

-- | Originate single Lorentz contract
lOriginateContract
  :: forall m cp st env.
     ( HasTezosRpc m
     , HasTezosClient m
     , WithClientLog env m
     , NiceStorage st
     , NiceParameterFull cp
     )
  => Bool
  -> Text
  -> AddressOrAlias
  -> Mutez
  -> L.Contract cp st
  -> st
  -> m (Text, Address)
lOriginateContract replaceExisting name sender' balance lContract lStorage = do
  (hash, contracts) <- lOriginateContracts sender' $
    [LOriginationData replaceExisting name balance lContract lStorage]
  case contracts of
    [addr] -> return (hash, addr)
    _ ->  throwM $ RpcOriginatedMoreContracts contracts

mkOriginationScript
  :: Contract cp st -> Value st -> OriginationScript
mkOriginationScript contract@Contract{} initialStorage = OriginationScript
  { osCode = toExpression contract
  , osStorage = toExpression initialStorage
  }
