-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Types used for interaction with @tezos-client@.

module Morley.Client.TezosClient.Types
  ( Alias
  , AddressOrAlias (..)
  , addressResolved
  , CalcOriginationFeeData (..)
  , CalcTransferFeeData (..)
  , TezosClientConfig (..)
  , TezosClientEnv (..)
  , HasTezosClientEnv (..)
  ) where

import Data.Aeson (FromJSON(..), withObject, (.!=), (.:), (.:?))
import Fmt (Buildable(..))
import qualified Options.Applicative as Opt

import Lorentz (ToAddress, toAddress)
import Michelson.Typed (Contract, EpName, Value)
import Morley.Micheline
import Tezos.Address (Address, parseAddress)
import Util.CLI (HasCLReader(..))

-- | @tezos-client@ can associate addresses with textual aliases.
-- This types denotes such an alias.
type Alias = Text

-- | Representation of an address that @tezos-client@ uses. It can be
-- an address itself or a textual alias.
data AddressOrAlias
  = AddressResolved Address
  -- ^ Address itself, can be used as is.
  | AddressAlias Alias
  -- ^ Address alias, should be resolved by @tezos-client@.
  deriving stock (Show, Eq, Ord)

instance HasCLReader AddressOrAlias where
  getReader =
    Opt.str <&> \addrOrAlias ->
      case parseAddress addrOrAlias of
        Right addr -> AddressResolved addr
        Left _ -> AddressAlias addrOrAlias
  getMetavar = "ADDRESS OR ALIAS"

-- | Creates an 'AddressOrAlias' with the given address.
addressResolved :: ToAddress addr => addr -> AddressOrAlias
addressResolved = AddressResolved . toAddress

instance Buildable AddressOrAlias where
  build = \case
    AddressResolved addr -> build addr
    AddressAlias alias -> build alias

-- | Configuration maintained by @tezos-client@, see its @config@ subcommands
-- (e. g. @tezos-client config show@).
-- Only the fields we are interested in are present here.
data TezosClientConfig = TezosClientConfig
  { tcNodeAddr :: Text
  , tcNodePort :: Word16
  , tcUseTls :: Bool
  } deriving stock Show

-- | For reading tezos-client config.
instance FromJSON TezosClientConfig where
  parseJSON = withObject "node info" $ \o -> do
    tcNodeAddr <- o .: "node_addr"
    tcNodePort <- o .: "node_port"
    tcUseTls <- o .:? "tls" .!= False
    return TezosClientConfig { .. }

-- | Runtime environment for @tezos-client@ bindings.
data TezosClientEnv = TezosClientEnv
  { tceAliasPrefix :: Maybe Text
  -- ^ Optional prefix for aliases that will be passed to @tezos-client@.
  -- If you call some function and pass @foo@ 'Alias' to it when the prefix
  -- is provided, it will be prepened to @foo@. So @prefix.foo@ will be passed
  -- to @tezos-client@. Note that the prefix will be only applied in functions
  -- such as `genKey` and `rememberContract` that work directly
  -- with @tezos-client@ contract cache and add addresses to it.
  , tceNodeAddress :: Text
  -- ^ Address of tezos node on which operations are performed.
  , tceNodePort :: Word16
  -- ^ Port of tezos node.
  , tceTezosClientPath :: FilePath
  -- ^ Path to tezos client binary through which operations are
  -- performed.
  , tceMbTezosClientDataDir :: Maybe FilePath
  -- ^ Path to tezos client data directory.
  , tceNodeUseHttps :: Bool
  -- ^ Whether to use tls to connect to tezos node.
  }

-- | Using this type class one can require 'MonadReader' constraint
-- that holds any type with 'TezosClientEnv' inside.
class HasTezosClientEnv env where
  tezosClientEnvL :: Lens' env TezosClientEnv

-- | Data required for calculating fee for transfer operation.
data CalcTransferFeeData t = CalcTransferFeeData
  { ctfdFrom :: AddressOrAlias
  , ctfdTo :: AddressOrAlias
  , ctfdParam :: Value t
  , ctfdEp :: EpName
  , ctfdAmount :: TezosInt64
  , ctfdBurnCap :: TezosInt64
  }

-- | Data required for calculating fee for origination operation.
data CalcOriginationFeeData cp st = CalcOriginationFeeData
  { cofdFrom :: AddressOrAlias
  , cofdBalance :: TezosInt64
  , cofdContract :: Contract cp st
  , cofdStorage :: Value st
  , cofdBurnCap :: TezosInt64
  }
