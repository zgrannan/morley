-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Abstraction layer for @tezos-client@ functionality.
-- We use it to mock @tezos-client@ in tests.

module Morley.Client.TezosClient.Class
  ( HasTezosClient (..)
  ) where

import Michelson.Typed.Scope
import Morley.Client.TezosClient.Types
import Morley.Micheline
import Tezos.Address
import Tezos.Crypto

-- | Type class that provides interaction with @tezos-client@ binary
class (Monad m) => HasTezosClient m where
  signBytes :: AddressOrAlias -> ByteString -> m Signature
  -- ^ Sign an operation with @tezos-client@.
  genKey :: Alias -> m Address
  -- ^ Generate a secret key and store it with given alias.
  -- If a key with this alias already exists, the corresponding address
  -- will be returned and no state will be changed.
  genFreshKey :: Alias -> m Address
  -- ^ Generate a secret key and store it with given alias.
  -- Unlike 'genKey' this function overwrites
  -- the existing key when given alias is already stored.
  revealKey :: Alias -> m ()
  -- ^ Reveal public key associated with given implicit account.
  waitForOperation :: Text -> m ()
  -- ^ Wait until operation known by some hash is included into the chain.
  rememberContract :: Bool -> Address -> Alias -> m ()
  -- ^ Associate the given contract with alias.
  -- The `Bool` variable indicates whether or not we should replace already
  -- existing contract alias or not.
  resolveAddress :: AddressOrAlias -> m Address
  -- ^ Retrieve an address from given address or alias using @tezos-client@.
  getAlias :: AddressOrAlias -> m Alias
  -- ^ Retrieve an alias from given address using @tezos-client@.  The
  -- primary (and probably only) reason this function exists is that
  -- @tezos-client sign@ command only works with aliases. It was
  -- reported upstream: <https://gitlab.com/tezos/tezos/-/issues/653>.
  getTezosClientConfig :: m TezosClientConfig
  -- ^ Retrieve the current @tezos-client@ config.
  calcTransferFee
    :: PrintedValScope t => CalcTransferFeeData t -> m TezosInt64
  -- ^ Calculate fee for transfer using `--dry-run` flag.
  calcOriginationFee
    :: PrintedValScope st => CalcOriginationFeeData cp st -> m TezosInt64
  -- ^ Calculate fee for origination using `--dry-run` flag.
