-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Interface to the @tezos-client@ executable expressed in Haskell types.

module Morley.Client.TezosClient.Impl
  ( TezosClientError (..)

  -- * @tezos-client@ api
  , signBytes
  , waitForOperationInclusion
  , rememberContract
  , genKey
  , genFreshKey
  , revealKey
  , resolveAddress
  , getAlias
  , getPublicKey
  , getTezosClientConfig
  , calcTransferFee
  , calcOriginationFee

  -- * Internals
  , callTezosClient
  , callTezosClientStrict
  , prefixName
  ) where

import Control.Exception (IOException, throwIO)
import Data.Aeson (eitherDecodeStrict)
import qualified Data.Text as T
import Fmt (Buildable(..), pretty, (+|), (|+))
import Colourista (formatWith, red)
import System.Exit (ExitCode(..))
import System.Process (readProcessWithExitCode)
import Text.Hex (encodeHex)
import Text.Printf (printf)

import Lorentz
import Michelson.Printer
import Michelson.Typed.Scope
import Morley.Client.Logging
import Morley.Client.TezosClient.Parser
import Morley.Client.TezosClient.Types
import Morley.Client.Util
import Morley.Micheline
import Tezos.Address
import Tezos.Crypto

----------------------------------------------------------------------------
-- Errors
----------------------------------------------------------------------------

-- | A data type for all /predicatable/ errors that can happen during
-- @tezos-client@ usage.
data TezosClientError =
    UnexpectedClientFailure
    -- ^ @tezos-client@ call unexpectedly failed (returned non-zero exit code).
    -- The error contains the error code, stdout and stderr contents.
      Int -- ^ Exit code
      Text -- ^ stdout
      Text -- ^ stderr

  -- These errors represent specific known scenarios.
  | UnknownAddressAlias
    -- ^ Could not find an address with given name.
      Text -- ^ Name of address which is eventually used
  | UnknownAddress
    -- ^ Could not find an address.
      Address -- ^ Address that is not present in local tezos cache
  | AlreadyRevealed
    -- ^ Public key of the given address is already revealed.
      Alias -- ^ Address alias that has already revealed its key
  | InvalidOperationHash
    -- ^ Can't wait for inclusion of operation with given hash because
    -- the hash is invalid.
      Text

  -- Note: the errors below most likely indicate that something is wrong in our code.
  -- Maybe we made a wrong assumption about tezos-client or just didn't consider some case.
  -- Another possible reason that a broken tezos-client is used.
  | ConfigParseError String
  -- ^ A parse error occurred during config parsing.
  | TezosClientCryptoParseError Text CryptoParseError
  -- ^ @tezos-client@ produced a cryptographic primitive that we can't parse.
  | TezosClientParseAddressError Text ParseAddressError
  -- ^ @tezos-client@ produced an address that we can't parse.
  | TezosClientParseFeeError Text Text
  -- ^ @tezos-client@ produced invalid output for parsing baker fee
  | TezosClientUnexpectedOutputFormat Text
  -- ^ @tezos-client@ printed a string that doesn't match the format we expect.
  deriving stock (Show, Eq)

instance Exception TezosClientError where
  displayException = pretty

instance Buildable TezosClientError where
  build = \case
    UnexpectedClientFailure errCode output errOutput ->
      "tezos-client unexpectedly failed with error code " +| errCode |+
      ". Stdout:\n" +| output |+ "\nStderr:\n" +| errOutput |+ ""
    UnknownAddressAlias name ->
      "Could not find an address with name " <> build name <> "."
    UnknownAddress uaddr ->
      "Could not find an associated name for the given address " <>
      build uaddr <> "."
    AlreadyRevealed alias ->
      "The address alias " <> build alias <> " is already revealed"
    InvalidOperationHash hash ->
      "Can't wait for inclusion of operation " <> build hash <>
      " because this hash is invalid."
    ConfigParseError err ->
      "A parse error occurred during config parsing: " <> build err
    TezosClientCryptoParseError txt err ->
      "tezos-client produced a cryptographic primitive that we can't parse: " +|
      txt |+ ".\n The error is: " +| err |+ "."
    TezosClientParseAddressError txt err ->
      "tezos-client produced an address that we can't parse: " +|
      txt |+ ".\n The error is: " +| err |+ "."
    TezosClientParseFeeError txt err ->
      "tezos-client produced invalid output for parsing baker fee: " +|
      txt |+ ".\n Parsing error is: " +| err |+ ""
    TezosClientUnexpectedOutputFormat txt ->
      "tezos-client printed a string that doesn't match the format we expect:\n" <>
      build txt

----------------------------------------------------------------------------
-- API
----------------------------------------------------------------------------

-- Note: if we try to sign with an unknown alias, tezos-client will
-- report a fatal error (assert failure) to stdout. It's bad. It's
-- reported in two issues: https://gitlab.com/tezos/tezos/-/issues/653
-- and https://gitlab.com/tezos/tezos/-/issues/813.
-- I (@gromak) currently think it's better to wait for it to be resolved upstream.
-- Currently we will throw 'TezosClientUnexpectedOutputFormat' error.
-- | Sign an arbtrary bytestring using @tezos-client@.
-- Secret key of the address corresponding to give 'AddressOrAlias' must be known.
signBytes
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => AddressOrAlias
  -> ByteString
  -> m Signature
signBytes signer opHash = do
  let
    bytes = addTezosBytesPrefix $ encodeHex opHash
  signerAlias <- getAlias signer
  logDebug $ "Signing for " <> pretty signer
  output <- callTezosClientStrict
    ["sign", "bytes", toString bytes, "for", toString signerAlias]
  liftIO case T.stripPrefix "Signature: " output of
    Nothing ->
      throwM $ TezosClientUnexpectedOutputFormat output
    Just signatureTxt ->
      either (throwM . TezosClientCryptoParseError signatureTxt) pure $
      parseSignature . T.strip $ signatureTxt

-- | Generate a new secret key and save it with given alias.
-- If an address with given alias already exists, it will be returned
-- and no state will be changed.
genKey
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => Alias
  -> m Address
genKey originatorAlias = do
  prefix <- tceAliasPrefix <$> view tezosClientEnvL
  let
    name :: Alias
    name = prefixName prefix originatorAlias
    isAlreadyExistsError :: Text -> Bool
    -- We can do a bit better here using more complex parsing if necessary.
    isAlreadyExistsError = T.isInfixOf "already exists."

    errHandler _ errOut = pure (isAlreadyExistsError errOut)

  _ <-
    callTezosClient errHandler
    ["gen", "keys", toString name]
  resolveAddress (AddressAlias name)

-- | Generate a new secret key and save it with given alias.
-- If an address with given alias already exists, it will be removed
-- and replaced with a fresh one.
genFreshKey
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => Alias
  -> m Address
genFreshKey originatorAlias = do
  prefix <- tceAliasPrefix <$> view tezosClientEnvL
  let
    name :: Alias
    name = prefixName prefix originatorAlias
    isNoAliasError :: Text -> Bool
    -- We can do a bit better here using more complex parsing if necessary.
    isNoAliasError = T.isInfixOf "no public key hash alias named"

    errHandler _ errOutput = pure (isNoAliasError errOutput)

  _ <-
    callTezosClient errHandler
    ["forget", "address", toString name, "--force"]
  callTezosClientStrict ["gen", "keys", toString name]
  resolveAddress (AddressAlias name)

-- | Reveal public key corresponding to the given alias.
-- Fails if it's already revealed.
revealKey
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => Alias
  -> m ()
revealKey alias = do
  logDebug $ "Revealing key for " <> alias
  let
    alreadyRevealed = T.isInfixOf "previously revealed"
    errHandler _ errOut =
      False <$ when (alreadyRevealed errOut) (throwM (AlreadyRevealed alias))

  _ <-
    callTezosClient errHandler
    ["reveal", "key", "for", toString alias]

  logDebug $ "Successfully revealed key for " <> alias

-- | Return 'Address' corresponding to given 'AddressOrAlias'.
resolveAddress
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => AddressOrAlias
  -> m Address
resolveAddress = \case
  AddressResolved addr -> pure addr
  AddressAlias originatorName -> do
    logDebug $ "Resolving " <> originatorName
    output <- callTezosClientStrict ["list", "known", "contracts"]
    let parse = T.stripPrefix (originatorName <> ": ")
    liftIO case safeHead . mapMaybe parse . lines $ output of
      Nothing -> throwM $ UnknownAddressAlias originatorName
      Just addrText ->
        either (throwM . TezosClientParseAddressError addrText) pure $
        parseAddress addrText

-- | Return 'Alias' corresponding to given 'AddressOrAlias'.
getAlias
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => AddressOrAlias
  -> m Alias
getAlias = \case
  AddressAlias alias -> pure alias
  AddressResolved senderAddress -> do
    logDebug $ "Getting an alias for " <> pretty senderAddress
    output <- callTezosClientStrict ["list", "known", "contracts"]
    let parse = T.stripSuffix (": " <> pretty senderAddress)
    liftIO case safeHead . mapMaybe parse . lines $ output of
      Nothing -> throwM $ UnknownAddress senderAddress
      Just alias -> pure alias

-- | Return 'PublicKey' corresponding to given 'AddressOrAlias'.
getPublicKey
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => AddressOrAlias
  -> m PublicKey
getPublicKey addrOrAlias = do
  alias <- getAlias addrOrAlias
  logDebug $ "Getting " <> alias <> " public key"
  output <- callTezosClientStrict ["show", "address", toString alias]
  liftIO case lines output of
    _ : [rawPK] -> do
      pkText <- maybe
        (throwM $ TezosClientUnexpectedOutputFormat rawPK) pure
        (T.stripPrefix "Public Key: " rawPK)
      either (throwM . TezosClientCryptoParseError pkText) pure $
        parsePublicKey pkText
    _ -> throwM $ TezosClientUnexpectedOutputFormat output

-- | This function blocks until operation with given hash is included into blockchain.
waitForOperationInclusion
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => Text
  -> m ()
waitForOperationInclusion op = void do
  logDebug $ "Waiting for operation " <> op <> " to be included..."
  callTezosClient errHandler
    ["wait", "for", toString op, "to", "be", "included"]
  where
    errHandler _ errOutput =
      False <$ when ("Invalid operation hash:" `T.isInfixOf` errOutput)
      (throwM $ InvalidOperationHash op)

-- | Save a contract with given address and alias.
-- If @replaceExisting@ is @False@ and a contract with given alias
-- already exists, this function does nothing.
rememberContract
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => Bool
  -> Address
  -> Alias
  -> m ()
rememberContract replaceExisting contractAddress newAlias = do
  prefix <- tceAliasPrefix <$> view tezosClientEnvL
  let
    name = prefixName prefix newAlias
    isAlreadyExistsError = T.isInfixOf "already exists"
    errHandler _ errOut = pure (isAlreadyExistsError errOut)
    args = ["remember", "contract", toString name, pretty contractAddress]
  _ <-
    callTezosClient errHandler $
    if replaceExisting then args <> ["--force"] else args
  pure ()

-- | Read @tezos-client@ configuration.
getTezosClientConfig :: FilePath -> Maybe FilePath -> IO TezosClientConfig
getTezosClientConfig client mbDataDir = do
  t <- readProcessWithExitCode' client
    (maybe [] (\dir -> ["-d", dir]) mbDataDir ++  ["config", "show"]) ""
  case t of
    (ExitSuccess, toText -> output, _)
      | null (T.strip output) -> pure defaultTezosClientConfig
      | otherwise -> case eitherDecodeStrict . encodeUtf8 . toText $ output of
        Right config -> pure config
        Left err -> throwM $ ConfigParseError err
    (ExitFailure errCode, toText -> output, toText -> errOutput) ->
      throwM $ UnexpectedClientFailure errCode output errOutput
  where
    defaultTezosClientConfig :: TezosClientConfig
    defaultTezosClientConfig = TezosClientConfig
      { tcNodeAddr = "localhost"
      , tcNodePort = 8732
      , tcUseTls = False
      }

-- | Calc baker fee for transfer using @tezos-client@.
calcTransferFee
  :: ( PrintedValScope t, WithClientLog env m, HasTezosClientEnv env
     , MonadIO m, MonadThrow m
     )
  => CalcTransferFeeData t -> m TezosInt64
calcTransferFee CalcTransferFeeData{..} = do
  output <- callTezosClientStrict
    [ "transfer", showTez ctfdAmount
    , "from", pretty ctfdFrom
    , "to", pretty ctfdTo, "--arg", toString $ printTypedValue True ctfdParam
    , "--entrypoint", toString $ epNameToTezosEp ctfdEp
    , "--burn-cap", showBurnCap ctfdBurnCap, "--dry-run"
    ]
  feeOutputParser output

-- | Calc baker fee for origination using @tezos-client@.
calcOriginationFee
  :: ( PrintedValScope st, WithClientLog env m, HasTezosClientEnv env
     , MonadIO m, MonadThrow m
     )
  => CalcOriginationFeeData cp st -> m TezosInt64
calcOriginationFee CalcOriginationFeeData{..} = do
  output <- callTezosClientStrict
    [ "originate", "contract", "-", "transferring"
    , showTez cofdBalance
    , "from", pretty cofdFrom, "running"
    , toString $ printTypedContract True cofdContract, "--init"
    , toString $ printTypedValue True cofdStorage, "--burn-cap"
    , showBurnCap cofdBurnCap, "--dry-run"
    ]
  feeOutputParser output

feeOutputParser :: (MonadIO m, MonadThrow m) => Text -> m TezosInt64
feeOutputParser output =
  case parseBakerFeeFromOutput output of
    Right fee -> return fee
    Left err -> throwM $ TezosClientParseFeeError output $ show err

showBurnCap :: TezosInt64 -> String
showBurnCap x = printf "%.6f" $ (fromIntegral x) / (1e3 :: Float)

showTez :: TezosInt64 -> String
showTez x = printf "%.6f" $ (fromIntegral x) / (1e6 :: Float)

----------------------------------------------------------------------------
-- Helpers
-- All interesting @tezos-client@ functionality is supposed to be
-- exported as functions with types closely resembling inputs of
-- respective @tezos-client@ functions. If something is not
-- available, consider adding it here. But if it is not feasible,
-- you can use these two functions directly to constructor a custom
-- @tezos-client@ call.
----------------------------------------------------------------------------

-- | Call @tezos-client@ with given arguments. Arguments defined by
-- config are added automatically. The second argument specifies what
-- should be done in failure case. It takes stdout and stderr
-- output. Possible handling:
--
-- 1. Parse a specific error and throw it.
-- 2. Parse an expected error that shouldn't cause a failure.
-- Return @True@ in this case.
-- 3. Detect an unexpected error, return @False@.
-- In this case 'UnexpectedClientFailure' will be throw.
callTezosClient
  :: forall env m. (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => (Text -> Text -> IO Bool) -> [String] -> m Text
callTezosClient errHandler args = do
  TezosClientEnv {..} <- view tezosClientEnvL
  let
    extraArgs :: [String]
    extraArgs =
      bool id ("-S":) tceNodeUseHttps $
      mconcat
      [ ["-A", toString tceNodeAddress]
      , ["-P", show tceNodePort]
      , maybe [] (\dir -> ["-d", dir]) tceMbTezosClientDataDir
      ]
  let allArgs = extraArgs ++ args
  logDebug $ "Running: " <> unwords (toText <$> tceTezosClientPath:allArgs)
  let
    ifNotEmpty prefix output
      | null output = ""
      | otherwise = prefix <> ":\n" <> output
    logOutput :: Text -> Text -> m ()
    logOutput output errOutput = logDebug $
      ifNotEmpty "stdout" output <>
      ifNotEmpty "stderr" errOutput

  liftIO (readProcessWithExitCode' tceTezosClientPath allArgs "")  >>= \case
    (ExitSuccess, toText -> output, toText -> errOutput) ->
      output <$ logOutput output errOutput
    (ExitFailure errCode, toText -> output, toText -> errOutput) -> do
      liftIO $ unlessM (errHandler output errOutput) $
        throwM $ UnexpectedClientFailure errCode output errOutput

      output <$ logOutput output errOutput

-- | Call tezos-client and expect success.
callTezosClientStrict
  :: (WithClientLog env m, HasTezosClientEnv env, MonadIO m)
  => [String] -> m Text
callTezosClientStrict args = callTezosClient errHandler args
  where
    errHandler _ _ = pure False

-- | Variant of @readProcessWithExitCode@ that prints a better error in case of
-- an exception in the inner @readProcessWithExitCode@ call.
readProcessWithExitCode'
  :: FilePath
  -> [String]
  -> String
  -> IO (ExitCode, String, String)
readProcessWithExitCode' fp args inp =
  catch
    (readProcessWithExitCode fp args inp) handler
  where
    handler :: IOException -> IO (ExitCode, String, String)
    handler e = do
      hPutStrLn @Text stderr $ formatWith [red] errorMsg
      throwIO e

    errorMsg =
      "ERROR!! There was an error in executing `" <> (show fp) <> "` program. Is the \
      \ executable available in PATH ?"

addTezosBytesPrefix :: Text -> Text
addTezosBytesPrefix = ("0x" <>)

prefixName :: Maybe Text -> Alias -> Alias
prefixName (Just prefix) alias = prefix <> "." <> alias
prefixName Nothing alias = alias
