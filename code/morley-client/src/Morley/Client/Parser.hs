-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Client.Parser
  ( ClientArgs (..)
  , ClientArgsRaw (..)
  , OriginateArgs (..)
  , TransferArgs (..)
  , clientConfigParser
  , morleyClientInfo
  , parserInfo
  ) where

import Fmt (pretty)
import Options.Applicative
  (auto, eitherReader, help, helper, long, metavar, option, short, strOption, subparser, switch,
  value)
import qualified Options.Applicative as Opt
import Options.Applicative.Help.Pretty (Doc, linebreak)

import qualified Michelson.Untyped as U
import Morley.CLI
import Morley.Client.Init
import Tezos.Address
import Tezos.Core
import qualified Tezos.Crypto.Ed25519 as Ed25519
import Util.Named

data ClientArgs
  = ClientArgs MorleyClientConfig ClientArgsRaw

data ClientArgsRaw
  = Originate OriginateArgs
  | Transfer TransferArgs
  | GetBalance Address

data OriginateArgs = OriginateArgs
  { oaMbContractFile :: Maybe FilePath
  , oaContractName   :: Text
  , oaInitialBalance :: Mutez
  , oaInitialStorage :: U.Value
  , oaOriginateFrom  :: Address
  }

data TransferArgs = TransferArgs
  { taSender      :: Address
  , taDestination :: Address
  , taAmount      :: Mutez
  , taParameter   :: U.Value
  }

morleyClientInfo :: Opt.ParserInfo ClientArgs
morleyClientInfo =
  parserInfo
    (#usage .! usageDoc)
    (#description .! "Morley Client: RPC client for interaction with tezos node")
    (#header .! "Morley Client")
    (#parser .! clientParser)

-- | Parser for the @morley-client@ executable.
clientParser :: Opt.Parser ClientArgs
clientParser = ClientArgs <$> clientConfigParser (pure Nothing) <*> argsRawParser

clientConfigParser :: (Opt.Parser (Maybe Text)) -> Opt.Parser MorleyClientConfig
clientConfigParser prefixParser = do
  mccAliasPrefix <- prefixParser
  mccNodeAddress <- urlOption
  mccNodePort <- portOption
  mccTezosClientPath <- pathOption
  mccMbTezosClientDataDir <- dataDirOption
  mccNodeUseHttps <- useHttpsSwitch
  mccVerbosity <- genericLength <$> many verboseSwitch
  mccSecretKey <- namedSecretKeyOption "secret-key"
    "The secret key of the operation performer (only Ed25519 keys are supported)"
  pure $ MorleyClientConfig {..}
  where
    verboseSwitch :: Opt.Parser ()
    verboseSwitch = Opt.flag' () . mconcat $
      [ short 'V'
      , help "Increase verbosity (pass several times to increase further)"
      ]

    useHttpsSwitch :: Opt.Parser Bool
    useHttpsSwitch = switch $
       short 'S' <> long "use-https" <>
       help "Use HTTPS to communicate with the remote node"

urlOption :: Opt.Parser (Maybe Text)
urlOption = optional $ strOption $
  mconcat [ short 'A', long "node-url", metavar "URL"
          , help "Remote node URL"
          ]

portOption :: Opt.Parser (Maybe Word16)
portOption = optional $ option auto $
  mconcat [ short 'P', long "node-port", metavar "PORT"
          , help "Remote node port"
          ]

pathOption :: Opt.Parser FilePath
pathOption = strOption $
  mconcat [ short 'I', long "client-path", metavar "PATH"
          , help "Path to tezos-client binary"
          , value "tezos-client"
          , Opt.showDefault
          ]

dataDirOption :: Opt.Parser (Maybe FilePath)
dataDirOption = optional $ strOption $
  mconcat [ short 'd', long "data-dir", metavar "PATH"
          , help "Path to tezos-client data directory"
          ]

namedSecretKeyOption :: String -> String -> Opt.Parser (Maybe Ed25519.SecretKey)
namedSecretKeyOption longName hInfo = optional . option (eitherReader parseSecretKeyDo) $
  mconcat
    [ metavar "SECRET_KEY"
    , long longName
    , help hInfo
    ]
  where
    parseSecretKeyDo :: String -> Either String Ed25519.SecretKey
    parseSecretKeyDo addr =
      either (Left . mappend "Failed to parse secret key: " . pretty) Right $
      Ed25519.parseSecretKey
      $ toText addr

argsRawParser :: Opt.Parser ClientArgsRaw
argsRawParser = subparser $
  originateCmd <> transferCmd <> getBalanceCmd
  where
    mkCommandParser commandName parser desc =
      Opt.command commandName $
      Opt.info (helper <*> parser) $
      Opt.progDesc desc
    originateCmd =
      mkCommandParser "originate"
      (Originate <$> originateArgsOption)
      "Originate passed contract on real network"
    transferCmd =
      mkCommandParser "transfer"
      (Transfer <$> transferArgsOption)
      "Perform a transfer to the given contract with given amount and parameter"
    getBalanceCmd =
      mkCommandParser "get-balance"
      (GetBalance <$> addressOption
        Nothing
        (#name .! "addr")
        (#help .! "Address to get balance for.")
      )
      "Get balance for given address"
    originateArgsOption :: Opt.Parser OriginateArgs
    originateArgsOption = do
      oaMbContractFile <- mbContractFileOption
      oaContractName <- contractNameOption
      oaInitialBalance <-
        mutezOption
          (Just (unsafeMkMutez 0))
          (#name .! "initial-balance")
          (#help .! "Inital balance of the contract")
      oaInitialStorage <-
        valueOption
          Nothing
          (#name .! "initial-storage")
          (#help .! "Initial contract storage value")
      oaOriginateFrom <-
        addressOption
          Nothing
          (#name .! "from")
          (#help .! "Address from which origination is performed")
      pure $ OriginateArgs {..}
      where
        mbContractFileOption = optional . strOption $ mconcat
          [ long "contract", metavar "FILEPATH"
          , help "Path to contract file"
          ]
        contractNameOption = strOption $ mconcat
          [ long "contract-name"
          , value "stdin"
          , help "Alias of originated contract"
          ]
    transferArgsOption :: Opt.Parser TransferArgs
    transferArgsOption = do
      taSender <-
        addressOption
          Nothing
          (#name .! "from")
          (#help .! "Address from which transfer is performed")
      taDestination <-
        addressOption
          Nothing
          (#name .! "to")
          (#help .! "Address of the contract that receives transfer")
      taAmount <-
        mutezOption
          (Just (unsafeMkMutez 0))
          (#name .! "amount")
          (#help .! "Transfer amount")
      taParameter <-
        valueOption
          Nothing
          (#name .! "parameter")
          (#help .! "Transfer parameter")
      pure $ TransferArgs {..}

usageDoc :: Doc
usageDoc = mconcat
  [ "You can use help for specific COMMAND", linebreak
  , "EXAMPLE:", linebreak
  , "morley-client originate --help"
  , "USAGE EXAMPLE:", linebreak
  , "morley-client -A carthage.testnet.tezos.serokell.team -P 8732 originate \\", linebreak
  , "  --from tz1akcPmG1Kyz2jXpS4RvVJ8uWr7tsiT9i6A \\", linebreak
  , "  --contract ../contracts/tezos_examples/attic/add1.tz --initial-balance 1 --initial-storage 0", linebreak
  , linebreak
  , "  This command will originate contract with code stored in add1.tz", linebreak
  , "  on real network with initial balance 1 and initial storage set to 0", linebreak
  , "  and return info about operation: operation hash and originated contract address", linebreak
  , linebreak
  , "morley-client -A carthage.testnet.tezos.serokell.team -P 8732 transfer \\", linebreak
  , "  --from tz1akcPmG1Kyz2jXpS4RvVJ8uWr7tsiT9i6A \\", linebreak
  , "  --to KT1USbmjj6P2oJ54UM6HxBZgpoPtdiRSVABW --amount 1 --parameter 0", linebreak
  , linebreak
  , "  This command will perform tranfer to contract with address on real network", linebreak
  , "  KT1USbmjj6P2oJ54UM6HxBZgpoPtdiRSVABW with amount 1 and parameter 0", linebreak
  , "  as a result it will return operation hash"
  ]
