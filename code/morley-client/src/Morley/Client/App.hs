-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE InstanceSigs #-}

module Morley.Client.App
  ( MorleyClientEnv
  , MorleyClientM
  , runMorleyClientM
  ) where

import Colog (HasLog(..), Message)
import qualified Data.Aeson as Aeson
import qualified Data.Binary.Builder as Binary
import Fmt (Builder, build, (+|), (|+))
import Network.HTTP.Types (Status(..), renderQuery)
import Servant.Client (runClientM)
import Servant.Client.Core
  (ClientError(..), Request, RequestBody(..), RequestF(..), Response, ResponseF(..), RunClient(..))

import Morley.Client.Env (MorleyClientEnv'(..))
import Morley.Client.Logging (logDebug)
import qualified Morley.Client.RPC.API as API
import Morley.Client.RPC.Class
import Morley.Client.RPC.Error
import Morley.Client.RPC.Types (InternalError(..))
import Morley.Client.TezosClient.Class
import qualified Morley.Client.TezosClient.Impl as TezosClient
import Morley.Client.TezosClient.Types
import Tezos.Core (toMutez)
import Tezos.Crypto (Signature(..))
import qualified Tezos.Crypto.Ed25519 as Ed25519

----------------------------------------------------------------------------
-- App
----------------------------------------------------------------------------

type MorleyClientEnv = MorleyClientEnv' MorleyClientM

newtype MorleyClientM a = MorleyClientM
  { unMorleyClientM :: ReaderT MorleyClientEnv IO a }
  deriving newtype
    ( Functor, Applicative, Monad, MonadReader MorleyClientEnv
    , MonadIO, MonadThrow, MonadCatch, MonadMask
    )

runMorleyClientM :: MorleyClientEnv -> MorleyClientM a -> IO a
runMorleyClientM env client = runReaderT (unMorleyClientM client) env

instance HasLog MorleyClientEnv Message MorleyClientM where
  getLogAction = mceLogAction
  setLogAction action mce = mce { mceLogAction = action }

instance HasTezosClient MorleyClientM where
  signBytes senderAlias opHash = do
    env <- ask
    case mceSecretKey env of
      Just sk -> pure . SignatureEd25519 $ Ed25519.sign sk opHash
      Nothing -> TezosClient.signBytes senderAlias opHash
  waitForOperation = TezosClient.waitForOperationInclusion
  rememberContract = TezosClient.rememberContract
  resolveAddress = TezosClient.resolveAddress
  getAlias = TezosClient.getAlias
  getTezosClientConfig = do
    path <- tceTezosClientPath <$> view tezosClientEnvL
    mbDataDir <- tceMbTezosClientDataDir <$> view tezosClientEnvL
    liftIO $ TezosClient.getTezosClientConfig path mbDataDir
  genFreshKey = TezosClient.genFreshKey
  genKey = TezosClient.genKey
  revealKey = TezosClient.revealKey
  calcTransferFee = TezosClient.calcTransferFee
  calcOriginationFee = TezosClient.calcOriginationFee

instance RunClient MorleyClientM where
  runRequest :: Request -> MorleyClientM Response
  runRequest req = do
    logRequest req
    env <- mceClientEnv <$> ask
    response <- either throwClientError pure =<<
      liftIO (runClientM (runRequest req) env)
    response <$ logResponse response
  throwClientError err = case err of
    FailureResponse _ resp
      | 500 <- statusCode (responseStatusCode resp) ->
        handleInternalError (responseBody resp)
    _ -> throwM err
    where
      -- In some cases RPC returns important useful errors as internal ones.
      -- We try to parse the response to a list of 'InternalError'.
      -- If we receive one 'InternalError', we throw it wrapped into
      -- 'ClientInternalError', that's what we observed in most obvious cases.
      -- If we receive more than one, we wrap them into 'UnexpectedInternalErrors'.
      handleInternalError :: LByteString -> MorleyClientM a
      handleInternalError body = case Aeson.decode @[InternalError] body of
        Nothing -> throwM err
        Just [knownErr] -> throwM $ ClientInternalError knownErr
        Just errs -> throwM $ UnexpectedInternalErrors errs

instance HasTezosRpc MorleyClientM where
  getHeadBlock = API.getLastBlock API.nodeMethods
  getCounter = API.getCounter API.nodeMethods
  getBlockConstants = API.getBlockConstants API.nodeMethods
  getProtocolParameters = API.getProtocolParameters API.nodeMethods
  runOperation = API.runOperation API.nodeMethods
  preApplyOperations = API.preApplyOperations API.nodeMethods
  forgeOperation = API.forgeOperation API.nodeMethods
  injectOperation = API.injectOperation API.nodeMethods (Just "main")
  getContractScript = API.getScript API.nodeMethods
  getContractStorage = API.getStorage API.nodeMethods
  getContractBigMap = API.getBigMap API.nodeMethods
  getBigMapValue = API.getBigMapValue API.nodeMethods
  getBalance addr =
    toMutez . fromIntegral <$> API.getBalance API.nodeMethods addr

----------------------------------------------------------------------------
-- Servant client helpers
----------------------------------------------------------------------------

-- | Convert a bytestring to a string assuming this bytestring stores
-- something readable in UTF-8 encoding.
fromBS :: ConvertUtf8 Text bs => bs -> Text
fromBS = decodeUtf8

ppRequestBody :: RequestBody -> Builder
ppRequestBody = build .
  \case
    RequestBodyLBS lbs -> fromBS lbs
    RequestBodyBS bs -> fromBS bs
    RequestBodySource {} -> "<body is not in memory>"

-- | Pretty print a @servant@'s request.
-- Note that we print only some part of 'Request': method, request
-- path and query, request body. We don't print other things that are
-- subjectively less interesting such as HTTP version or media type.
-- But feel free to add them if you want.
ppRequest :: Request -> Builder
ppRequest Request {..} =
  fromBS requestMethod |+ " " +| fromBS (Binary.toLazyByteString requestPath)
  |+ fromBS (renderQuery True $ toList requestQueryString) |+
  maybe mempty (mappend "\n" . ppRequestBody . fst) requestBody

logRequest :: HasCallStack => Request -> MorleyClientM ()
logRequest req = logDebug $ "RPC request: " +| ppRequest req |+ ""

-- | Pretty print a @servant@'s response.
-- Note that we print only status and body,
-- the rest looks not so interesting in our case.
--
-- If response is not human-readable text in UTF-8 encoding it will
-- print some garbage.  Apparently we don't make such requests for now.
ppResponse :: Response -> Builder
ppResponse Response {..} =
  statusCode responseStatusCode |+ " " +|
  fromBS (statusMessage responseStatusCode) |+ "\n" +|
  fromBS responseBody  |+ ""

logResponse :: HasCallStack => Response -> MorleyClientM ()
logResponse resp = logDebug $ "RPC response: " +| ppResponse resp |+ ""
