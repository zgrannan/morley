-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module that defines some basic infrastructure
-- for mocking tezos-node RPC interaction.
module TestM
  ( ContractState (..)
  , ContractStateBigMap (..)
  , Handlers (..)
  , MockState (..)
  , TestError (..)
  , TestM
  , defaultHandlers
  , defaultMockState
  , runMockTest
  ) where

import Colog.Core.Class (HasLog(..))
import Colog.Message (Message)

import Michelson.Typed.Scope (PrintedValScope)
import Morley.Client
import Morley.Client.Logging (ClientLogAction)
import Morley.Client.RPC
import Morley.Client.TezosClient (CalcOriginationFeeData, CalcTransferFeeData, TezosClientConfig)
import Morley.Micheline
import Tezos.Address
import Tezos.Core
import Tezos.Crypto (Signature)
import Util.ByteString

-- | Reader environment to interact with the mock state.
data Handlers m = Handlers
  { -- HasTezosRpc
    hGetHeadBlock :: m Text
  , hGetCounter :: Address -> m TezosInt64
  , hGetBlockConstants :: Text -> m BlockConstants
  , hGetProtocolParameters :: m ProtocolParameters
  , hRunOperation :: RunOperation -> m RunOperationResult
  , hPreApplyOperations :: [PreApplyOperation] -> m [RunOperationResult]
  , hForgeOperation :: ForgeOperation -> m HexJSONByteString
  , hInjectOperation :: Text -> m Text
  , hGetContractScript :: Address -> m OriginationScript
  , hGetContractBigMap :: Address -> GetBigMap -> m GetBigMapResult
  , hGetBigMapValue :: Natural -> Text -> m Expression
  , hGetBalance :: Address -> m Mutez

  -- HasTezosClient
  , hSignBytes :: AddressOrAlias -> ByteString -> m Signature
  , hGenKey :: Alias -> m Address
  , hGenFreshKey :: Alias -> m Address
  , hRevealKey :: Alias -> m ()
  , hWaitForOperation :: Text -> m ()
  , hRememberContract :: Bool -> Address -> Alias -> m ()
  , hResolveAddress :: AddressOrAlias -> m Address
  , hGetAlias :: AddressOrAlias -> m Alias
  , hGetTezosClientConfig :: m TezosClientConfig
  , hCalcTransferFee
    :: forall t. PrintedValScope t => CalcTransferFeeData t -> m TezosInt64
  , hCalcOriginationFee
    :: forall cp st. PrintedValScope st => CalcOriginationFeeData cp st -> m TezosInt64

  -- HasLog
  , hLogAction :: ClientLogAction m
  }

defaultHandlers :: Handlers TestM
defaultHandlers = Handlers
  { hGetHeadBlock = throwM $ UnexpectedRpcCall "getHeadBlock"
  , hGetCounter = \_ -> throwM $ UnexpectedRpcCall "getCounter"
  , hGetBlockConstants = \_ -> throwM $ UnexpectedRpcCall "getBlockConstants"
  , hGetProtocolParameters = throwM $ UnexpectedRpcCall "getProtocolParameters"
  , hRunOperation = \_ -> throwM $ UnexpectedRpcCall "runOperation"
  , hPreApplyOperations = \_ -> throwM $ UnexpectedRpcCall "preApplyOperations"
  , hForgeOperation = \_ -> throwM $ UnexpectedRpcCall "forgeOperation"
  , hInjectOperation = \_ -> throwM $ UnexpectedRpcCall "injectOperation"
  , hGetContractScript = \_ -> throwM $ UnexpectedRpcCall "getContractScript"
  , hGetContractBigMap = \_ _ -> throwM $ UnexpectedRpcCall "getContractBigMap"
  , hGetBigMapValue = \_ _ -> throwM $ UnexpectedRpcCall "getBigMapValue"
  , hGetBalance = \_ -> throwM $ UnexpectedRpcCall "getBalance"

  , hSignBytes = \_ _ -> throwM $ UnexpectedClientCall "signBytes"
  , hGenKey = \_ -> throwM $ UnexpectedClientCall "genKey"
  , hGenFreshKey = \_ -> throwM $ UnexpectedRpcCall "genFreshKey"
  , hRevealKey = \_ -> throwM $ UnexpectedClientCall "revealKey"
  , hWaitForOperation = \_ -> throwM $ UnexpectedRpcCall "waitForOperation"
  , hRememberContract = \_ _ _ -> throwM $ UnexpectedClientCall "hRememberContract"
  , hResolveAddress = \_ -> throwM $ UnexpectedRpcCall "resolveAddress"
  , hGetAlias = \_ -> throwM $ UnexpectedRpcCall "getAlias"
  , hGetTezosClientConfig = throwM $ UnexpectedClientCall "getTezosClientConfig"
  , hCalcTransferFee = \_ -> throwM $ UnexpectedClientCall "calcTransferFee"
  , hCalcOriginationFee = \_ -> throwM $ UnexpectedClientCall "calcOriginationFee"

  , hLogAction = mempty
  }

-- | Type to represent contract state in the @MockState@.
-- This type can represent both implicit and explicit contracts.
-- For implicit contracts (i.e. addresses beginning with tz1, tz2, or tz3),
-- @csMbExplicit@ field should be Nothing.
data ContractState = ContractState
  { csCounter :: TezosInt64
  , csAlias :: Alias
  , csMbExplicit :: Maybe (OriginationScript, Maybe ContractStateBigMap)
  }

-- | Type to represent big_map in @ContractState@.
data ContractStateBigMap = ContractStateBigMap
  { csbmKeyType :: Expression
  , csbmValueType :: Expression
  , csbmMap :: Map Text ByteString
  -- ^ Real tezos bigmap also has deserialized keys and values
  , csbmId :: Natural
  -- ^ The big_map's ID
  }

newtype TestHandlers = TestHandlers {unTestHandlers :: Handlers TestM}

-- | Type to represent chain state in mock tests.
data MockState = MockState
  { msContracts :: Map Address ContractState
  , msHeadBlock :: Text
  , msBlockConstants :: BlockConstants
  , msProtocolParameters :: ProtocolParameters
  }

defaultMockState :: MockState
defaultMockState = MockState
  { msContracts = mempty
  , msHeadBlock = "HEAD"
  , msBlockConstants = BlockConstants "PROTOCOL" "CHAIN_ID"
  , msProtocolParameters = ProtocolParameters 257 1040000 60000
  }

type TestM = StateT MockState (ReaderT TestHandlers (Either SomeException))

runMockTest
  :: forall a. Handlers TestM -> MockState -> TestM a -> Either SomeException a
runMockTest handlers mockState action =
  runReaderT (evalStateT action mockState) (TestHandlers handlers)

getHandler :: (Handlers TestM -> fn) -> TestM fn
getHandler fn = fn . unTestHandlers <$> ask

-- | Various mock test errors.
data TestError
  = UnexpectedRpcCall Text
  | UnexpectedClientCall Text
  | UnknownContract Address
  | UnknownAddressAlias Alias
  | UnexpectedImplicitContract Address
  | ContractDoesntHaveBigMap Address
  | InvalidChainId
  | InvalidProtocol
  | CounterMismatch
  deriving stock Show

instance Exception TestError

instance HasLog TestHandlers Message TestM where
  getLogAction = hLogAction . unTestHandlers
  setLogAction action (TestHandlers handlers) =
    TestHandlers $ handlers { hLogAction = action }

instance HasTezosClient TestM where
  signBytes alias op = do
    h <- getHandler hSignBytes
    h alias op
  genKey alias = do
    h <- getHandler hGenKey
    h alias
  genFreshKey alias = do
    h <- getHandler hGenFreshKey
    h alias
  revealKey alias = do
    h <- getHandler hRevealKey
    h alias
  waitForOperation op = do
    h <- getHandler hWaitForOperation
    h op
  rememberContract replaceExisting addr alias = do
    h <- getHandler hRememberContract
    h replaceExisting addr alias
  resolveAddress originator = do
    h <- getHandler hResolveAddress
    h originator
  getAlias originator = do
    h <- getHandler hGetAlias
    h originator
  getTezosClientConfig =
    join $ getHandler hGetTezosClientConfig
  calcTransferFee transferData = do
    h <- getHandler hCalcTransferFee
    h transferData
  calcOriginationFee origData = do
    h <- getHandler hCalcOriginationFee
    h origData

instance HasTezosRpc TestM where
  getHeadBlock =
    join $ getHandler hGetHeadBlock
  getCounter addr = do
    h <- getHandler hGetCounter
    h addr
  getBlockConstants block = do
    h <- getHandler hGetBlockConstants
    h block
  getProtocolParameters =
    join $ getHandler hGetProtocolParameters
  runOperation op = do
    h <- getHandler hRunOperation
    h op
  preApplyOperations ops = do
    h <- getHandler hPreApplyOperations
    h ops
  forgeOperation op = do
    h <- getHandler hForgeOperation
    h op
  injectOperation op = do
    h <- getHandler hInjectOperation
    h op
  getContractScript addr = do
    h <- getHandler hGetContractScript
    h addr
  getContractStorage addr = do
    h <- getHandler hGetContractScript
    osStorage <$> h addr
  getContractBigMap addr getBigMap = do
    h <- getHandler hGetContractBigMap
    h addr getBigMap
  getBigMapValue bigMapId scriptExpr = do
    h <- getHandler hGetBigMapValue
    h bigMapId scriptExpr
  getBalance addr = do
    h <- getHandler hGetBalance
    h addr
