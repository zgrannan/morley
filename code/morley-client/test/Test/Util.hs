-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module with various helpers that are used in morley-client mock tests.
module Test.Util
  ( chainOperationHandlers
  , dumbContractState
  , dumbImplicitContractState
  , mapToContractStateBigMap
  , handleGetBigMapValue
  ) where

import Data.Aeson (encode)
import qualified Data.ByteString.Lazy as LBS (toStrict)
import Data.Map as Map (elems, fromList, lookup, toList)
import Data.Singletons (demote)
import Network.HTTP.Types.Status (status404)
import Network.HTTP.Types.Version (http20)
import Servant.Client.Core
  (BaseUrl(..), ClientError(..), RequestF(..), ResponseF(..), Scheme(..), defaultRequest)

import Lorentz as L (compileLorentz, drop)
import Lorentz.Constraints
import Lorentz.Pack
import Michelson.Runtime.GState (genesisAddress6)
import Michelson.Typed
import Morley.Client.RPC.Types
import Morley.Client.TezosClient.Types
import Morley.Micheline
import TestM
import Tezos.Address
import Tezos.Crypto
import qualified Tezos.Crypto.Ed25519 as Ed25519
import Util.ByteString

import Morley.Client (BigMapId(..))

-- | Function to convert given map to big map representation
-- used in mock state.
mapToContractStateBigMap
  :: forall k v. (NicePackedValue k, NicePackedValue v)
  => BigMapId k v -> Map k v -> ContractStateBigMap
mapToContractStateBigMap (BigMapId bigMapId) map' = ContractStateBigMap
  { csbmKeyType = toExpression $ demote @(ToT k)
  , csbmValueType = toExpression $ demote @(ToT v)
  , csbmMap = fromList $
    map (bimap (encodeBase58Check . valueToScriptExpr) lEncodeValue) $
    Map.toList map'
  , csbmId = bigMapId
  }

-- | Initial simple contract mock state.
dumbContractState :: ContractState
dumbContractState = ContractState
  { csCounter = 100500
  , csAlias = "genesis2"
  , csMbExplicit = Just
    ( OriginationScript
      { osCode = toExpression $ compileLorentz L.drop
      , osStorage = toExpression $ toVal ()
      }
    , Nothing
    )
  }

dumbImplicitContractState :: ContractState
dumbImplicitContractState = ContractState
  { csCounter = 100500
  , csAlias = "genesis1"
  , csMbExplicit = Nothing
  }

-- | Mock handlers used for transaction sending and contract origination.
chainOperationHandlers :: Handlers TestM
chainOperationHandlers = defaultHandlers
  { hGetHeadBlock = handleGetHeadBlock
  , hGetCounter = handleGetCounter
  , hGetBlockConstants = handleGetBlockConstants
  , hGetProtocolParameters = handleGetProtocolParameters
  , hRunOperation = handleRunOperation
  , hPreApplyOperations = mapM handlePreApplyOperation
  , hForgeOperation = pure . HexJSONByteString . LBS.toStrict . encode
  , hInjectOperation = pure . (<> "_injected")
  , hGetContractScript = handleGetContractScript
  , hSignBytes =
    \_ -> pure . SignatureEd25519 . Ed25519.sign testSecretKey
  , hWaitForOperation = const pass
  , hGetAlias = handleGetAlias
  , hResolveAddress = handleResolveAddress
  , hRememberContract = \_ _ _ -> pure ()
  , hCalcTransferFee = \_ -> pure 100500
  , hCalcOriginationFee = \_ -> pure 100500
  }
  where
    testSecretKey :: Ed25519.SecretKey
    testSecretKey = Ed25519.detSecretKey "\001\002\003\004"

mkRunOperationResult :: [Address] -> RunOperationResult
mkRunOperationResult originatedContracts = RunOperationResult
  { rrOperationContents =
    [ OperationContent $ RunMetadata
      { rmOperationResult = OperationApplied $
        AppliedResult 100500 100500 100500 originatedContracts 0
      , rmInternalOperationResults = []
      }
    ]
  }

handleGetHeadBlock :: TestM Text
handleGetHeadBlock = do
  MockState{..} <- get
  pure $ msHeadBlock

handleGetCounter
  :: ( MonadState MockState m
     , MonadThrow m
     )
  => Address -> m TezosInt64
handleGetCounter addr = do
  MockState{..} <- get
  case lookup addr msContracts of
    Nothing -> throwM $ UnknownContract addr
    Just ContractState{..} -> pure $ csCounter

handleGetBlockConstants
  :: MonadState MockState m
  => anything -> m BlockConstants
handleGetBlockConstants _ = do
  MockState{..} <- get
  pure $ msBlockConstants

handleGetProtocolParameters
  :: MonadState MockState m
  => m ProtocolParameters
handleGetProtocolParameters = do
  MockState{..} <- get
  pure $ msProtocolParameters

handleRunOperation :: RunOperation -> TestM RunOperationResult
handleRunOperation RunOperation{..} = do
  MockState{..} <- get
  -- Ensure that passed chain id matches with one that mock state has
  unless (roChainId == bcChainId msBlockConstants) (throwM $ InvalidChainId)
  originatedContracts <- handleRunOperationInternal roOperation
  pure $ mkRunOperationResult originatedContracts

handlePreApplyOperation :: PreApplyOperation -> TestM RunOperationResult
handlePreApplyOperation PreApplyOperation{..} = do
  MockState{..} <- get
  -- Ensure that passed protocol matches with one that mock state has
  unless (paoProtocol == bcProtocol msBlockConstants) (throwM $ InvalidProtocol)
  originatedContracts <- concatMapM handleTransactionOrOrigination paoContents
  pure $ mkRunOperationResult originatedContracts

handleRunOperationInternal :: RunOperationInternal -> TestM [Address]
handleRunOperationInternal RunOperationInternal{..} = do
  concatMapM handleTransactionOrOrigination roiContents

handleTransactionOrOrigination
  :: Either TransactionOperation OriginationOperation -> TestM [Address]
handleTransactionOrOrigination op = do
  MockState{..} <- get
  case op of
    -- Ensure that transaction sender exists
    Left TransactionOperation{..} -> case lookup codSource msContracts of
      Nothing -> throwM $ UnknownContract codSource
      Just ContractState{..} -> do
        -- Ensure that sender counter matches
        unless (csCounter + 1 == codCounter) (throwM CounterMismatch)
        case lookup toDestination msContracts of
          Nothing -> throwM $ UnknownContract toDestination
          Just _ -> pure []
      where
        CommonOperationData{..} = toCommonData
    -- Ensure that originator exists
    Right OriginationOperation{..} -> case lookup codSource msContracts of
      Nothing -> throwM $ UnknownContract codSource
      Just ContractState{..} -> do
        -- Ensure that originator counter matches
        unless (csCounter + 1 == codCounter) (throwM CounterMismatch)
        pure [genesisAddress6]
      where
        CommonOperationData{..} = ooCommonData

handleGetContractScript
  :: ( MonadState MockState m
     , MonadThrow m
     )
  => Address
  -> m OriginationScript
handleGetContractScript addr = do
  MockState{..} <- get
  case lookup addr msContracts of
    Nothing -> throwM $ err404 path
    Just ContractState{..} -> case csMbExplicit of
      Nothing -> throwM $ UnexpectedImplicitContract addr
      Just (script, _) -> pure script
  where
    path = "/chains/main/blocks/head/context/contracts/" <> formatAddress addr <> "/script"

handleGetBigMapValue :: Natural -> Text -> TestM Expression
handleGetBigMapValue bigMapId scriptExpr = do
  st <- get

  let allBigMaps :: [ContractStateBigMap] =
        catMaybes $
          Map.elems (msContracts st) <&> csMbExplicit <&> \case
            Just (_, bigMapMaybe) -> bigMapMaybe
            _ -> Nothing

  -- Check if a big_map with the given ID exists and, if so, check
  -- whether the giv en key exists.
  case find (\bigMap -> csbmId bigMap == bigMapId) allBigMaps of
    Nothing -> throwM $ err404 path
    Just bigMap ->
      case lookup scriptExpr (csbmMap bigMap ) of
        Nothing -> throwM $ err404 path
        Just serializedValue -> pure $ decodeExpression serializedValue
  where
    path = "/chains/main/blocks/head/context/big_maps/" <> show bigMapId <> "/" <> scriptExpr

handleGetAlias :: AddressOrAlias -> TestM Alias
handleGetAlias = \case
  AddressAlias alias -> pure alias
  AddressResolved addr -> do
    MockState{..} <- get
    case lookup addr msContracts of
      Nothing -> throwM $ UnknownContract addr
      Just ContractState{..} -> pure $ csAlias

handleResolveAddress :: AddressOrAlias -> TestM Address
handleResolveAddress = \case
  AddressResolved addr -> pure addr
  AddressAlias alias -> do
    MockState{..} <- get
    case find checkAlias $ Map.toList msContracts of
      Just (addr, _) -> pure addr
      Nothing -> throwM $ UnknownAddressAlias alias
    where
      checkAlias (_, ContractState { csAlias = alias' }) = alias == alias'

-- In scenarios where the system under test checks for 404 errors, we
-- use this function to mock and simulate those errors.
err404 :: Text -> ClientError
err404 path = FailureResponse
  (defaultRequest { requestBody = Nothing, requestPath = (baseUrl , "") })
  response
  where
    baseUrl = BaseUrl
      { baseUrlScheme = Http
      , baseUrlHost = "localhost"
      , baseUrlPort = 8732
      , baseUrlPath = toString path
      }
    response = Response
      { responseStatusCode = status404
      , responseHeaders = mempty
      , responseHttpVersion = http20
      , responseBody = "Contract with given address not found"
      }
