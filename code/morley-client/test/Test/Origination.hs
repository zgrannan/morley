-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Origination
  ( test_lRunTransactionsUnit
  ) where

import Data.Map (fromList)
import Test.Hspec.Expectations (shouldThrow)
import Test.HUnit (Assertion, assertFailure)
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)

import qualified Lorentz as L
import Michelson.Runtime.GState (genesisAddress1, genesisAddress2, genesisAddress3)
import Morley.Client
import Test.Util
import TestM
import Tezos.Core

mockState
  :: MockState
mockState = defaultMockState
  { msContracts = fromList $
    [ (genesisAddress1, dumbImplicitContractState)
    , (genesisAddress2, dumbContractState)
    ]
  }

dumbLorentzContract :: L.Contract Integer ()
dumbLorentzContract = L.defaultContract $
  L.drop L.# L.unit L.# L.nil L.# L.pair

test_lRunTransactionsUnit :: TestTree
test_lRunTransactionsUnit = testGroup "Mock test transaction sending"
  [ testCase "Successful origination" $ handleSuccess $
    runMockTest chainOperationHandlers mockState $
      lOriginateContract True "dummy" (AddressResolved genesisAddress1) (toMutez 100500) dumbLorentzContract ()
  , testCase "Originator doesn't exist" $ handleUnknownContract $
    runMockTest chainOperationHandlers mockState $
      lOriginateContract True "dummy" (AddressResolved genesisAddress3) (toMutez 100500) dumbLorentzContract ()
  ]
  where
    handleSuccess :: Either SomeException a -> Assertion
    handleSuccess (Right _) = pass
    handleSuccess (Left e) = assertFailure $ displayException e

    handleUnknownContract :: Either SomeException a -> Assertion
    handleUnknownContract (Right _) =
      assertFailure "Origination unexpectedly didn't fail."
    handleUnknownContract (Left e) =
      shouldThrow (throwM e) $ \case
        UnknownContract _ -> True
        _ -> False
