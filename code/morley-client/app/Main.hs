-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Main
  ( main
  ) where

import Fmt (pretty)
import qualified Options.Applicative as Opt

import Michelson.Runtime (prepareContract)
import Michelson.TypeCheck (SomeContract(..), typeCheckContract, typeVerifyParameter)
import Michelson.Typed (Contract(..))
import Michelson.Typed.Value (Value' (..))
import qualified Michelson.Untyped as U
import Morley.Client
import Morley.Client.Parser
import Morley.Client.RPC.Getters (getContractsParameterTypes)
import Morley.Client.Util (extractAddressesFromValue)
import Tezos.Address (formatAddress, Address (..))
import Util.Exception (throwLeft, TextException (..))

mainImpl :: ClientArgsRaw -> MorleyClientM ()
mainImpl cmd =
  case cmd of
    Originate OriginateArgs{..} -> do
      contract <- liftIO $ prepareContract oaMbContractFile
      let originator = AddressResolved oaOriginateFrom
      (operationHash, contractAddr) <-
        originateUntypedContract True oaContractName originator oaInitialBalance contract oaInitialStorage
      putTextLn $ "Contract was succesfully deployed.\nOperation hash " <>
        operationHash <> ".\nContractAddress " <> formatAddress contractAddr

    Transfer TransferArgs{..} -> do
      operationHash <- case taDestination of
        ContractAddress _ -> do
          contract <- getContract taDestination
          SomeContract fullContract <-
            throwLeft $ pure $ typeCheckContract contract
          case fullContract of
            (Contract{} :: Contract cp st) -> do
              let addrs = extractAddressesFromValue taParameter
              tcOriginatedContracts <- getContractsParameterTypes addrs
              parameter <- throwLeft $ pure $
                typeVerifyParameter @cp tcOriginatedContracts taParameter
              transfer taSender taDestination taAmount U.DefEpName parameter
        KeyAddress _ -> case taParameter of
          U.ValueUnit -> transfer taSender taDestination taAmount U.DefEpName VUnit
          _ -> throwM $ TextException ("The transaction parameter must be 'Unit' "
            <> "when transferring to an implicit account")

      putTextLn $ "Transaction was succesfully sent.\nOperation hash " <> operationHash <> "."

    GetBalance addr -> do
      balance <- getBalance addr
      putTextLn $ pretty balance

main :: IO ()
main = do
  disableAlphanetWarning
  ClientArgs parsedConfig cmd <- Opt.execParser morleyClientInfo
  env <- mkMorleyClientEnv parsedConfig
  runMorleyClientM env (mainImpl cmd)
