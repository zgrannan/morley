-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | The implementation of Unsafe ledger with V1 balance bug fixed

module Lorentz.Contracts.UpgradeableUnsafeLedger.V2
  ( UnsafeLedgerV2
  , migrate
  , unsafeLedgerContract
  ) where

import Lorentz

import Lorentz.Contracts.Upgradeable.Common
import qualified Lorentz.Contracts.UpgradeableUnsafeLedger.V1 as V1

data UnsafeLedgerV2 :: VersionKind

-- The storage does not change
type UStoreV2 = V1.UStoreV1

type Interface = V1.Interface

instance KnownContractVersion UnsafeLedgerV2 where
  type VerInterface UnsafeLedgerV2 = Interface
  type VerUStoreTemplate UnsafeLedgerV2 = VerUStoreTemplate V1.UnsafeLedgerV1
  contractVersion _ = 2

-- | Storage migration function. Since the storage is the same,
--   there's nothing to migrate
migrate :: '[UStore_] :-> '[UStore_]
migrate = nop

-- | The second version of the UpgradeableUnsafeLedger.
--   Most of the functions are from V1 except for getBalance.
unsafeLedgerContract :: UContractRouter UnsafeLedgerV2
unsafeLedgerContract = mkUContractRouter $ do
  caseUParamT @Interface
    ( #transfer /-> V1.transfer
    , #getTotalSupply /-> V1.getTotalSupply
    , #getBalance /-> getBalance
    )
    uparamFallbackFail

-- Note that the new getBalance returns the correct balance
getBalance :: '[Void_ Address (Maybe Natural), UStoreV2]
           :-> '[([Operation], UStoreV2)]
getBalance = void_ (ustoreGet #ledger)
