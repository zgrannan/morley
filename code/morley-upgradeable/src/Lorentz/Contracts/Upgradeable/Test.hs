-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Lorentz.Contracts.Upgradeable.Test
  ( -- * Test predicates
    testUpgradeableContractDoc

    -- * Individual test predicates
  , testSingleVersion
  ) where

import Prelude

import Test.HUnit (assertFailure)

import Lorentz.Contracts.Upgradeable.Common
import Lorentz.Test.Doc

-- | Check that contract documentation mentions version only once.
testSingleVersion :: DocTest
testSingleVersion =
  mkDocTest "Version documented" $
  \doc ->
    case forEachContractDocItem @DVersion doc id of
      [] -> assertFailure "Contract contains no 'DVersion'"
      [_] -> pass
      _ -> assertFailure "Contract contains several 'DVersion's"

-- | Test all properties of upgradeable contract.
testUpgradeableContractDoc :: [DocTest]
testUpgradeableContractDoc = mconcat
  [ testLorentzDoc
  , [ testSingleVersion
    ]
  ]
