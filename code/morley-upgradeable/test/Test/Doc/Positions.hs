-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests on ordering of documentation items.
module Test.Doc.Positions
  ( test_Errors
  ) where

import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Cleveland.Util (goesBefore)
import Lorentz.Contracts.Upgradeable.Common.Base (PermanentEntrypointsKind, UpgradeableEntrypointsKind)
import Lorentz.Entrypoints.Doc (DEntrypoint, PlainEntrypointsKind)

test_Errors :: [TestTree]
test_Errors =
  [ testCase "`PlainEntrypointsKind` should come before `PermanentEntrypointsKind`" $
      Proxy @(DEntrypoint PlainEntrypointsKind) `goesBefore` Proxy @(DEntrypoint PermanentEntrypointsKind)
  , testCase "`PermanentEntrypointsKind` should come before `UpgradeableEntrypointsKind`" $
      Proxy @(DEntrypoint PermanentEntrypointsKind) `goesBefore` Proxy @(DEntrypoint UpgradeableEntrypointsKind)
  ]
