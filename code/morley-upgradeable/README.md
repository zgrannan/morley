# Morley Upgradeable

This package contains infrastructure that one can use to implement an upgradeable contract.
For overview of upgradeability approaches please read [the related document](/docs/upgradeableContracts.md).
