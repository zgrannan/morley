-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module containing some utilities for testing Michelson contracts using
-- Haskell testing frameworks (hspec and QuickCheck in particular).
-- It's Morley testing EDSL.

module Michelson.Test
  ( -- * Importing a contract
    specWithContract
  , specWithTypedContract
  , specWithUntypedContract
  , testTreesWithContract
  , testTreesWithUntypedContract
  , testTreesWithTypedContract
  , concatTestTrees
  , importContract
  , importSomeContract
  , importUntypedContract

  -- * Unit testing
  , ContractReturn
  , ContractPropValidator
  , contractProp
  , contractPropVal
  , validateSuccess
  , validateStorageIs
  , validateMichelsonFailsWith

  -- * Integrational testing
  -- ** Testing engine
  , IntegrationalScenario
  , IntegrationalScenarioM
  , integrationalTestExpectation
  , integrationalTestProp
  , originate
  , tOriginate
  , transfer
  , tTransfer
  , integrationalFail
  , unexpectedInterpreterError
  , setMaxSteps
  , setNow
  , rewindTime
  , withSender
  , setChainId
  , branchout
  , (?-)

  -- ** Validators
  , expectNoStorageUpdates
  , expectStorageUpdate
  , expectStorageUpdateConst
  , expectBalance
  , expectStorage
  , expectStorageConst
  , tExpectStorageConst

  -- ** Errors
  , attempt
  , expectError
  , catchExpectedError
  , expectGasExhaustion
  , expectMichelsonFailed

  -- ** Various
  , TxData (..)
  , TxParam (..)
  , genesisAddress

  -- * General utilities
  , failedTest
  , succeededTest
  , eitherIsLeft
  , eitherIsRight
  , meanTimeUpperBoundProp
  , meanTimeUpperBoundPropNF

  -- * Re-exports
  --
  -- | These functions from "Time" are re-exported here to make it convenient to call
  -- 'meanTimeUpperBoundProp' and 'meanTimeUpperBoundPropNF'.
  , mcs, ms, sec, minute

  -- * Autodoc testing
  , runDocTests
  , testDocBasic
  , excludeDocTests

  -- * Dummy values
  , dummyContractEnv


  -- * Deprecated
  , integrationalTestProperty
  , failedProp
  , succeededProp
  , qcIsLeft
  , qcIsRight
  ) where

import Cleveland.Util
import Michelson.Doc.Test
import Michelson.Test.Dummy
import Michelson.Test.Import
import Michelson.Test.Integrational
import Michelson.Test.Unit
