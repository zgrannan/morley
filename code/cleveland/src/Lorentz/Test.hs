-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Lorentz.Test
  ( -- * Importing a contract
    specWithContract
  , specWithTypedContract
  , specWithUntypedContract

  -- * Unit testing
  , ContractReturn
  , ContractPropValidator
  , contractProp
  , contractPropVal
  , expectContractEntrypoints

  -- * Integrational testing
  -- ** Testing engine
  , IntegrationalScenario
  , IntegrationalScenarioM
  , TestError (..)
  , integrationalTestExpectation
  , integrationalTestProp
  , lOriginate
  , lOriginateEmpty
  , lTransfer
  , lCall
  , lCallEP
  , EntrypointRef (..)
  , lCallDef
  , integrationalFail
  , unexpectedInterpreterError
  , setMaxSteps
  , setNow
  , rewindTime
  , withSender
  , setChainId
  , branchout
  , (?-)
  , offshoot

  -- ** Validators
  , expectNoStorageUpdates
  , lExpectStorageUpdate
  , lExpectBalance
  , lExpectStorage
  , lExpectStorageConst

  -- ** Errors
  , attempt
  , expectError
  , catchExpectedError
  , lExpectMichelsonFailed
  , lExpectFailWith
  , lExpectError
  , lExpectErrorNumeric
  , lExpectCustomError
  , lExpectCustomErrorNumeric
  , lExpectCustomError_
  , lExpectCustomErrorNumeric_
  , lExpectConsumerStorage
  , lExpectViewConsumerStorage

  -- ** Various
  , TxData (..)
  , TxParam (..)
  , genesisAddresses
  , genesisAddress
  , genesisAddress1
  , genesisAddress2
  , genesisAddress3
  , genesisAddress4
  , genesisAddress5
  , genesisAddress6

  -- * Autodoc testing
  , runDocTests
  , testLorentzDoc
  , excludeDocTests

  -- * General utilities
  , failedTest
  , succeededTest
  , eitherIsLeft
  , eitherIsRight
  , meanTimeUpperBoundProp
  , meanTimeUpperBoundPropNF

  -- * Re-exports
  --
  -- | These functions from "Time" are re-exported here to make it convenient to call
  -- 'meanTimeUpperBoundProp' and 'meanTimeUpperBoundPropNF'.
  , mcs, ms, sec, minute

  -- * Dummy values
  , dummyContractEnv

  -- * Special contracts for testing
  , contractConsumer

  -- * Deprecated
  , integrationalTestProperty
  , failedProp
  , succeededProp
  , qcIsLeft
  , qcIsRight
  ) where

import Cleveland.Util
import Michelson.Doc.Test
import Michelson.Test.Dummy
import Michelson.Test.Import
import Michelson.Test.Unit

import Lorentz.Test.Consumer
import Lorentz.Test.Doc
import Lorentz.Test.Integrational
import Lorentz.Test.Unit
