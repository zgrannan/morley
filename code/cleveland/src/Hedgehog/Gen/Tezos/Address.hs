-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Address in Tezos.

module Hedgehog.Gen.Tezos.Address
  ( genAddress
  ) where

import Hedgehog (MonadGen)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import Tezos.Address (Address(..), ContractHash(..))

import Hedgehog.Gen.Tezos.Crypto (genKeyHash)

genAddress :: MonadGen m => m Address
genAddress = Gen.choice [genKeyAddress, genContractAddress]
  where
    genKeyAddress = KeyAddress <$> genKeyHash
    genContractAddress = ContractAddress . ContractHash <$> Gen.bytes (Range.singleton 20)
