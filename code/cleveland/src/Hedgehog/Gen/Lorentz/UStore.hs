-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Hedgehog.Gen.Lorentz.UStore
  ( genUStoreSubMap
  , genUStoreFieldExt
  ) where

import Hedgehog (MonadGen)
import qualified Hedgehog.Gen as Gen
import qualified Hedgehog.Range as Range

import Lorentz.UStore.Types (UStoreFieldExt(..), type (|~>) (..))

import Cleveland.Util (genTuple2)

genUStoreSubMap :: (MonadGen m, Ord k) => m k -> m v -> m (k |~> v)
genUStoreSubMap genK genV = UStoreSubMap <$> Gen.map (Range.linear 0 100) (genTuple2 genK genV)

genUStoreFieldExt :: MonadGen m => m v -> m (UStoreFieldExt marker v)
genUStoreFieldExt genV = UStoreField <$> genV
