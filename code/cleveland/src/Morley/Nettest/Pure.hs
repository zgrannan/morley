-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Integration with integrational testing engine (pun intended).

module Morley.Nettest.Pure
  ( runNettestViaIntegrational
  , nettestTestExpectation
  , nettestTestProp
  , nettestToIntegrational
  , BadScenario(..)
  ) where

import qualified Data.Map as Map
import Data.Singletons (SingI, demote)
import Data.Typeable (gcast)
import Fmt (pretty)
import Hedgehog (MonadTest)
import Test.Hspec (Expectation)
import Test.HUnit (Counts(..), Test(..), runTestTT)

import Lorentz (ToAddress, toAddress)
import Lorentz.Entrypoints (TrustEpName(..))
import Lorentz.Test
import Michelson.Interpret (MichelsonFailed(..))
import Michelson.Runtime (AddressState(..), ContractState(..), ExecutorError, ExecutorError'(..))
import Michelson.Runtime.GState (GState(..), asBalance, genesisSecretKey)
import Michelson.Test.Integrational
  (addrToAddrName, expectBalance, expectMichelsonFailed, isGState, originate, tExpectStorageConst)
import Michelson.Typed (ShiftArithErrorType(..), SomeValue'(SomeValue), toVal)
import qualified Michelson.Typed as T
import Michelson.Typed.Arith (ArithError(..))
import Morley.Client (Alias)
import Tezos.Address
import Tezos.Crypto (PublicKey(..), SecretKey(..), Signature(..))
import qualified Tezos.Crypto.Ed25519 as Ed25519
import Util.Named

import Morley.Nettest.Abstract

{-# ANN nettestImplIntegrational ("HLint: ignore Use tuple-section" :: Text) #-}

data BadScenario = BadScenario
  deriving stock (Show, Eq)

instance Exception BadScenario

-- | Run a 'NettestScenario' by converting it to integrational
-- scenario and then passing to HUnit engine.
-- Throws an exception in case of an unexpected failure.
--
-- It is suitable for very simple test-suites where we just want to
-- test scenario quickly first and then run it on real network in case
-- we consider it valid.
runNettestViaIntegrational :: NettestScenario -> IO ()
runNettestViaIntegrational scenario =
  checkCounts . runTestTT . TestCase . integrationalTestExpectation $
    nettestToIntegrational scenario
  where
    checkCounts action =
      action >>= \case
        Counts {..}
          | errors > 0 || failures > 0 -> throwM BadScenario
          | otherwise -> pass


-- | Nettest test that executes given operations and validates
-- them. It can fail using 'Expectation' capability.
-- Uses 'integrationalTestExpectation' on a `NettestScenario`,
-- transformed into `IntegrationalScenario`
nettestTestExpectation :: NettestScenario -> Expectation
nettestTestExpectation ntScenario = integrationalTestExpectation $ nettestToIntegrational ntScenario

-- | Nettest test similar to 'nettestTestExpectation'.
-- It can fail using 'Property' capability.
-- Uses 'integrationalTestExpectation' on a `NettestScenario`,
-- transformed into `IntegrationalScenario`
nettestTestProp :: MonadTest m => NettestScenario -> m ()
nettestTestProp ntScenario = integrationalTestProp $ nettestToIntegrational ntScenario

-- | Build 'IntegrationalScenario' out of 'NettestScenario'.
-- Also accepts a 'NettestExpectation' to validate the result of the scenario
nettestToIntegrational :: NettestScenario -> IntegrationalScenario
nettestToIntegrational scenario =
  scenario nettestImplIntegrational `evalStateT`
    one ( nettestAddressAlias
        , AliasData genesisAddress $
          Just $ unwrappedGenesisSecretKey
        )
  where
    unwrappedGenesisSecretKey = case genesisSecretKey of
      SecretKeyEd25519 ed25519sk -> ed25519sk
      _ -> error "other curves aren't supported yet"

type Aliases = Map Text AliasData

-- | Datatype to store alias data, we store optional 'SecretKey' in addition
-- to 'Address' in order to support bytes signing.
data AliasData = AliasData
  { adAddress :: Address
  , adMbSecretKey :: Maybe Ed25519.SecretKey
  }

nettestImplIntegrational ::
     forall m. m ~ StateT Aliases IntegrationalScenarioM
  => NettestImpl m
nettestImplIntegrational =
  NettestImpl
    { niResolveAddress = resolve

    , niGetAlias = getAlias

    , niSignBytes = \bs alias -> do
        mbSk <- gets (Map.lookup alias) >>= maybe (unknownAlias alias) (pure . adMbSecretKey)
        case mbSk of
          Nothing ->
            lift . integrationalFail . CustomTestError .
            mappend "Given address doesn't have known associated secret key: " . show $ alias
          Just sk -> pure $ SignatureEd25519 $ Ed25519.sign sk bs

    , niGenKey = smartGenKey Nothing

    , niGenFreshKey =
        \alias -> gets (Map.lookup alias) >>= \a -> smartGenKey (adAddress <$> a) alias

    , niOriginateUntyped = \UntypedOriginateData {..} -> do
        ref <-
          lift $ originate uodContract uodName uodStorage uodBalance
        saveAlias uodName (toAddress ref) Nothing

    , niTransfer = \TransferData {..} -> do
        fromAddr <- #from <.!> resolve tdFrom
        toAddr <- #to <.!> resolve tdTo
        -- Here @toAddr@ is 'Address', so we can not check anything
        -- about it and assume that entrypoint is correct. We pass
        -- unit as contract parameter because it won't be checked
        -- anyway.
        lift $ lTransfer @() fromAddr toAddr tdAmount
          (TrustEpName tdEntrypoint) tdParameter

    -- Comments are not supported by integrational testing engine (yet).
    , niComment = const pass

    , niExpectFailure = \operation failure -> do
        err <- StateT $ \aliases ->
          runStateT operation aliases `catchExpectedError` \err -> pure (err, aliases)
        lift $ toExpectation failure err
    , niGetBalance = \addrOrAlias -> do
        addr <- resolve addrOrAlias
        GState{..} <- lift $ use isGState
        maybe (unknownAddress addr) (pure . asBalance) $ Map.lookup addr gsAddresses
    , niCheckBalance = \addrOrAlias expected -> do
        addr <- resolve addrOrAlias
        lift $ expectBalance addr expected
    , niCheckStorage = \addrOrAlias expectedStorage -> do
        addr <- resolve addrOrAlias
        lift $ tExpectStorageConst addr expectedStorage
    , niGetPublicKey = \addrOrAlias -> do
        mbSk <- case addrOrAlias of
          AddressAlias name ->
            gets (Map.lookup name) >>=
            maybe (unknownAlias name) (pure . adMbSecretKey)
          AddressResolved addr ->
            gets (fmap (adMbSecretKey . snd) . find
                  (\(_, AliasData addr' _) -> addr == addr') . Map.toList
                 ) >>= maybe (unknownAddress addr) pure
        case mbSk of
          Nothing ->
            lift . integrationalFail . CustomTestError .
            mappend "Given address doesn't have known associated public key: " . show $ addrOrAlias
          Just sk -> pure $ PublicKeyEd25519 $ Ed25519.toPublic sk
    , ..
    }
  where
    niGetStorage
      :: forall expectedT. (Typeable expectedT, SingI expectedT)
      => AddressOrAlias -> StateT Aliases IntegrationalScenarioM (T.Value expectedT)
    niGetStorage addrOrAlias = do
      addr <- resolve addrOrAlias
      GState{..} <- lift $ use isGState
      case Map.lookup addr gsAddresses of
        Just (ASContract (ContractState _ _ (storage :: T.Value actualT))) ->
          case gcast @actualT @expectedT storage of
            Nothing -> lift $ integrationalFail $ UnexpectedStorageType (demote @expectedT) (demote @actualT)
            Just val -> pure val
        Just (ASSimple {}) -> lift . integrationalFail . CustomTestError $
          "Expected address to be contract with storage, but it's a simple address: " <> show addr
        Nothing -> unknownAddress addr

    resolve :: AddressOrAlias -> m Address
    resolve = \case
      AddressResolved addr -> pure addr
      AddressAlias name ->
        gets (Map.lookup name) >>= maybe (unknownAlias name) (pure . adAddress)

    getAlias :: AddressOrAlias -> m Alias
    getAlias = \case
      AddressAlias alias -> pure alias
      AddressResolved addr ->
        gets (fmap fst . find (\(_, AliasData addr' _) -> addr == addr') . Map.toList)
        >>= maybe (unknownAddress addr) pure

    -- Generate a fresh address which was never generated for given alias.
    -- If the address is not saved, we use the alias as its seed.
    -- Otherwise we concatenate the alias with the saved address.
    smartGenKey :: Maybe Address -> Text -> m Address
    smartGenKey existingAddr alias =
      let
        seed = maybe alias (mappend alias . pretty) existingAddr
        sk = Ed25519.detSecretKey (encodeUtf8 seed)
        addr = detGenKeyAddress (encodeUtf8 seed)
       in saveAlias alias addr $ Just sk

    unknownAlias :: Text -> m whatever
    unknownAlias =
      lift . integrationalFail . CustomTestError .
      mappend "Unknown address alias: "

    unknownAddress :: Address -> m whatever
    unknownAddress =
      lift . integrationalFail . CustomTestError .
      mappend "Unknown address provided: " . show

    saveAlias :: Alias -> Address -> Maybe Ed25519.SecretKey -> m Address
    saveAlias name addr mbSk = addr <$ modify (Map.insert name (AliasData addr mbSk))

-- | Converts a 'NettestFailure' to an 'IntegrationalScenario' expectation.
toExpectation :: NettestFailure -> ExecutorError -> IntegrationalScenario
toExpectation expectedErr actualErr =
  case expectedErr of
    NettestFailedWith (toAddress -> expectedAddr) (toVal -> expectedVal) ->
      expectMichelsonFailed expectedAddr actualErr >>= \case
        MichelsonFailedWith actualVal
          | SomeValue expectedVal == SomeValue actualVal -> pass
          | otherwise -> integrationalFail $ UnexpectedFailWithValue
              (SomeValue expectedVal)
              (SomeValue actualVal)
        _ -> unexpectedInterpreterError actualErr "expected FAILWITH"

    NettestFailedWithError expectedErr' ->
      lExpectError (== expectedErr') actualErr

    NettestFailedWithNumericError errorTagMap expectedErr' ->
      lExpectErrorNumeric errorTagMap (== expectedErr') actualErr

    NettestShiftOverflow expectedAddr ->
      expectMichelsonFailed (toAddress expectedAddr) actualErr >>= \case
        MichelsonArithError (ShiftArithError errType _ _) | isOverflow errType -> pass
        _ -> unexpectedInterpreterError actualErr "expected shift overflow error"

    NettestEmptyTransaction expectedAddr
      | EEZeroTransaction actualAddr <- actualErr -> checkAddr expectedAddr actualAddr
      | otherwise -> unexpectedInterpreterError actualErr "expected empty transaction"

    NettestBadParameter
      | EEIllTypedParameter _ <- actualErr -> pass
      | EEUnexpectedParameterType _ _ <- actualErr -> pass
      | otherwise -> unexpectedInterpreterError actualErr "expected ill-typed parameter"

    -- TODO: [#284](https://gitlab.com/morley-framework/morley/-/issues/284)
    -- NettestMutezArithError _ -> pass
    -- NettestGasExhaustion -> pass

  where
    checkAddr :: ToAddress addr => addr -> Address -> IntegrationalScenario
    checkAddr (toAddress -> expectedAddr) actualAddr
      | expectedAddr /= actualAddr = do
          iState <- get
          unexpectedInterpreterError actualErr $
            "expected runtime failure for contract with address: "
            <> pretty (addrToAddrName expectedAddr iState)
            <> ", but found address: "
            <> pretty (addrToAddrName actualAddr iState)
      | otherwise = pass

    isOverflow :: ShiftArithErrorType -> Bool
    isOverflow = \case
      LslOverflow -> True
      LsrUnderflow -> False
