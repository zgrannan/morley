-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE InstanceSigs #-}

-- | @caps@-based interface for nettest.
module Morley.Nettest.Caps
  ( MonadNettest
  , NettestT
  , resolveAddress
  , resolveNettestAddress
  , getAlias
  , newAddress
  , newFreshAddress
  , signBytes
  , originate
  , originateSimple
  , originateUntyped
  , originateUntypedSimple
  , transfer
  , call
  , callFrom
  , comment
  , expectFailure
  , getBalance
  , checkBalance
  , getStorage
  , checkStorage
  , getPublicKey

  , uncapsNettest
  , nettestCapImpl

  -- * Internal helpers
  , ActionToCaps(..)
  ) where

import qualified Monad.Capabilities as Caps

import Lorentz (HasEntrypointArg, IsoValue, TAddress, ToTAddress)
import Lorentz.Constraints
import Lorentz.Run (Contract)
import Michelson.Typed (StorageScope, UnpackedValScope)
import qualified Michelson.Typed as T
import qualified Michelson.Untyped as U
import Morley.Client (Alias)
import Morley.Nettest.Abstract
import Tezos.Address (Address)
import Tezos.Core (Mutez)
import Tezos.Crypto (PublicKey, Signature)

-- | Constraint for a monad in which we can do nettest actions.
-- It requires 'NettestImpl' capability.
type MonadNettest caps base m =
  (Monad base, m ~ Caps.CapsT caps base, Caps.HasCap NettestImpl caps)

-- | Monad transformer that adds only 'NettestImpl' capability.
type NettestT m = Caps.CapsT '[ NettestImpl] m

-- | 'resolveAddressAction' adapted to @caps@.
resolveAddress :: MonadNettest caps base m => AddressOrAlias -> m Address
resolveAddress = actionToCaps resolveAddressAction

getAlias :: MonadNettest caps base m => AddressOrAlias -> m Alias
getAlias = actionToCaps getAliasAction

-- | 'resolveNettestAddressAction' adapted to @caps@.
resolveNettestAddress :: MonadNettest caps base m => m Address
resolveNettestAddress = actionToCaps resolveNettestAddressAction

-- | 'newAddressAction' adapted to @caps@.
newAddress :: MonadNettest caps base m => Alias -> m Address
newAddress = actionToCaps newAddressAction

-- | 'newFreshAddressAction' adapted to @caps@.
newFreshAddress :: MonadNettest caps base m => Alias -> m Address
newFreshAddress = actionToCaps newFreshAddressAction

-- | 'signBytesAction' adapted to @caps@.
signBytes :: MonadNettest caps bas m => ByteString -> Alias -> m Signature
signBytes = actionToCaps signBytesAction

-- | 'originateUntypedAction' adapted to @caps@
originateUntyped :: MonadNettest caps base m => UntypedOriginateData -> m Address
originateUntyped = actionToCaps originateUntypedAction

-- | 'originateUntypedSimpleAction' adapted to @caps@
originateUntypedSimple
  :: MonadNettest caps base m => Text -> U.Value -> U.Contract -> m Address
originateUntypedSimple = actionToCaps originateUntypedSimpleAction

-- | 'originateAction' adapted to @caps@.
originate :: MonadNettest caps base m => OriginateData param -> m (TAddress param)
originate = actionToCaps originateAction

-- | 'originateSimpleAction' adapted to @caps@.
originateSimple
  :: ( MonadNettest caps base m
     , NiceParameterFull param, NiceStorage st
     )
  => Text
  -> st
  -> Contract param st
  -> m (TAddress param)
originateSimple = actionToCaps originateSimpleAction

-- | 'transferAction' adapted to @caps@.
transfer :: MonadNettest caps base m => TransferData -> m ()
transfer = actionToCaps transferAction

-- | Call a certain entrypoint of a contract referred to by some
-- typed address. The sender is 'nettestAddress', so it should have sufficient
-- XTZ to pay tx fee.
call
  :: forall v addr m epRef epArg caps base.
     (MonadNettest caps base m, ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg)
  => addr
  -> epRef
  -> epArg
  -> m ()
call = callFrom @v nettestAddress

-- | Call a certain entrypoint of a contract referred to by the second
-- typed address from the first address.
callFrom
  :: forall v addr m epRef epArg caps base.
     (MonadNettest caps base m, ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg)
  => AddressOrAlias
  -> addr
  -> epRef
  -> epArg
  -> m ()
callFrom = actionToCaps (callFromAction @v)

-- | 'commentAction' adapted to @caps@.
comment :: MonadNettest caps base m => Text -> m ()
comment = actionToCaps commentAction

-- | 'expectFailureAction' adapted to @caps@.
expectFailure :: MonadNettest caps base m => m a -> NettestFailure -> m ()
expectFailure = actionToCaps expectFailureAction

-- | 'getBalanceAction' adapted to @caps@.
getBalance :: MonadNettest caps base m => AddressOrAlias -> m Mutez
getBalance = actionToCaps getBalanceAction

-- | 'checkBalanceAction' adapted to @caps@.
checkBalance :: MonadNettest caps base m => AddressOrAlias -> Mutez -> m ()
checkBalance = actionToCaps checkBalanceAction

-- | 'getStorageAction' adapted to @caps@
getStorage
  :: forall t caps base m. (MonadNettest caps base m, UnpackedValScope t, Typeable t)
  => AddressOrAlias
  -> m (T.Value t)
getStorage = actionToCaps getStorageAction

-- | 'checkStorageAction' adapted to @caps@
checkStorage
  :: forall t caps base m. (MonadNettest caps base m,  UnpackedValScope t, StorageScope t)
  => AddressOrAlias
  -> T.Value t
  -> m ()
checkStorage = actionToCaps checkStorageAction

-- | 'getPublicKeyAction' adapted to @caps@.
getPublicKey :: MonadNettest caps bas m => AddressOrAlias -> m PublicKey
getPublicKey = actionToCaps getPublicKeyAction

-- | Adapt @caps@-based interface back to argument passing style.
uncapsNettest
  :: forall m a. Monad m
  => NettestT m a
  -> NettestImpl m
  -> m a
uncapsNettest action impl =
  runReaderT action $
  Caps.buildCaps $
    Caps.AddCap (nettestCapImpl impl) $
    Caps.BaseCaps Caps.emptyCaps

nettestCapImpl :: Monad m => NettestImpl m -> Caps.CapImpl NettestImpl '[] m
nettestCapImpl impl = Caps.CapImpl $ NettestImpl
  { niResolveAddress = lift . niResolveAddress impl
  , niGetAlias = lift . niGetAlias impl
  , niSignBytes = lift ... niSignBytes impl
  , niGenKey = lift . niGenKey impl
  , niGenFreshKey = lift . niGenFreshKey impl
  , niOriginateUntyped = lift . niOriginateUntyped impl
  , niTransfer = lift . niTransfer impl
  , niComment = lift . niComment impl
  , niExpectFailure = \op failure -> do
      ReaderT $ \ r -> niExpectFailure impl (runReaderT op r) failure
  , niGetBalance = lift . niGetBalance impl
  , niCheckBalance = lift ... niCheckBalance impl
  , niGetStorage = lift . niGetStorage impl
  , niCheckStorage = lift ... niCheckStorage impl
  , niGetPublicKey = lift . niGetPublicKey impl
  }

----------------------------------------------------------------------------
-- Internal helpers
----------------------------------------------------------------------------

class ActionToCaps impl m x where
  actionToCaps :: (impl m -> x) -> x

instance (Monad base, m ~ Caps.CapsT caps base, Caps.HasCap impl caps, Typeable impl) => ActionToCaps impl m (m a) where
  actionToCaps :: (impl m -> m a) -> m a
  actionToCaps action = do
    impl <- asks Caps.getCap
    action impl

instance ActionToCaps impl m r => ActionToCaps impl m (x -> r) where
  actionToCaps :: (impl m -> (x -> r)) -> (x -> r)
  actionToCaps action x = actionToCaps (flip action x)
