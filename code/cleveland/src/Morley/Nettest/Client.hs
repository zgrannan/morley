-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Implementation that works with real Tezos network, it
-- talks to a Tezos node and uses `tezos-client`.

module Morley.Nettest.Client
  ( runNettestClient
  , nettestImplClient
  , revealKeyUnlessRevealed
  , TestError
  ) where

import Data.Constraint ((\\))
import Fmt (Buildable(build), pretty, (+|), (|+))
import Named (arg)
import System.IO (hFlush)
import Type.Reflection (SomeTypeRep(..), someTypeRep)

import Lorentz
  (NiceUnpackedValue, ToAddress(toAddress), errorToVal, errorToValNumeric, niceUnpackedValueEvi)
import Michelson.Typed (SomeValue, SomeValue'(SomeValue), ToT, UnpackedValScope, toVal)
import qualified Michelson.Typed as T
import Morley.Client (Alias, MorleyClientEnv, disableAlphanetWarning, runMorleyClientM)
import qualified Morley.Client as Client
import Morley.Client.Logging (logDebug)
import Morley.Client.RPC.Error
import qualified Morley.Client.TezosClient.Impl as TezosClient
  (genFreshKey, genKey, getAlias, getPublicKey, resolveAddress, revealKey, signBytes)
import Morley.Micheline (Expression, FromExpression(fromExpression), FromExpressionError)
import Tezos.Address (Address)
import Tezos.Core (Mutez)
import Util.Exception (displayUncaughtException)
import Util.Named ((:!), (.!))

import Cleveland.Util (formatSomeValue)
import Morley.Nettest.Abstract

-- | Run 'NettestScenario' using implementation for real network.
runNettestClient :: MorleyClientEnv -> NettestScenario -> IO ()
runNettestClient env scenario = displayUncaughtException do
  disableAlphanetWarning
  scenario $ nettestImplClient env

-- | Implementation that works with real network and uses `tezos-node`
-- RPC and `tezos-client`.
nettestImplClient :: MorleyClientEnv -> NettestImpl IO
nettestImplClient env =
  NettestImpl
    { niOriginateUntyped = \UntypedOriginateData {..} -> do
        originatorAlias <- niGetAlias uodFrom
        let
          originationScenario =
            Client.originateUntypedContract True uodName uodFrom uodBalance uodContract uodStorage
        -- Note that tezos key reveal operation cost an additional fee
        -- so that's why we reveal keys in origination and transaction
        -- rather than doing it before scenario execution
        revealKeyUnlessRevealed env originatorAlias
        (_, res) <- runMorleyClientM env originationScenario
        niComment $
          "Originated smart contract " <> uodName <>
          " with address " <> pretty res
        pure res

    , niTransfer = \TransferData {..} -> do
        sender' <- niResolveAddress tdFrom
        senderAlias <- niGetAlias tdFrom
        receiver <- niResolveAddress tdTo
        let
          transferScenario =
            Client.lTransfer sender' receiver tdAmount tdEntrypoint tdParameter
        revealKeyUnlessRevealed env senderAlias
        runMorleyClientM env transferScenario
        pure ()

    , niSignBytes = \hash signer -> runMorleyClientM env $
        TezosClient.signBytes (AddressAlias signer) hash

    , niGenKey =
        runMorleyClientM env . TezosClient.genKey

    , niGenFreshKey =
        runMorleyClientM env . TezosClient.genFreshKey

    , niExpectFailure = withExpectedFailure
    , niGetBalance = niGetBalanceHelper
    , niCheckBalance = \addrOrAlias expected -> do
        balance <- niGetBalanceHelper addrOrAlias
        when (balance /= expected) $
          throwM $
            UnexpectedBalance addrOrAlias (#expected .! expected) (#actual .! balance)
    , niCheckStorage = \addrOrAlias expected -> do
        actual <- niGetStorage addrOrAlias
        when (actual /= expected) $
          throwM $
            UnexpectedStorage addrOrAlias (#expected .! SomeValue expected) (#actual .! SomeValue actual)
    , ..
    }
  where
    niGetStorage :: forall t. UnpackedValScope t => AddressOrAlias -> IO (T.Value t)
    niGetStorage addrOrAlias = do
      addr <- niResolveAddress addrOrAlias
      expr <- runMorleyClientM env $ Client.getContractStorage addr
      either throwM pure (fromExpression @(T.Value t) expr)

    niComment msg = putTextLn msg >> hFlush stdout
    niResolveAddress = runMorleyClientM env . TezosClient.resolveAddress
    niGetAlias = runMorleyClientM env . TezosClient.getAlias
    niGetPublicKey = runMorleyClientM env . TezosClient.getPublicKey
    niGetBalanceHelper addrOrAlias = do
      addr <- niResolveAddress addrOrAlias
      runMorleyClientM env $ Client.getBalance addr

----------------------------------------------------------------------------
-- Helpers
----------------------------------------------------------------------------

revealKeyUnlessRevealed :: MorleyClientEnv -> Alias -> IO ()
revealKeyUnlessRevealed env alias = runMorleyClientM env $ do
  catch (TezosClient.revealKey alias) $ \case
    (Client.AlreadyRevealed _) ->
      logDebug $ "<nettest> " <> alias <> " alias has already revealed key"
    err -> throwM err

----------------------------------------------------------------------------
-- Validation
----------------------------------------------------------------------------

-- | Signals an assertion failure during the execution of a 'NettestScenario' action.
data TestError
  = UnexpectedBalance AddressOrAlias ("expected" :! Mutez) ("actual" :! Mutez)
  | UnexpectedStorage AddressOrAlias ("expected" :! SomeValue) ("actual" :! SomeValue)
  | UnexpectedAddress ("expected" :! Address) ("actual" :! Address)
  | UnexpectedFailWithValue ("expected" :! SomeValue) ("actual" :! SomeValue)
  | ParseExpressionError SomeTypeRep Expression FromExpressionError
  | UnexpectedClientSuccess
  deriving stock Show

instance Exception TestError where
  displayException = pretty

instance Buildable TestError where
  build = \case
    UnexpectedBalance addrOrAlias (arg #expected -> expected) (arg #actual -> actual) ->
      "Account " +| addrOrAlias |+ " is expected to have balance " +| expected |+
      ". But its actual balance is " +| actual |+ ""
    UnexpectedStorage addrOrAlias (arg #expected -> expected) (arg #actual -> actual) ->
      "Account " +| addrOrAlias |+ " is expected to have storage " +| formatSomeValue expected |+
      ". But its actual storage is " +| formatSomeValue actual |+ "."
    UnexpectedAddress (arg #expected -> expected) (arg #actual -> actual) ->
      "Expected address: " +| expected |+ ", but found: " +| actual |+ ""
    UnexpectedFailWithValue (arg #expected -> expected) (arg #actual -> actual) ->
      "Expected interpretation to fail with value: " +| formatSomeValue expected |+
      ", but found: " +| formatSomeValue actual |+ ""
    ParseExpressionError (SomeTypeRep expectedType) expr err ->
      "Failed to parse expression: " +| expr |+ " to value of type: " +| show @String expectedType |+
      " due to: " +| err |+ ""
    UnexpectedClientSuccess ->
      "Nettest failed because tezos-client unexpectedly exited with 0 error code"

-- | Checks that a failure occurs (if one is expected) and that it matches the
-- expected one.
withExpectedFailure :: IO a -> NettestFailure -> IO ()
withExpectedFailure sc ntFailure =
  catch (sc >> throwM UnexpectedClientSuccess) (checkFailure ntFailure)
  where
    checkFailure :: NettestFailure -> ClientRpcError -> IO ()
    checkFailure expectedErr actualErr =
      case expectedErr of
        NettestFailedWith expectedAddr expectedVal
          | ContractFailed actualAddr actualExpr <- actualErr -> do
              checkAddr expectedAddr actualAddr
              checkExpression' expectedVal actualExpr
          | otherwise -> throwM actualErr

        NettestFailedWithError expectedErr'
          | ContractFailed _ actualExpr <- actualErr -> do
              errorToVal expectedErr' $ \expectedVal ->
                checkExpression expectedVal actualExpr
          | otherwise -> throwM actualErr

        NettestFailedWithNumericError errorTagMap expectedErr'
          | ContractFailed _ actualExpr <- actualErr -> do
              errorToValNumeric errorTagMap expectedErr' $ \expectedVal ->
                checkExpression expectedVal actualExpr
          | otherwise -> throwM actualErr

        NettestShiftOverflow expectedAddr
          | ShiftOverflow actualAddr <- actualErr -> checkAddr expectedAddr actualAddr
          | otherwise -> throwM actualErr

        NettestEmptyTransaction expectedAddr
          | EmptyTransaction actualAddr <- actualErr -> checkAddr expectedAddr actualAddr
          | otherwise -> throwM actualErr

        NettestBadParameter
          | BadParameter _ _ <- actualErr -> pass
          | otherwise -> throwM actualErr

    -- | Asserts that two addresses are equal.
    checkAddr :: ToAddress addr => addr -> Address -> IO ()
    checkAddr (toAddress -> expectedAddr) actualAddr
      | expectedAddr /= actualAddr =
          throwM $ UnexpectedAddress (#expected .! expectedAddr) (#actual .! actualAddr)
      | otherwise = pass

    -- | Tries to parse a Micheline expression to a value of type @t@ and,
    -- if successful, asserts that it is equal to the given expected value.
    checkExpression :: forall t. (UnpackedValScope t, Typeable t) => T.Value t -> Expression -> IO ()
    checkExpression expectedVal actualExpr =
      case fromExpression @(T.Value t) actualExpr of
        Left err -> throwM $ ParseExpressionError (someTypeRep $ Proxy @(T.Value t)) actualExpr err
        Right actualVal
          | expectedVal /= actualVal ->
              throwM $ UnexpectedFailWithValue
                (#expected .! SomeValue expectedVal)
                (#actual .! SomeValue actualVal)
          | otherwise -> pass

    checkExpression' :: forall t. NiceUnpackedValue t => t -> Expression -> IO ()
    checkExpression' expectedVal actualExpr =
      -- Note: we use `niceUnpackedValueEvi` here to build an `UnpackedValScope` dict
      -- (necessary for `fromExpression`) from a `NiceUnpackedValue` dict.
      checkExpression @(ToT t) (toVal expectedVal) actualExpr \\ niceUnpackedValueEvi @t
