-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Abstract nettest interface not bound to a particular
-- implementation.
--
-- The interface may look a bit untyped and unsafe in some places.
-- For example, in order to call a contract one should supply a
-- simple address rather than a contract ref, so it is easy to pass
-- a value of wrong type. Also it is easy to call a non-existing entrypoint.
--
-- Subjectively, it makes writing test scenarios easier because you
-- have to prove less to the compiler. It also makes implementation of
-- nettest engine a bit easier. Of course, it also makes it easier
-- to make certain mistakes. However, we expect users of this interface
-- to also use the functionality of the 'Morley.Nettest.Pure' module
-- and convert nettest scenarios to purely testable scenarios for
-- integrational testing engine. In this case errors should be detected
-- almost as quickly as they would reported by the compiler, at least
-- before trying to run scenario on a live network.
--
-- Also this interface uses 'Address' rather than 'EpAddress'.
-- I (\@gromak) concluded that 'EpAddress' can not be passed to @tezos-client@.
-- For key addresses it just does not make sense and for contract addresses
-- I get such errors:
--
-- @
--   bad contract notation
--   Invalid contract notation "KT1VrFpBPwBTm3hsK7DB7SPmY8fTHJ3vY6sJ%mint"
-- @

module Morley.Nettest.Abstract
  ( AddressOrAlias (..)
  , OriginateData (..)
  , TransferData (..)
  , NettestImpl (..)
  , NettestScenario
  , UntypedOriginateData (..)

  -- * Constant address
  , nettestAddress
  , nettestAddressAlias

  -- * Actions
  , resolveAddressAction
  , getAliasAction
  , resolveNettestAddressAction
  , newAddressAction
  , newFreshAddressAction
  , signBytesAction
  , originateAction
  , originateUntypedAction
  , originateUntypedSimpleAction
  , originateSimpleAction
  , transferAction
  , callFromAction
  , commentAction
  , expectFailureAction
  , getBalanceAction
  , checkBalanceAction
  , getStorageAction
  , checkStorageAction
  , getPublicKeyAction

  -- * Validation
  , NettestFailure (..)

  -- * Helpers
  , ep
  ) where

import Data.Constraint ((\\))

import Lorentz
  (ErrorTagMap, HasEntrypointArg, IsError, TAddress(..), ToAddress, ToTAddress(toTAddress),
  useHasEntrypointArg)
import Lorentz.Constraints
import Lorentz.Run (Contract, compileLorentzContract)
import Michelson.Typed (Dict(Dict), IsoValue(..), StorageScope, UnpackedValScope)
import qualified Michelson.Typed as T
import Michelson.Typed.Convert
import Michelson.Typed.Entrypoints
import qualified Michelson.Untyped as U
import Morley.Client
import Tezos.Address
import Tezos.Core (Mutez, unsafeMkMutez)
import qualified Tezos.Crypto as Crypto

data OriginateData param =
  forall st.
  (NiceParameterFull param, NiceStorage st) => OriginateData
  { odFrom :: AddressOrAlias
  -- ^ The address from which contract will be originated (must have some XTZ).
  , odName :: Alias
  -- ^ Alias for the originated contract.
  , odBalance :: Mutez
  -- ^ Initial balance.
  , odStorage :: st
  -- ^ Initial storage.
  , odContract :: Contract param st
  -- ^ The contract itself.
  --
  -- We are using Lorentz version here which is convenient. However, keep in
  -- mind that if someone wants to test a contract from @.tz@ file, they should use
  -- 'UntypedOriginateData'.
  }

-- | Untyped version of OriginateData. It can be used for interaction with raw
-- Michelson contracts
data UntypedOriginateData = UntypedOriginateData
  { uodFrom :: AddressOrAlias
  -- ^ The address from which contract will be originated (must have some XTZ).
  , uodName :: Alias
  -- ^ Alias for the originated contract.
  , uodBalance :: Mutez
  -- ^ Initial balance.
  , uodStorage :: U.Value
  -- ^ Initial storage.
  , uodContract :: U.Contract
  -- ^ The contract itself.
  }

data TransferData =
  forall v. NiceParameter v => TransferData
  { tdFrom :: AddressOrAlias
  -- ^ Sender address for this transaction.
  , tdTo :: AddressOrAlias
  -- ^ Receiver address for this transaction.
  , tdAmount :: Mutez
  -- ^ Amount to be transferred.
  , tdEntrypoint :: EpName
  -- ^ An entrypoint to be called. Consider using 'ep' in testing
  -- scenarios.
  , tdParameter :: v
  -- ^ Parameter that will be used for a contract call. Set to @()@
  -- for transfers to key addresses.
  }

-- | A record data type with all base methods one can use during nettest.
data NettestImpl m = NettestImpl
  { niResolveAddress :: AddressOrAlias -> m Address
  -- ^ Unwrap an 'Address' or get an 'Address' corresponding to an alias.
  , niGetAlias :: AddressOrAlias -> m Alias
  -- ^ Get an alias for the address provided to it.
  , niGenKey :: Alias -> m Address
  -- ^ Generate a secret key and store it with given alias.
  -- If a key with this alias already exists, the corresponding address
  -- will be returned and no state will be changed.
  , niGenFreshKey :: Alias -> m Address
  -- ^ Generate a secret key and store it with given alias.
  -- Unlike 'niGenKey' this function overwrites the existing key when
  -- given alias is already stored.
  , niSignBytes :: ByteString -> Alias -> m Crypto.Signature
  -- ^ Get signature for preapplied operation.
  , niOriginateUntyped :: UntypedOriginateData -> m Address
  -- ^ Originate a new raw Michelson contract with given data.
  , niTransfer :: TransferData -> m ()
  -- ^ Send a transaction with given data.
  , niComment :: Text -> m ()
  -- ^ Print given string verbatim as a comment.
  , niExpectFailure :: forall a. m a -> NettestFailure -> m ()
  -- ^ Expect a failure while running another action.
  , niGetBalance :: AddressOrAlias -> m Mutez
  -- ^ Get balance for given address or alias.
  , niCheckBalance :: AddressOrAlias -> Mutez -> m ()
  -- ^ Get balance for given address or alias and check whether it's
  -- equal to given one.
  , niGetStorage
      :: forall t. (UnpackedValScope t, Typeable t)
      => AddressOrAlias -> m (T.Value t)
  -- ^ Get storage for given address or alias.
  -- Note: the 'UnpackedValScope' constraint is used here because some implementations
  -- need it to parse a Micheline 'Expression's to 'T.Value'.
  --
  -- The 'UnpackedValScope' constraint also forbids storages with @big_map@s from being
  -- retrieved (due to a limitation in the tezos RPC).
  -- If a contract's storage contains @big_map@s, usage of that contract's getters is preferred.
  , niCheckStorage
      :: forall t. (UnpackedValScope t, StorageScope t)
      => AddressOrAlias -> T.Value t -> m ()
  -- ^ Get storage for given address or alias and check whether it's equal to given one.
  -- Note: the 'UnpackedValScope' constraint is used here because some implementations
  -- need it to parse a Micheline 'Expression's to 'T.Value', while others need
  -- 'StorageScope' to convert the given 'T.Value' to 'U.Value'.
  --
  -- The 'UnpackedValScope' constraint also forbids storages with @big_map@s from being
  -- retrieved (due to a limitation in the tezos RPC).
  -- If a contract's storage contains @big_map@s, usage of that contract's getters is preferred.
  , niGetPublicKey :: AddressOrAlias -> m Crypto.PublicKey
  -- ^ Get public key assosiated with given address or alias.
  -- Fail if given address or alias is not an implicit account.
  }

-- | Any monadic computation that can only access nettest methods.
type NettestScenario = forall m. Monad m => NettestImpl m -> m ()

----------------------------------------------------------------------------
-- Specific actions
--
-- We call them actions because they take 'NettestImpl' as an explicit argument
-- and do not have extra constraints. It makes it easier to adapt them for
-- any reasonable interface.
-- Adapted to @caps@ versions are provided in another module.
----------------------------------------------------------------------------

-- | This address will be used as source by default. Some
-- functionality in this module assumes that this address has
-- /sufficient/ amount of XTZ in advance (required amount depends on scenario).
nettestAddress :: AddressOrAlias
nettestAddress = AddressAlias nettestAddressAlias

-- | Alias for nettest used in 'nettestAddress'.
nettestAddressAlias :: Alias
nettestAddressAlias = "nettest"

-- | Alias for 'niResolveAddress'.
resolveAddressAction :: NettestImpl m -> AddressOrAlias -> m Address
resolveAddressAction = niResolveAddress

-- | Alias for `niGetAlias`
getAliasAction :: NettestImpl m -> AddressOrAlias -> m Alias
getAliasAction = niGetAlias

-- | Get 'Address' corresponding to 'nettestAddress'.
resolveNettestAddressAction :: NettestImpl m -> m Address
resolveNettestAddressAction = flip niResolveAddress nettestAddress

-- | Generate a new secret key and record it with given alias. Also
-- transfer small amount of XTZ to it from 'nettestAddress'. Does not
-- override an existing address.
--
-- Note, that when we generate a new address we must be aware of
-- prefix appended to it, so that when you create address alias @foo@,
-- you cannot call it directly by `AddressAlias "foo"`, but with the
-- corresponding prefix `AddressAlias "prefix.foo"`.
newAddressAction :: Monad m => NettestImpl m -> Alias -> m Address
newAddressAction impl name = do
  addr <- niGenKey impl name
  niTransfer impl TransferData
    { tdFrom = nettestAddress
    , tdTo = AddressResolved addr
    , tdAmount = unsafeMkMutez $ 900 * 1000 -- 0.9 XTZ
    , tdEntrypoint = DefEpName
    , tdParameter = ()
    }
  pure addr

-- | Generate a new secret key and record it with given alias. If the
-- alias is already known, the key will be overwritten. The address is
-- guaranteed to be fresh, i. e. no operations on it have been made.
newFreshAddressAction :: NettestImpl m -> Alias -> m Address
newFreshAddressAction = niGenFreshKey

-- | Alias for 'niSignBytes'
signBytesAction :: NettestImpl m -> ByteString -> Alias -> m Crypto.Signature
signBytesAction = niSignBytes

-- | Alias for 'niOriginateUntyped'
originateUntypedAction :: NettestImpl m -> UntypedOriginateData -> m Address
originateUntypedAction = niOriginateUntyped

-- | A simplified version of the originateUntyped command. Uses default
-- address ('nettestAddress'). The contract will have 0 balance.
originateUntypedSimpleAction
  :: NettestImpl m -> Text -> U.Value -> U.Contract -> m Address
originateUntypedSimpleAction impl name storage contract =
  originateUntypedAction impl $
  UntypedOriginateData
    { uodFrom = nettestAddress
    , uodName = name
    , uodBalance = unsafeMkMutez 0
    , uodStorage = storage
    , uodContract = contract
    }

-- | Lorentz version for origination.
originateAction :: Functor m => NettestImpl m -> OriginateData param -> m (TAddress param)
originateAction impl OriginateData{..} =
  fmap TAddress . originateUntypedAction impl $
  UntypedOriginateData
  { uodFrom = odFrom
  , uodName = odName
  , uodBalance = odBalance
  , uodStorage = untypeHelper odStorage
  , uodContract = convertContract $ compileLorentzContract odContract
  }
  where
    untypeHelper :: forall st. NiceStorage st => st -> U.Value
    untypeHelper = untypeValue . toVal \\ niceStorageEvi @st

-- | A simplified version of the originate command. Uses default
-- address ('nettestAddress'). The contract will have 0 balance.
originateSimpleAction ::
     (NiceParameterFull param, NiceStorage st, Functor m)
  => NettestImpl m
  -> Text
  -> st
  -> Contract param st
  -> m (TAddress param)
originateSimpleAction impl name storage contract =
  originateAction impl $
  OriginateData
    { odFrom = nettestAddress
    , odName = name
    , odBalance = unsafeMkMutez 0
    , odStorage = storage
    , odContract = contract
    }

-- | Alias for 'niTransfer'.
transferAction :: NettestImpl m -> TransferData -> m ()
transferAction = niTransfer

-- | Call a certain entrypoint of a contract referred to by the second
-- typed address from the first address.
callFromAction ::
     forall v addr m epRef epArg.
     (ToTAddress v addr, HasEntrypointArg v epRef epArg, IsoValue epArg, Typeable epArg)
  => NettestImpl m
  -> AddressOrAlias
  -> addr
  -> epRef
  -> epArg
  -> m ()
callFromAction impl from (toTAddress @v @addr -> TAddress to) epRef param =
  case useHasEntrypointArg @v @epRef @epArg epRef of
    (Dict, epName) ->
      transferAction impl $
      TransferData
        { tdFrom = from
        , tdTo = AddressResolved to
        , tdAmount = unsafeMkMutez 0
        , tdEntrypoint = epName
        , tdParameter = param
        }

-- | Alias for 'niComment'.
commentAction :: NettestImpl m -> Text -> m ()
commentAction = niComment

-- | Alias for 'niExpectFailure'.
expectFailureAction :: NettestImpl m -> m a -> NettestFailure -> m ()
expectFailureAction = niExpectFailure

-- | Alias for 'niGetBalance'.
getBalanceAction :: NettestImpl m -> AddressOrAlias -> m Mutez
getBalanceAction = niGetBalance

-- | Alias for 'niCheckBalance'.
checkBalanceAction :: NettestImpl m -> AddressOrAlias -> Mutez -> m ()
checkBalanceAction = niCheckBalance

-- | Alias for 'niGetStorage'.
getStorageAction :: (UnpackedValScope t, Typeable t) => NettestImpl m -> AddressOrAlias -> m (T.Value t)
getStorageAction = niGetStorage

-- | Alias for 'niCheckStorage'.
checkStorageAction
  :: (UnpackedValScope t, StorageScope t)
  => NettestImpl m
  -> AddressOrAlias
  -> T.Value t
  -> m ()
checkStorageAction = niCheckStorage

-- | Alias for 'niGetPublicKey'.
getPublicKeyAction :: NettestImpl m -> AddressOrAlias -> m Crypto.PublicKey
getPublicKeyAction = niGetPublicKey

----------------------------------------------------------------------------
-- Validation
----------------------------------------------------------------------------

-- | Failures that could be expected in the execution of a 'NettestScenario' action.
-- These can be caught and handled with 'Morley.Nettest.expectFailure'.
data NettestFailure
  = forall t addr. (ToAddress addr, NiceUnpackedValue t) => NettestFailedWith addr t
  -- ^ Expect that interpretation of contract with the given address ended
  -- with [FAILED].
  | forall err. (IsError err, Eq err) => NettestFailedWithError err
  -- ^ Expect contract to fail with the given error, e.g. @NettestFailedWithError (VoidResult False)@.
  | forall err. (IsError err, Eq err) => NettestFailedWithNumericError ErrorTagMap err
  -- ^ Expect contract to fail with the given error (when using numeric representation of errors).
  | forall addr. ToAddress addr => NettestShiftOverflow addr
  -- ^ Expect that interpretatino of contract with the given address ended
  -- with a shift overflow error.
  | forall addr. ToAddress addr => NettestEmptyTransaction addr
  -- ^ Expect failure due to an attempt to transfer 0tz towards a simple address.
  | NettestBadParameter
  -- ^ Expect failure due to an attempt to call a contract with an invalid parameter.

  -- TODO: [#284] add more errors here!
  --  | NettestMutezArithError ArithErrorType
  --  | NettestGasExhaustion

----------------------------------------------------------------------------
-- Other helpers
----------------------------------------------------------------------------

-- | A short partial constructor for 'EpName'. It is supposed to be
-- applied to string constants, so programmer is responsible for
-- validity. And this code is for tests anyway, so each failure is a
-- programmer mistake.
--
-- It is intentionally here and not in some deeper module because the
-- name is really short and more suitable for writing scenarios.
ep :: HasCallStack => Text -> EpName
ep = U.unsafeBuildEpName
