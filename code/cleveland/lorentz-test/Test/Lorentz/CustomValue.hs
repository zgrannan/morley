-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Checks that the strategies for 'Util.CustomGeneric' work as expected to
-- produce 'IsoValue' with a custom shape.

{-# OPTIONS_GHC -Wno-partial-fields #-}

module Test.Lorentz.CustomValue
  ( test_Custom_Values
  ) where

import Data.Typeable ((:~:)(..))
import Test.Tasty (TestTree)

import Lorentz.Value
import Michelson.Typed.T

----------------------------------------------------------------------------
-- Example data types
----------------------------------------------------------------------------

data CustomType a
  = CustomUp Integer Integer
  | CustomMid {unMid :: Natural}
  | CustomDown a
  | CustomNone

$(customGeneric "CustomType" $ withDepths
    [ cstr @3 [fld @1, fld @1]
    , cstr @3 [fld @0]
    , cstr @2 [fld @0]
    , cstr @1 []
    ]
  )

deriving anyclass instance IsoValue a => IsoValue (CustomType a)

data KindaRightType
  = KindaRightA
  | KindaRightB Mutez Integer
  | KindaRightC Mutez Natural

$(customGeneric "KindaRightType" rightBalanced)

deriving anyclass instance IsoValue KindaRightType

data KindaLeftType
  = KindaLeftA Integer Integer Integer
  | KindaLeftB Natural Mutez Natural

$(customGeneric "KindaLeftType" leftBalanced)

deriving anyclass instance IsoValue KindaLeftType

data VeryRightType
  = VeryRightA
  | VeryRightB
  | VeryRightC
  | VeryRightD
  | VeryRightE
  | VeryRightF

$(customGeneric "VeryRightType" rightComb)

deriving anyclass instance IsoValue VeryRightType

data VeryLeftType = VeryLeft
  { unA :: Integer
  , unB :: Natural
  , unC :: Mutez
  , unD :: Natural
  , unE :: Integer
  }

$(customGeneric "VeryLeftType" leftComb)

deriving anyclass instance IsoValue VeryLeftType

----------------------------------------------------------------------------
-- Expected resulting IsoValue
----------------------------------------------------------------------------

type ExpectedCustomValue a =
  'TOr ('TOr ('TOr ('TPair 'TInt 'TInt) 'TNat) (ToT a)) 'TUnit

type ExpectedKindaRightValue =
  'TOr 'TUnit ('TOr ('TPair 'TMutez 'TInt) ('TPair 'TMutez 'TNat))

type ExpectedKindaLeftValue =
  'TOr ('TPair ('TPair 'TInt 'TInt) 'TInt) ('TPair ('TPair 'TNat 'TMutez) 'TNat)

type ExpectedVeryRightValue =
  'TOr 'TUnit ('TOr 'TUnit ('TOr 'TUnit ('TOr 'TUnit ('TOr 'TUnit 'TUnit))))

type ExpectedVeryLeftValue =
  'TPair ('TPair ('TPair ('TPair 'TInt 'TNat) 'TMutez) 'TNat) 'TInt

----------------------------------------------------------------------------
-- Type equality checking
----------------------------------------------------------------------------

-- Fake tests to deceive "weeder".
-- We only do typechecking in this module.
test_Custom_Values :: [TestTree]
test_Custom_Values = []
  where
    _nonSense = (unMid, unA, unB, unC, unD, unE)

_checkWithDepths :: ToT (CustomType a) :~: (ExpectedCustomValue a)
_checkWithDepths = Refl

_checkRightBalanced :: ToT KindaRightType :~: ExpectedKindaRightValue
_checkRightBalanced = Refl

_checkLeftBalanced :: ToT KindaLeftType :~: ExpectedKindaLeftValue
_checkLeftBalanced = Refl

_checkRightComb :: ToT VeryRightType :~: ExpectedVeryRightValue
_checkRightComb = Refl

_checkLeftComb :: ToT VeryLeftType :~: ExpectedVeryLeftValue
_checkLeftComb = Refl

