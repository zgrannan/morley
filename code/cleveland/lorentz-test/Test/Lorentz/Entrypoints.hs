-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for Lorentz compilation which uses 'LorentzCompilationWay'.
module Test.Lorentz.Entrypoints
  ( test_FieldEntrypoints
  , test_TypeEntrypoints
  , test_RootEntrypoints
  , test_Entrypoints_lookup
  , test_Contract_call
  , test_Self_call
  ) where

import Fcf (Eval)
import Test.HUnit ((@?=))
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit (testCase)
import Test.Tasty.TypeSpec (typeTest)
import Test.TypeSpec (Is, TypeSpec(..))
import Test.Util.TypeSpec (ExactlyIs)
import Test.Util.Annotation

import Lorentz ((:!), ( # ), (/->))
import qualified Lorentz as L
import Lorentz.Annotation
import Lorentz.Constraints
import Lorentz.Entrypoints
import Lorentz.Run
import Lorentz.Test
import Lorentz.Value
import Michelson.Typed hiding (Contract)
import qualified Michelson.Typed as M (Contract(..))
import Michelson.Untyped (RootAnn, ann, noAnn)

----------------------------------------------------------------------------
-- Entrypoints declarations
----------------------------------------------------------------------------

data MyEntrypoints1
  = Do1 Integer
  | Do2 (Integer, Integer)
  | Do3 MyEntrypoints2
  | Do4 MyParams
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints1a
  = Do1a Integer
  | Do2a (Integer, Integer)
  | Do3a MyEntrypoints2
  | Do4a MyParams
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints2
  = Do10
  | Do11 Natural
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints3
  = Do12 ("tuplearg" :! ("TL" :! Integer, "TR" :!  Natural), "boolarg" :! Bool)
  | Do13 ("integerarg" :! Integer, "boolarg" :! Bool)
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints4
  = Do14 ("viewarg1" :! L.View ("owner" :! L.Address) Natural)
  | Do15 ()
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints5
  = Do16 ("maybearg" :! Maybe ("maybeinner" :! Natural))
  | Do17 ()
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints6
  = Do18 ("lambdaarg" :! L.Lambda Natural Natural)
  | Do19 ()
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints7
  = Do20 ("listarg" :! [("balance" :! Natural , "address" :! L.Address)])
  | Do21 ()
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints8
  = Do22 ("maparg" :! (Map Natural ("balance" :! Natural , "address" :! L.Address)))
  | Do23 ()
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints9
  = Do24 ("maybearg" L.:? ("maybeinner" :! Natural))
  | Do25 ()
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints10
  = Do26 ("bigmaparg" :! L.Lambda (BigMap Natural ("balance" :! Natural , "address" :! L.Address)) ())
  | Do27 ()
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypoints11
  = Do28 ("kek" :! Natural, "pek" :! Integer)
  | Do29
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypointsWithDef
  = Default Integer
  | NonDefault Natural
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypointsWithRoot
  = Dor1
  | Dor2 Natural
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyParams = MyParams
  { param1 :: ()
  , param2 :: ByteString
  }
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

-- Normally this cannot declare entrypoints because this is not a sum type.
-- But we will declare them forcibly
data MySingleEntrypoint = Dos1 Integer
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypointsDelegated
  = Dod1
  | Dod2 MyEntrypointsSubDelegated
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

data MyEntrypointsSubDelegated
  = Dosd1
  | Dosd2
  deriving stock Generic
  deriving anyclass (IsoValue, HasAnnotation)

instance ParameterHasEntrypoints MyEntrypoints1 where
  type ParameterEntrypointsDerivation MyEntrypoints1 = EpdRecursive

instance ParameterHasEntrypoints MyEntrypoints1a where
  type ParameterEntrypointsDerivation MyEntrypoints1a = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints2 where
  type ParameterEntrypointsDerivation MyEntrypoints2 = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints3 where
  type ParameterEntrypointsDerivation MyEntrypoints3 = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints4 where
  type ParameterEntrypointsDerivation MyEntrypoints4 = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints5 where
  type ParameterEntrypointsDerivation MyEntrypoints5 = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints6 where
  type ParameterEntrypointsDerivation MyEntrypoints6 = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints7 where
  type ParameterEntrypointsDerivation MyEntrypoints7 = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints8 where
  type ParameterEntrypointsDerivation MyEntrypoints8 = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints9 where
  type ParameterEntrypointsDerivation MyEntrypoints9 = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints10 where
  type ParameterEntrypointsDerivation MyEntrypoints10 = EpdPlain

instance ParameterHasEntrypoints MyEntrypoints11 where
  type ParameterEntrypointsDerivation MyEntrypoints11 = EpdNone

instance ParameterHasEntrypoints MyEntrypointsWithDef where
  type ParameterEntrypointsDerivation MyEntrypointsWithDef = EpdPlain

instance ParameterHasEntrypoints (ShouldHaveEntrypoints MySingleEntrypoint) where
  type ParameterEntrypointsDerivation (ShouldHaveEntrypoints MySingleEntrypoint) = EpdPlain

instance ParameterHasEntrypoints MyEntrypointsDelegated where
  type ParameterEntrypointsDerivation MyEntrypointsDelegated = EpdDelegate

instance ParameterHasEntrypoints MyEntrypointsSubDelegated where
  type ParameterEntrypointsDerivation MyEntrypointsSubDelegated = EpdNone

instance ParameterHasEntrypoints MyEntrypointsWithRoot where
  type ParameterEntrypointsDerivation MyEntrypointsWithRoot = EpdWithRoot "root" EpdPlain

dummyContract :: Contract param ()
dummyContract = defaultContract $
  L.drop L.# L.unit L.# L.nil L.# L.pair

test_FieldEntrypoints :: [TestTree]
test_FieldEntrypoints =
  [ testCase "Simple parameter" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints2))
      @?=
      FANodeOr
        (ann "do10") FALeaf
        (ann "do11") FALeaf

  , testGroup "Complex parameter"
    [ testCase "Interpreting as direct list of entrypoints" $
        (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints1a))
        @?=
        FANodeOr
          noAnn (FANodeOr (ann "do1a") FALeaf (ann "do2a") FALeaf)
          noAnn
            (FANodeOr
              (ann "do3a")
                (FANodeOr noAnn FALeaf noAnn FALeaf)
              (ann "do4a")
                (FANodePair (ann "param1") FALeaf (ann "param2") FALeaf))

    , testCase "Recursive entrypoints traversal" $
        (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints1))
        @?=
        FANodeOr
          noAnn (FANodeOr (ann "do1") FALeaf (ann "do2") FALeaf)
          noAnn (FANodeOr
                  noAnn
                    (FANodeOr (ann "do10") FALeaf (ann "do11") FALeaf)
                  (ann "do4")
                    (FANodePair
                      (ann "param1") FALeaf
                      (ann "param2") FALeaf)
                )

    , testCase "Delegating entrypoints traversal" $
        (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypointsDelegated))
        @?=
        FANodeOr (ann "dod1") FALeaf
             (ann "dod2") (FANodeOr noAnn FALeaf noAnn FALeaf)
    ]
  ]
  where
    paramAnnTree :: M.Contract cp st -> FieldAnnTree cp
    paramAnnTree = extractFieldAnnTree . pnNotes . cParamNotes

test_TypeEntrypoints :: [TestTree]
test_TypeEntrypoints =
  [ testCase "Named field parameter" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints3))
      @?=
      (TANodeOr noAnn
       (TANodePair noAnn (TANodePair (ann "tuplearg") (TALeaf (ann "TL")) (TALeaf (ann "TR"))) (TALeaf (ann "boolarg")))
       (TANodePair noAnn (TALeaf (ann "integerarg")) (TALeaf (ann "boolarg")))
      )
  , testCase "Named field parameter for views" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints4))
      @?=
      (TANodeOr noAnn
        (TANodePair (ann "viewarg1") (TALeaf (ann "owner")) (TALeaf noAnn)) (TALeaf noAnn))

  , testCase "Maybe field parameter" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints5))
      @?=
      (TANodeOr noAnn
        (TANodeOption (ann "maybearg") (TALeaf (ann "maybeinner"))) (TALeaf noAnn))

  , testCase "Lambda field parameter" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints6))
      @?=
      (TANodeOr noAnn
        (TANodeLambda (ann "lambdaarg") (TALeaf noAnn) (TALeaf noAnn)) (TALeaf noAnn))

  , testCase "List field parameter" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints7))
      @?=
      (TANodeOr noAnn
       (TANodeList (ann "listarg") (TANodePair noAnn (TALeaf (ann "balance")) (TALeaf (ann "address")))) (TALeaf noAnn))

  , testCase "Map field parameter" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints8))
      @?=
      (TANodeOr noAnn
        (TANodeMap
          (ann "maparg")
          (TALeaf noAnn)
          (TANodePair noAnn (TALeaf (ann "balance")) (TALeaf (ann "address")))
        )
        (TALeaf noAnn)
      )

  , testCase "Maybe field parameter 2" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints9))
      @?=
      (TANodeOr noAnn
        (TANodeOption (ann "maybearg") (TALeaf (ann "maybeinner"))) (TALeaf noAnn))

  , testCase "Big map field parameter" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints10))
      @?=
      (TANodeOr noAnn
        (TANodeLambda (ann "bigmaparg")
          (TANodeBigMap
            noAnn
            (TALeaf noAnn)
            (TANodePair noAnn (TALeaf (ann "balance")) (TALeaf (ann "address")))
          )
          (TALeaf noAnn)
        )
        (TALeaf noAnn)
      )

  , testCase "Newtype" $
      (paramAnnTree $ compileLorentzContract (dummyContract @(ShouldHaveEntrypoints MySingleEntrypoint)))
      @?=
      TALeaf noAnn

  , testGroup "Primitive type parameter"
      -- Parameters used in these test cases should not require any instances
      [ testCase "Address" $
          (paramAnnTree $ compileLorentzContract (dummyContract @Address))
          @?=
          TALeaf noAnn
      , testCase "Void" $
          (paramAnnTree $ compileLorentzContract (dummyContract @(L.Void_ Integer Natural)))
          @?=
          TANodePair noAnn (TALeaf noAnn) (TANodeLambda noAnn (TALeaf noAnn) (TALeaf noAnn))
      ]
  , testCase "EpdNone case (type annotations are preserved)" $
      (paramAnnTree $ compileLorentzContract (dummyContract @MyEntrypoints11))
      @?=
      (TANodeOr noAnn
        (TANodePair noAnn (TALeaf (ann "kek")) (TALeaf (ann "pek")))
        (TALeaf noAnn)
      )
  ]
  where
    paramAnnTree :: M.Contract cp st -> TypeAnnTree cp
    paramAnnTree = extractTypeAnnTree . pnNotes . cParamNotes

test_RootEntrypoints :: [TestTree]
test_RootEntrypoints =
  [ testCase "No root entrypoint" $
      (rootAnn $ compileLorentzContract (dummyContract @MyEntrypoints1))
      @?=
      noAnn

  , testCase "With root entrypoint" $
      (rootAnn $ compileLorentzContract (dummyContract @MyEntrypointsWithRoot))
      @?=
      ann "root"
  ]
  where
    rootAnn :: M.Contract cp st -> RootAnn
    rootAnn = pnRootAnn . cParamNotes

----------------------------------------------------------------------------
-- @contract@ instruction
----------------------------------------------------------------------------

test_Entrypoints_lookup :: [TestTree]
test_Entrypoints_lookup =
  [ testGroup "Flat parameter type"
    [ typeTest "Default entrypoint arg" $ Valid @
        (GetDefaultEntrypointArg MyEntrypoints1a `Is` MyEntrypoints1a)
    , typeTest "Can get entrypoint on surface" $ Valid @
        (GetEntrypointArg MyEntrypoints1a "Do1a" `Is` Integer)
    , typeTest "Cannot get entrypoint in deep" $ Valid @
        (Eval (LookupParameterEntrypoint MyEntrypoints1a "Do11")
           `ExactlyIs` 'Nothing
        )
    ]
  , testGroup "Nested parameter type"
    [ typeTest "Default entrypoint arg" $ Valid @
        (GetDefaultEntrypointArg MyEntrypoints1 `Is` MyEntrypoints1)
    , typeTest "Can get entrypoint on surface" $ Valid @
        (GetEntrypointArg MyEntrypoints1 "Do1" `Is` Integer)
    , typeTest "Can get entrypoint in deep" $ Valid @
        (GetEntrypointArg MyEntrypoints1 "Do11" `Is` Natural)
    , typeTest "Can get entrypoint without arg" $ Valid @
        (GetEntrypointArg MyEntrypoints1 "Do10" `Is` ())
    ]
  , testGroup "Parameter type with default entrypoint"
    [ typeTest "Default entrypoint arg" $ Valid @
        (GetDefaultEntrypointArg MyEntrypointsWithDef `Is` Integer)
    , typeTest "Can get non-default entrypoint" $ Valid @
        (GetEntrypointArg MyEntrypointsWithDef "NonDefault" `Is` Natural)
    ]
  , testGroup "Delegation"
    [ typeTest "Calling immediate entrypoint works" $ Valid @
        (GetEntrypointArg MyEntrypointsDelegated "Dod1" `Is` ())
    ]
  ]

-- | A contract which accepts 'Address' as parameter and calls specific
-- entrypoint of another contract.
callerContract
  :: forall cp mname arg.
     ( arg ~ GetEntrypointArgCustom cp mname
     , NiceConstant arg, NiceParameter arg, NiceParameterFull cp
     )
  => EntrypointRef mname
  -> arg
  -> Contract (TAddress cp) ()
callerContract epRef arg = defaultContract $
  L.car # L.contractCalling epRef #
  L.assertSome [mt|Contract lookup failed|] #
  L.push (toMutez 1) # L.push arg # L.transferTokens #
  L.dip (L.unit # L.nil) # L.cons # L.pair

test_Contract_call :: [TestTree]
test_Contract_call =
  [ testCase "Calling entrypoint" $
      integrationalTestExpectation $ do
        let myCallerContract = callerContract (Call @"Do11") 5
        let myTargetContract = defaultContract $ L.car # L.caseT @MyEntrypoints2
              ( #cDo10 /-> L.push 0
              , #cDo11 /-> L.nop
              ) # L.nil # L.pair

        caller <- lOriginate myCallerContract "Caller" () (toMutez 10)
        target <- lOriginateEmpty myTargetContract "Call target"
        lCallDef caller target
        lExpectStorageConst target (5 :: Natural)

  , testCase "Calling default entrypoint" $
      integrationalTestExpectation $ do
        let myCallerContract = callerContract CallDefault 3
        let myTargetContract = defaultContract $ L.car # L.caseT @MyEntrypointsWithDef
              ( #cDefault /-> L.nop
              , #cNonDefault /-> L.neg
              ) # L.nil # L.pair

        caller <- lOriginate myCallerContract "Caller" () (toMutez 10)
        target <- lOriginateEmpty myTargetContract "Call target"
        lCallDef caller target
        lExpectStorageConst target (3 :: Natural)

  , testCase "Calling root entrypoint" $
      integrationalTestExpectation $ do
        let myCallerContract = callerContract (Call @"root") (Dor2 5)
        let myTargetContract = defaultContract $ L.car # L.caseT @MyEntrypointsWithRoot
              ( #cDor1 /-> L.push 0
              , #cDor2 /-> L.nop
              ) # L.nil # L.pair

        caller <- lOriginate myCallerContract "Caller" () (toMutez 10)
        target <- lOriginateEmpty myTargetContract "Call target"
        lCallDef caller target
        lExpectStorageConst target (5 :: Natural)
  ]

test_Self_call :: [TestTree]
test_Self_call =
  [ testCase "Calling entrypoint" $
      integrationalTestExpectation $ do
        let myContract = defaultContract $ L.car # L.caseT @MyEntrypoints2
              ( #cDo10 /->
                  L.selfCalling @MyEntrypoints2 (Call @"Do11") #
                  L.push (toMutez 1) # L.push 5 # L.transferTokens #
                  L.dip (L.push @Integer 1 # L.nil) # L.cons # L.pair
              , #cDo11 /-> L.push @Integer 10 # L.add # L.nil # L.pair
              )

        contractRef <- lOriginate myContract "Contract" 0 (toMutez 10)
        lCallDef contractRef Do10
        lExpectStorageConst contractRef (15 :: Natural)
  , testCase "Calling root entrypoint" $
      integrationalTestExpectation $ do
        let myContract = defaultContract $ L.car # L.caseT @MyEntrypointsWithRoot
              ( #cDor1 /->
                  L.selfCalling @MyEntrypointsWithRoot (Call @"root") #
                  L.push (toMutez 1) # L.push (Dor2 5) # L.transferTokens #
                  L.dip (L.push @Integer 1 # L.nil) # L.cons # L.pair
              , #cDor2 /->
                  L.push @Integer 10 # L.add # L.nil # L.pair
              )
        contractRef <- lOriginate myContract "Contract" 0 (toMutez 10)
        lCallDef contractRef Dor1
        lExpectStorageConst contractRef (15 :: Natural)
  ]
