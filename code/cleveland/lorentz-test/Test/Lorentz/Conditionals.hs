-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# LANGUAGE NoApplicativeDo, RebindableSyntax #-}
{-# OPTIONS_GHC -Wno-unused-do-bind #-}

-- | Tests (and examples) on Lorentz' @if .. then .. else ..@.
module Test.Lorentz.Conditionals
  ( test_Named
  ) where

import Michelson.Test.Dummy
import Test.HUnit ((@?=))
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Lorentz

test_Named :: [TestTree]
test_Named =
  [ testCase "Named compare works good" $
      interpretLorentzLambda dummyContractEnv myContract (5 :: Natural)
        @?= Right True
  ]
  where
    myContract = do
      toNamed #x
      push @Natural 3; toNamed #y
      if #y >=. #x
        then push False
        else push True
