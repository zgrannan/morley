-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Lorentz.UStore.Migration.Batched.V2
  ( MyTemplate (..)
  ) where

import Lorentz.Base
import Lorentz.UStore

data MyTemplate = MyTemplate
  { bytes :: Integer |~> ByteString
  , int1 :: UStoreField Natural
  , code1 :: UStoreField $ Lambda () ()
  } deriving stock Generic
