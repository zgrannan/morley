-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.BalanceCheck
  ( nettestBalanceCheckScenario
  ) where

import Michelson.Untyped.Entrypoints
import Morley.Nettest
import Tezos.Address (Address)
import Tezos.Core (toMutez)

nettestBalanceCheckScenario
  :: forall m. Monad m => NettestImpl m -> m ()
nettestBalanceCheckScenario = uncapsNettest $ do
  nettestAddr :: Address <- resolveNettestAddress
  testAddr :: Address <- newFreshAddress "test"
  let
    test :: AddressOrAlias
    test = AddressResolved testAddr

    nettest :: AddressOrAlias
    nettest = AddressResolved nettestAddr
    td = TransferData
      { tdFrom = nettest
      , tdTo = test
      , tdAmount = toMutez 100
      , tdEntrypoint = DefEpName
      , tdParameter = ()
      }
  comment "balance is updated after transfer"
  transfer td
  checkBalance test (toMutez 100)
