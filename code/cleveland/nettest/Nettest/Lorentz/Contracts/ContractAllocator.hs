-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.Lorentz.Contracts.ContractAllocator
  ( allocatorContract
  ) where

import Lorentz as L

allocatorContract :: Contract [Address] Storage
allocatorContract = defaultContract allocatorContractLorentz

type Storage = ("storage" :! [Address])

allocatorContractLorentz :: ContractCode [Address] Storage
allocatorContractLorentz =
  unpair #
  dip (nil @Operation) #
  iter (
    contractCallingUnsafe @() DefEpName #
    whenSome
      (push (toMutez 1) # unit # transferTokens # cons)
    ) #
  unit # push (toMutez 0) # none @KeyHash #
  createContract @() @() (defaultContract failWith) #
  dip L.drop #
  cons #
  pair
