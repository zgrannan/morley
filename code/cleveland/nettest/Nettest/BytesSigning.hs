-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.BytesSigning
  ( nettestBytesSigningScenario
  ) where

import Lorentz (EntrypointRef(CallDefault), TAddress, ( # ))
import qualified Lorentz as L
import Morley.Nettest
import Tezos.Address (Address)
import Tezos.Crypto

checkSignatureContract :: L.Contract (PublicKey, (Signature, ByteString)) ()
checkSignatureContract = L.defaultContract $
  L.car #
  L.unpair # L.dip L.unpair #
  L.checkSignature # L.assert [L.mt|Invalid signature|] #
  L.unit # L.nil @L.Operation # L.pair

nettestBytesSigningScenario
  :: forall m. Monad m => NettestImpl m -> m ()
nettestBytesSigningScenario = uncapsNettest $ do
  nettestAddr :: Address <- resolveNettestAddress
  signerAddr :: Address <- newFreshAddress "signer"

  helperAddr :: TAddress (PublicKey, (Signature, ByteString)) <-
    originateSimple "helper" () checkSignatureContract

  let
    signer :: AddressOrAlias
    signer = AddressResolved signerAddr

    nettest :: AddressOrAlias
    nettest = AddressResolved nettestAddr

    bytes :: ByteString
    bytes = "some bytestring"

  signerAlias <- getAlias signer
  sig <- signBytes bytes signerAlias
  signerPK <- getPublicKey signer
  nettestPK <- getPublicKey nettest

  expectFailure (call helperAddr CallDefault (nettestPK, (sig, bytes))) $
    NettestFailedWith helperAddr [L.mt|Invalid signature|]
  call helperAddr CallDefault (signerPK, (sig, bytes))
