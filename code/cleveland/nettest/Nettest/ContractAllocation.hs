-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.ContractAllocation
  ( nettestContractAllocationScenario
  ) where

import Lorentz (EntrypointRef(CallDefault), TAddress)
import Morley.Nettest
import Tezos.Address (Address)
import Tezos.Core (toMutez)
import Util.Named

import Nettest.Lorentz.Contracts.ContractAllocator

nettestContractAllocationScenario
  :: forall m. Monad m => NettestImpl m -> m ()
nettestContractAllocationScenario = uncapsNettest $ do
  nettestAddr :: Address <- resolveNettestAddress
  addresses :: [Address] <-
    mapM (\i -> newFreshAddress $ "addr" <> show (i :: Integer)) [1..10]
  let
    nettest :: AddressOrAlias
    nettest = AddressResolved nettestAddr

    od = OriginateData
      { odFrom = nettest
      , odName = "allocator"
      , odStorage = #storage .! (take 570 $ cycle addresses)
      , odBalance = toMutez 25
      , odContract = allocatorContract
      }

  allocatorAddr :: TAddress [Address] <- originate od

  call allocatorAddr CallDefault (take 25 $ cycle addresses)
