-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Nettest.StorageCheck
  ( nettestStorageCheckScenario
  , nettestStorageCheckFailingScenario
  ) where

import Lorentz hiding (comment)
import Morley.Nettest

type Storage = Natural

nettestStorageCheckScenario :: NettestScenario
nettestStorageCheckScenario = uncapsNettest $ do
  addr <- addressResolved <$> originate OriginateData
    { odFrom = nettestAddress
    , odName = "save parameter in storage"
    , odStorage = 1
    , odBalance = toMutez 25
    , odContract = testContract
    }

  comment "checking initial storage"
  checkStorage addr (toVal @Natural 1)

  transfer TransferData
    { tdFrom = nettestAddress
    , tdTo = addr
    , tdAmount = toMutez 100
    , tdEntrypoint = DefEpName
    , tdParameter = 2 :: Natural
    }

  comment "storage is updated after transfer"
  checkStorage addr (toVal @Natural 2)

nettestStorageCheckFailingScenario :: NettestScenario
nettestStorageCheckFailingScenario = uncapsNettest $ do
  addr <- addressResolved <$> originate OriginateData
    { odFrom = nettestAddress
    , odName = "save parameter in storage"
    , odStorage = 1
    , odBalance = toMutez 25
    , odContract = testContract
    }

  comment "checking initial storage"
  checkStorage addr (toVal @Natural 99)

-- | Simple contract that takes a parameter and saves it in its storage.
testContract :: Contract Natural Storage
testContract = defaultContract $
  car #
  nil @Operation #
  pair
