-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
{-# LANGUAGE NoApplicativeDo #-}
module Main
  ( main
  ) where

import qualified Options.Applicative as Opt

import Morley.Nettest
import Morley.Nettest.Client (TestError)
import Morley.Nettest.Pure (BadScenario)
import Util.Exception (DisplayExceptionInShow(unDisplayExceptionInShow), displayUncaughtException)
import Util.Named

import Nettest.BalanceCheck
import Nettest.BytesSigning
import Nettest.ContractAllocation
import Nettest.ExpectFailure
import Nettest.StorageCheck

main :: IO ()
main = displayUncaughtException $ do
  let clientParser = clientConfigParser . pure $ Nothing
  parsedConfig <- Opt.execParser $
    parserInfo
      (#usage .! mempty)
      (#description .! "cleveland nettest testing scenario")
      (#header .! "cleveland nettest testing scenario")
      (#parser .! clientParser)
  env <- mkMorleyClientEnv parsedConfig
  let
    scenario :: NettestScenario
    scenario impl = do
      commentAction impl "Test nettest balance check"
      nettestBalanceCheckScenario impl
      commentAction impl "Test nettest storage check"
      nettestStorageCheckScenario impl
      commentAction impl "Test bytes signing"
      nettestBytesSigningScenario impl
      commentAction impl "Contract allocation"
      nettestContractAllocationScenario impl
      commentAction impl "Test expectFailure"
      nettestExpectFailureScenario impl

    failingScenarios :: (NettestScenario -> IO ()) -> IO ()
    failingScenarios runNettest = do
      expectTestFailure $ runNettest $ \impl -> do
        commentAction impl ""
        nettestStorageCheckFailingScenario impl

  putTextLn "Running pure nettest scenarios"
  runNettestViaIntegrational scenario
  failingScenarios runNettestViaIntegrational

  putTextLn "Running nettest scenarios on real chain"
  runNettestClient env scenario
  failingScenarios (runNettestClient env)

  where
    expectTestFailure :: IO () -> IO ()
    expectTestFailure test =
      try @IO @SomeException test >>= \case
        Left ex
          | isIntegrationalException ex || isNettestException ex -> pass
          | otherwise -> error $ "expected NettestFailure or BadScenario, but got: " <> show ex
        Right () -> error "expected scenario to fail, but it succeeded."

    isIntegrationalException ex = isJust $
      fromException @BadScenario ex
    isNettestException ex = isJust $
      fromException @DisplayExceptionInShow ex <&> unDisplayExceptionInShow >>= fromException @TestError
