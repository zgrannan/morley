-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for integrational testing machinery.
module Test.Integrational
  ( spec_Chain_id
  ) where

import Data.Default (def)
import Test.Hspec (Spec, it)

import Michelson.Test
import Michelson.Typed
import Tezos.Core
import Util.Named
import Michelson.Runtime.GState (initGState, gsChainId)

spec_Chain_id :: Spec
spec_Chain_id = do
  it "Chain id can be set" $
    integrationalTestExpectation $ do
      let code = DROP `Seq` CHAIN_ID `Seq` SOME `Seq` NIL `Seq` PAIR
      let contract = Contract
            { cCode = code
            , cParamNotes = starParamNotes @'TUnit
            , cStoreNotes = starNotes
            , cEntriesOrder = def
            }

      let expectedChainId = gsChainId initGState

      contractAddr <-
        tOriginate contract "" (toVal (Nothing @ChainId)) (toMutez 50)

      tTransfer (#from .! genesisAddress) (#to .! contractAddr)
        minBound DefEpName (toVal ())

      tExpectStorageConst contractAddr (toVal (Just expectedChainId))
