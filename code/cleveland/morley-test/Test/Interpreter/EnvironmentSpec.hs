-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for the 'environment.tz' contract

module Test.Interpreter.EnvironmentSpec
  ( test_environment
  ) where

import Hedgehog (Gen, forAll, property, withTests)
import qualified Hedgehog.Gen as Gen
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)
import Test.Tasty.HUnit (testCase)

import Michelson.Runtime.GState
import Michelson.Test (testTreesWithContract)
import Michelson.Text (mt)
import Michelson.Typed
import qualified Michelson.Typed as T
import qualified Michelson.Untyped as U
import Morley.Nettest
import Tezos.Core

import Test.Util.Contracts

test_environment :: IO [TestTree]
test_environment =
  testTreesWithContract (inContractsDir "environment.tz") $
  \contract -> pure
    [ nettestImpl contract
    , testCase ("Default balance") $
      nettestTestExpectation nettestDefaultBalance
    ]

data Fixture = Fixture
  { fPassOriginatedAddress :: Bool
  , fBalance :: Mutez
  , fAmount :: Mutez
  } deriving stock (Show)

genFixture :: Gen Fixture
genFixture = do
  fPassOriginatedAddress <- Gen.bool
  fBalance <- unsafeMkMutez <$> Gen.enum 1 1234
  fAmount <- unsafeMkMutez <$> Gen.enum 1 42
  return Fixture {..}

nettestImpl
  :: (U.Contract, T.Contract 'TAddress 'TUnit)
  -> TestTree
nettestImpl (uEnvironment, _environment)  = do
  testProperty description $
    withTests 50 $ property $ do
      fixture <- forAll genFixture
      nettestTestProp (scenario fixture)
  where
    scenario :: Fixture -> NettestScenario
    scenario = nettestScenario uEnvironment
    description =
      -- The conditions under which this contract fails are described in a comment
      -- at the beginning of the contract.
      "contract fails under certain conditions"

nettestScenario :: U.Contract -> Fixture -> NettestScenario
nettestScenario contract fixture = uncapsNettest $ do
  -- Then let's originated the 'environment.tz' contract
  let
    uoData = UntypedOriginateData
      { uodFrom = AddressResolved genesisAddress
      , uodName = "environment"
      , uodBalance = fBalance fixture
      , uodStorage = U.ValueUnit
      , uodContract = contract
      }

  environmentAddress <- originateUntyped uoData

  -- And transfer tokens to it
  let
    param
      | fPassOriginatedAddress fixture = environmentAddress
      | otherwise = genesisAddress
    transferData = TransferData
      { tdFrom = AddressResolved genesisAddress
      , tdTo =  AddressResolved environmentAddress
      , tdAmount = fAmount fixture
      , tdEntrypoint = DefEpName
      , tdParameter = param
      }

  -- Execute operations and check that interpreter fails when one of
  -- failure conditions is met or updates environment's storage
  -- approriately
  let
    balanceAfterTransfer = fBalance fixture `unsafeAddMutez` fAmount fixture
  if
    | balanceAfterTransfer > unsafeMkMutez 1000 ->
        transfer transferData `expectFailure` NettestFailedWith environmentAddress balanceAfterTransfer
    | fPassOriginatedAddress fixture ->
        transfer transferData `expectFailure` NettestFailedWith environmentAddress environmentAddress
    | fAmount fixture < unsafeMkMutez 15 ->
        transfer transferData `expectFailure` NettestFailedWith environmentAddress (fAmount fixture)
    | otherwise -> transfer transferData


nettestDefaultBalance :: NettestScenario
nettestDefaultBalance = uncapsNettest $ do
  addr <- newFreshAddress "random address testDefaultBalance"

  let
    transferData = TransferData
      { tdFrom = AddressResolved genesisAddress
      , tdTo = AddressResolved addr
      , tdAmount = unsafeMkMutez 1
      , tdEntrypoint = DefEpName
      , tdParameter = [mt||]
      }

  transfer transferData
  checkBalance (AddressResolved addr) (unsafeMkMutez 1)
