-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for the 'APPLY' instruction.

module Test.Interpreter.Apply
  ( unit_Basic
  , unit_Partially_applied_lambda_packed
  ) where

import Test.HUnit (Assertion)
import Text.Hex (decodeHex)

import Michelson.Test
import Michelson.Typed

import Test.Util.Contracts

unit_Basic :: Assertion
unit_Basic = do
  (_, applyContract) <- importContract (inContractsDir "apply.tz")
  let
    lam :: Instr '[ ToT (Integer, Integer) ] '[ ToT Integer ]
    lam = DUP `Seq` CAR `Seq` DIP CDR `Seq` SUB

  contractPropVal
    applyContract (validateStorageIs @Integer 2)
    dummyContractEnv (VLam $ RfNormal lam) (VInt 0)

unit_Partially_applied_lambda_packed :: Assertion
unit_Partially_applied_lambda_packed = do
  (_, partApplyContract) <-
    importContract (inContractsDir "partially-applied-lambda-packed.tz")
  let expected = decodeHex "05020000000f0743035b0005034202000000020316"
              ?: error "Bad bytes"
  contractProp @() @ByteString
    partApplyContract (validateStorageIs @ByteString expected)
    dummyContractEnv () ""
