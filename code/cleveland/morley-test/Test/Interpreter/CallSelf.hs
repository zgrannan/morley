-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Tests for the contract that calls self several times.

module Test.Interpreter.CallSelf
  ( test_self_caller
  ) where

import Hedgehog (forAll, property, withTests)
import qualified Hedgehog.Gen as Gen
import Test.HUnit (Assertion, (@?=))
import Test.Tasty (TestTree)
import Test.Tasty.Hedgehog (testProperty)
import Test.Tasty.HUnit (testCase)

import Michelson.Interpret (ContractEnv(..), InterpreterState(..), RemainingSteps(..))
import Michelson.Runtime.GState
import Michelson.Test (ContractPropValidator, contractProp, testTreesWithContract)
import Michelson.Test.Dummy
import Michelson.Typed as T
import qualified Michelson.Untyped as U
import Morley.Nettest
import Tezos.Core (unsafeMkMutez)

import Test.Util.Contracts

test_self_caller :: IO [TestTree]
test_self_caller =
  testTreesWithContract (inContractsDir "call_self_several_times.tz") $ \selfCaller ->
  pure (nettestImpl selfCaller)

gasForOneExecution :: Num a => a
gasForOneExecution = 19

gasForLastExecution :: Num a => a
gasForLastExecution = 20

type Parameter = 'TInt
type Storage = 'TNat

nettestImpl ::
     (U.Contract, T.Contract Parameter Storage)
  -> [TestTree]
nettestImpl (uSelfCaller, selfCaller) =
  [ testCase ("With parameter 1 single execution consumes " <>
      show @_ @Int gasForLastExecution <> " gas") $
    contractProp selfCaller (unitValidator gasForLastExecution) unitContractEnv
    (1 :: Integer) (0 :: Natural)

  , testCase ("With parameter 2 single execution consumes " <>
      show @_ @Int gasForOneExecution <> " gas") $
    contractProp selfCaller (unitValidator gasForOneExecution) unitContractEnv
    (2 :: Integer) (0 :: Natural)

  , testProperty propertyDescription $
    withTests 10 $ property $ do
      callCount <- forAll $ Gen.enum minCalls maxCalls
      nettestTestProp (nettestTransferScenario uSelfCaller callCount)
  ]
  where
    -- Environment for unit test
    unitContractEnv = dummyContractEnv
    -- Validator for unit test
    unitValidator ::
      RemainingSteps -> ContractPropValidator Storage Assertion
    unitValidator gasDiff (_, isRemainingSteps -> remSteps) =
      remSteps @?= ceMaxSteps unitContractEnv - gasDiff

    propertyDescription =
      "calls itself n times, sets storage to n OR fails due to gas limit"

    minCalls = 1
    maxCalls = 10

nettestTransferScenario :: U.Contract -> Word64 -> NettestScenario
nettestTransferScenario uSelfContract parameter = uncapsNettest $ do
  let
    uoData = UntypedOriginateData
      { uodFrom = AddressResolved genesisAddress
      , uodName = "self-caller"
      , uodBalance = unsafeMkMutez 1
      , uodStorage = U.ValueInt 0
      , uodContract = uSelfContract
      }
  address <- addressResolved <$> originateUntyped uoData
  let
    transferData = TransferData
      { tdFrom = AddressResolved genesisAddress
      , tdTo = address
      , tdAmount = minBound
      , tdEntrypoint = DefEpName
      , tdParameter = fromIntegral @Word64 @Integer parameter
      }
  transfer transferData
  let expectedStorage = toVal @Integer (fromIntegral parameter)
  checkStorage address expectedStorage
