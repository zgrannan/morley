-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Doc.Position
  ( test_DifferentPosition
  , test_SamePosition
  ) where

import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCase)

import Cleveland.Util (goesBefore)
import Michelson.Doc
import Michelson.Typed.Haskell (DType, DStorageType)

-- | Tests 'docItemPosition'.
test_DifferentPosition :: [TestTree]
test_DifferentPosition =
  [ testCase "`DComment` should come before `DGeneralInfoSection`" $
      Proxy @DComment `goesBefore` Proxy @DGeneralInfoSection
  , testCase "`DGeneralInfoSection` should come before `DGitRevision`" $
      Proxy @DGeneralInfoSection `goesBefore` Proxy @DGitRevision
  , testCase "`DGitRevision` should come before `DStorageType`" $
      Proxy @DGitRevision `goesBefore` Proxy @DStorageType
  , testCase "`DStorageType` should come before `DType`" $
      Proxy @DStorageType `goesBefore` Proxy @DType
  ]

test_SamePosition :: [TestTree]
test_SamePosition =
  [ testCase "`DTypeA` should come before `DTypeB`" $
      Proxy @DTypeA `goesBefore` Proxy @DTypeB
  ]

-- | Custom 'DocItem' for testing purpose.
data DTypeA = DTypeA
instance DocItem DTypeA where
  docItemPos = 1
  docItemSectionName = Nothing
  docItemToMarkdown _ DTypeA = ""

data DTypeB = DTypeB
instance DocItem DTypeB where
  docItemPos = 1
  docItemSectionName = Nothing
  docItemToMarkdown _ DTypeB = ""
