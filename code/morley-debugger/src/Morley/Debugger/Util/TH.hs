-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Morley.Debugger.Util.TH
  ( deriveSum
  ) where

import Control.Lens (filtered, ix, to, _head)
import Data.Aeson
import Data.Aeson.TH
import Data.Char (toLower)
import Data.Text (dropEnd)
import Language.Haskell.TH
import Language.Haskell.TH.Lens

-- TODO [#271] This is temporary and should be replaced by proper DAP integration.

-- | Derive 'FromJSON' for all components of a sum type,
-- then derive 'FromJSON' for the type itself, using a field of the
-- value to select the constructor.
--
-- If the sum type has a constructor named @Unknown*@, it must contain one field of type
-- 'Value'. It will be used to store a value when no command was matched.
deriveSum :: Name -> String -> String -> Q [Dec]
deriveSum typ suffix field = do
  let suffLength = length suffix
  TyConI (DataD _ _ _ _ cons _) <- reify typ

  decs <- concatMapM deepDerive
    $ cons ^.. traverse
      . filtered (not . isPrefixOf "Unknown" . nameBase . view name)
      . conFields
      . _2
      . _ConT

  let conNames = cons ^.. traverse . name . filtered (not . isPrefixOf "Unknown" . nameBase)
  let unkCon = cons ^? traverse . name . filtered (isPrefixOf "Unknown" . nameBase)

  a <- newName "a"

  -- For every constructor in the sum type try to match its name (lowercased and with stripped
  -- suffix) with a field of the json object.
  let matches = conNames <&> \n ->
        let
          dapName = n ^. to nameBase . to toText . to (dropEnd suffLength) . to (over _head toLower) . to toString
          body = normalB [| $(conE n) <$> parseJSON $(varE a) |]
        in match (conP 'Just [litP $ StringL dapName]) body []

  let matchesWithDef = matches <> [
        case unkCon of
          Nothing -> match (wildP) (normalB [| fail $ "Unknown " <> field |]) []
          Just con -> match (wildP) (normalB [| return $ $(conE con) $(varE a) |]) []
        ]

  funBody <- [|
    withObject "Request"
      (\obj -> $(caseE [| obj ^? ix field |] matchesWithDef))
      $(varE a) |]

  fromJSONDec <- instanceD (pure []) [t| FromJSON $(conT typ) |] $
    [ funD 'parseJSON [clause [varP a] (normalB $ pure funBody) []]
    ]

  toJSONDecs <- deriveToJSON (defaultOptions { sumEncoding = UntaggedValue }) typ

  return $ fromJSONDec : toJSONDecs <> decs

-- | Derive 'FromJSON' and 'ToJSON' for input type and all types it mentions that are
-- declared in the same module.
deepDerive :: Name -> Q [Dec]
deepDerive typ = do
  let typMod = nameModule typ
  TyConI (DataD _ _ _ _ cons _) <- reify typ

  let filteredNames = filtered ((== typMod) . nameModule)

  subDecs <- concatForM cons $ \con -> do
    -- Collect fields that are simply a constructor and fields
    -- where one constructor (* -> *) is applied to another, for example [Foo].
    let immediate = con ^.. conFields . _2 . _ConT . filteredNames
    let f1 = con ^.. conFields . _2 . _AppT . _2 . _ConT . filteredNames
    concatMapM deepDerive (immediate <> f1)

  -- DAP types have suffixes equal to the type's name.
  dec <- deriveJSON (defaultOptions { fieldLabelModifier = rdrop (nameBase typ) }) typ

  return $ dec <> subDecs

rdrop :: String -> String -> String
rdrop str = reverse . drop (length str) . reverse
