-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

{-# OPTIONS_GHC -Wno-orphans #-}

module Morley.Debugger.Util.DAP
  ( DAPSpecificRequest(..)
  , DAPRequest(..)

  , DAPSpecificEvent(..)
  , DAPSpecificResponse(..)
  )
  where

import Data.Aeson

import qualified Haskell.DAP as DAP

import Morley.Debugger.Util.TH

data DAPSpecificRequest
  = InitializeRequest DAP.InitializeRequest
  | DisconnectRequest DAP.DisconnectRequest
  | PauseRequest DAP.PauseRequest
  | TerminateRequest DAP.TerminateRequest
  | LaunchRequest DAP.LaunchRequest
  | SetBreakpointsRequest DAP.SetBreakpointsRequest
  | SetFunctionBreakpointsRequest DAP.SetFunctionBreakpointsRequest
  | SetExceptionBreakpointsRequest DAP.SetExceptionBreakpointsRequest
  | ConfigurationDoneRequest DAP.ConfigurationDoneRequest
  | ThreadsRequest DAP.ThreadsRequest
  | StackTraceRequest DAP.StackTraceRequest
  | ScopesRequest DAP.ScopesRequest
  | VariablesRequest DAP.VariablesRequest
  | ContinueRequest DAP.ContinueRequest
  | NextRequest DAP.NextRequest
  | StepInRequest DAP.StepInRequest
  | EvaluateRequest DAP.EvaluateRequest
  | CompletionsRequest DAP.CompletionsRequest
  | UnknownRequest Value
  deriving stock (Eq, Show, Generic)

$(deriveSum ''DAPSpecificRequest "Request" "command")

data DAPRequest = DAPRequest
  { drSpecific :: DAPSpecificRequest
  , drRaw :: Value
  }
  deriving stock (Eq, Show, Generic)

instance FromJSON DAPRequest where
  parseJSON val = DAPRequest <$> parseJSON val <*> pure val

data DAPSpecificEvent
  = OutputEvent DAP.OutputEvent
  | InitializedEvent DAP.InitializedEvent
  | TerminatedEvent DAP.TerminatedEvent
  | ExitedEvent DAP.ExitedEvent
  | ContinuedEvent DAP.ContinuedEvent
  | StoppedEvent DAP.StoppedEvent
  deriving stock (Eq, Show, Generic)

$(deriveSum ''DAPSpecificEvent "Event" "event")

data DAPSpecificResponse
  = InitializeResponse DAP.InitializeResponse
  | SetBreakpointsResponseSource DAP.SetBreakpointsResponse
  | SetFunctionBreakpointsResponseSource DAP.SetFunctionBreakpointsResponse
  | ThreadsResponse DAP.ThreadsResponse
  | StackTraceResponse DAP.StackTraceResponse
  | ScopesResponse DAP.ScopesResponse
  | VariablesResponse DAP.VariablesResponse
  | EvaluateResponse DAP.EvaluateResponse
  | CompletionsResponse DAP.CompletionsResponse
  deriving stock (Eq, Show, Generic)

-- Leads to duplicate instances. haskell-dap should be forked and all instances should be
-- derived there.
-- $(deriveSum ''DAPSpecificResponse "Response" "command")
