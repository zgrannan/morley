-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Integration of debugger core with lorentz.
module Morley.Debugger.Lorentz
  ( lCollectInterpretSnapshots
  ) where

import Lorentz
import Michelson.Interpret
import Morley.Debugger.Core

-- | Lorentz version of 'collectInterpretSnapshots'.
lCollectInterpretSnapshots
  :: forall cp st arg.
     (IsoValue arg, IsoValue st)
  => ContractCode cp st
  -> EntrypointCall cp arg
  -> arg
  -> st
  -> ContractEnv
  -> InterpretHistory
lCollectInterpretSnapshots contract_ epc param store env =
  collectInterpretSnapshots (compileLorentz contract_)
    epc (toVal param) (toVal store) env
