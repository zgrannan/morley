-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Debugger custom printing.
module Morley.Debugger.Core.Print
  ( DebugPrintMode (..)
  , DebugPrint (..)
  , debugBuild
  , DebugBuildable
  ) where

import qualified Data.Map as M
import Fmt (Buildable(..), Builder)

import Michelson.Typed
import Tezos.Address
import Tezos.Core
import Tezos.Crypto
import Util.Text

-- | Different ways of printing stuff.
data DebugPrintMode
  = DpmNormal
    -- ^ Print full info.
    -- Use this to allow user copy-paste ready Michelson values.
  -- TODO: add this when it comes to UI
  -- | DpmShort
  --   -- ^ Print only information relevant for debugging, preferrably
  --   -- human-readable.
  --   -- Rule of thumb: display as few information as needed to distinguish two
  --   -- different values in the most cases.

-- | Modifies print capabilities to fit debugger needs.
--
-- Differences include
-- 1. Printing operations;
-- 2. Printing shortened versions of values.
data DebugPrint a = DebugPrint DebugPrintMode a

-- | Constraint for showing something for debugger.
type DebugBuildable a = Buildable (DebugPrint a)

-- | Show something for debugger.
debugBuild :: DebugBuildable a => DebugPrintMode -> a -> Builder
debugBuild mode a = build $ DebugPrint mode a

dBuildImpl :: (DebugPrintMode -> a -> Builder) -> (DebugPrint a -> Builder)
dBuildImpl f (DebugPrint mode a) = f mode a

instance Buildable (DebugPrint Mutez) where
  build (DebugPrint _ m) = build m

instance Buildable (DebugPrint KeyHash) where
  build = dBuildImpl $ \case
    DpmNormal -> build

instance Buildable (DebugPrint PublicKey) where
  build = dBuildImpl $ \case
    DpmNormal -> build

instance Buildable (DebugPrint Address) where
  build = dBuildImpl $ \case
    DpmNormal -> build

instance Buildable (DebugPrint EpAddress) where
  build = dBuildImpl $ \case
    DpmNormal -> build

instance Buildable (DebugPrint Signature) where
  build = dBuildImpl $ \case
    DpmNormal -> build

instance Buildable (DebugPrint ByteString) where
  build = dBuildImpl $ \case
    DpmNormal -> build . untypeValue . VBytes

instance Buildable (DebugPrint Timestamp) where
  build = dBuildImpl $ \case
    DpmNormal -> \t ->
      if t < [timestampQuote|1900-01-01T00:00:00Z|]
      then build @Integer (timestampToSeconds t) <> "s"
      else build t

instance Buildable (DebugPrint Operation) where
  build (DebugPrint mode op) = case op of
    OpTransferTokens TransferTokens{..} ->
      "<transfer " <> debugBuild mode ttAmount <> " with arg " <>
      debugBuild mode ttTransferArgument <> " to " <>
      buildVContract ttContract <> ">"
    OpSetDelegate (SetDelegate mbDelegate) ->
      "<set delegate to " <> maybe "none" (debugBuild mode) mbDelegate <> ">"
    OpCreateContract CreateContract{..} ->
      "<create contract balance=" <> debugBuild mode ccBalance <> " \
      \ storage=" <> debugBuild mode ccStorageVal <> ">"


-- This instance differs from @instance RenderDoc U.Value@:
-- 1. It doesn't require any constraints on @t@;
-- 2. Prints operations;
-- 3. Renders only relevant parts of values.
instance Buildable (DebugPrint (Value t)) where
  build (DebugPrint mode val) = buildValue mode False val

-- Bool argument stands for need in parentheses
buildValue :: DebugPrintMode -> Bool -> Value t' -> Builder
buildValue mode pn = \case
  VInt i -> build i
  VNat n -> build n
  VString s -> build s
  VBytes b -> debugBuild mode b
  VMutez m -> build m
  VBool b -> build b
  VKeyHash k -> debugBuild mode k
  VTimestamp t -> debugBuild mode t
  VAddress a -> debugBuild mode a
  VKey k -> debugBuild mode k
  VUnit -> "()"
  VSignature s -> debugBuild mode s
  VChainId c -> build c
  VLam{} -> "<code>"
  VOp op -> debugBuild mode op

  VContract addr sepc ->
    debugBuild mode $ EpAddress addr (sepcName sepc)

  VOption ov -> case ov of
    Nothing -> "None"
    Just v -> addParens pn $ "Some " <> buildValue mode True v
  VList vs ->
    surround "[" "]" $
    mconcat $ intersperse ", " (buildValue mode False <$> vs)
  VSet vs ->
    surround "{" "}" $
    mconcat $ intersperse ", " (buildValue mode False <$> toList vs)
  VMap vs ->
    surround "{" "}" $
    mconcat $ intersperse ", " (buildElt mode <$> M.toList vs)
  VBigMap vs ->
    surround "{" "}" $
    mconcat $ intersperse ", " (buildElt mode <$> M.toList vs)

  VPair (vl, vr) ->
    "(" <> buildValue mode False vl <> ", " <> buildValue mode False vr <> ")"
  VOr ve -> addParens pn $ case ve of
    Left v -> "Left " <> buildValue mode True v
    Right v -> "Right " <> buildValue mode True v

buildElt :: DebugPrintMode -> (Value k, Value v) -> Builder
buildElt mode (k, v) = buildValue mode False k <> " -> " <> buildValue mode False v

addParens :: Bool -> Builder -> Builder
addParens = \case
  True -> \x -> "(" <> x <> ")"
  False -> id
