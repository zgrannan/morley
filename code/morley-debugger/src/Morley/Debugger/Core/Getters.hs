-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Some functions to operate with snapshots data.
--
-- They are moved to a separate module because some of them are
-- time/memory consuming, it's fine to perform them once on user action, but
-- we would better not store their result in debugger state.
module Morley.Debugger.Core.Getters
  ( SomeStackElem (..)
  , stackToElems
  ) where

import Prelude hiding (Const(..))

import Data.Vinyl.Functor (Const(..))
import Data.Vinyl.Recursive (recordToList, rmap)
import Fmt (Buildable(..))

import Michelson.Typed
import Morley.Debugger.Core.Print
import Morley.Debugger.Core.Snapshots

-- | Stack element.
data SomeStackElem = forall t. SomeStackElem (Value t)

deriving stock instance Show SomeStackElem

instance Buildable (DebugPrint SomeStackElem) where
  build (DebugPrint mode (SomeStackElem v)) = build (DebugPrint mode v)

instance Eq SomeStackElem where
  SomeStackElem v1 == SomeStackElem v2 = v1 `eqValueExt` v2

-- | Turn stack into printable list of elements, from stack top to bottom
stackToElems :: SomeStack -> [SomeStackElem]
stackToElems (SomeStack stack) =
  recordToList $ rmap (\e -> Const $ SomeStackElem e) stack
