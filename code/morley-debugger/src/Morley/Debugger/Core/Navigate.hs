-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Debugger state, navigation within execution history.
module Morley.Debugger.Core.Navigate
  ( DebuggerState (..)
  , mkDebuggerState
  , curSnapshot
  , Direction (..)
  , move
  , moveTill
  ) where

import Control.Lens (makeLenses, (.=))

import Morley.Debugger.Core.Snapshots

-- Utilities
----------------------------------------------------------------------------

-- | List with a focus on an element, aka zipper.
--
-- Had to reimplement our own.
--
-- * 'list-zipper' package has too strict cabal deps to work with our lts;
-- * 'ListZipper' provides interface where at some given moment there can be no
--    focused element, this is too inconvenient for us.
data Tape a = Tape [a] a [a]
  deriving stock (Show, Generic)

-- | Make up a tape with initial focus at the left end.
mkTapeL :: NonEmpty a -> Tape a
mkTapeL (a :| as) = Tape [] a as

tapeFocus :: Tape a -> a
tapeFocus (Tape _ x _) = x

tapeNext :: Tape a -> Maybe (Tape a)
tapeNext = \case
  Tape _ _ [] -> Nothing
  Tape l x (r : rs) -> Just $ Tape (x : l) r rs

tapePrev :: Tape a -> Maybe (Tape a)
tapePrev = \case
  Tape [] _ _ -> Nothing
  Tape (l : ls) x rs -> Just $ Tape ls l (x : rs)

-- Snapshots navigation
----------------------------------------------------------------------------

-- | Debugger state.
data DebuggerState = DebuggerState
  { _dsSnapshots :: Tape InterpretSnapshot
    -- ^ The overall execution history + focus at a specific stage.
  }
  deriving stock (Show)

makeLenses ''DebuggerState

-- | Construct initial debugger state.
mkDebuggerState :: InterpretHistory -> DebuggerState
mkDebuggerState (InterpretHistory snapshots) = DebuggerState
  { _dsSnapshots = mkTapeL snapshots
  }

-- | Get the snapshot at the current position.
curSnapshot
  :: MonadState DebuggerState m
  => m InterpretSnapshot
curSnapshot = tapeFocus <$> use dsSnapshots

-- | Direction of movement over execution history.
data Direction = Backward | Forward

-- | Go 1 instruction forward/backward.
--
-- Returns @False@ if we have already reached of the ends and no state change
-- was applied.
-- If you don't care about whether state has changed, then explicitly ignore the
-- result of the call.
move
  :: MonadState DebuggerState m
  => Direction -> m Bool
move dir = do
  sps <- use dsSnapshots
  let tapeMove = case dir of { Backward -> tapePrev; Forward -> tapeNext }
  case tapeMove sps of
    Nothing -> pure False
    Just sps' -> (dsSnapshots .= sps') $> True

-- | Move over execution history, halting at the snapshot where predicate turns true.
--
-- This performs at least one step if we are not yet at beginning/end.
-- If we reach the end of the execution history, we stop there.
moveTill
  :: MonadState DebuggerState m
  => Direction -> (InterpretSnapshot -> Bool) -> m ()
moveTill dir predicate = do
  moved <- move dir
  when moved $ do
    sp <- curSnapshot
    unless (predicate sp) $ moveTill dir predicate
