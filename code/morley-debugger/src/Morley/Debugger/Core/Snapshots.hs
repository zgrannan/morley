-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Building a series of snapshots of contract interpretation.

module Morley.Debugger.Core.Snapshots
  ( SnapshotNo (..)
  , SomeStack (..)
  , InterpretStatus (..)
  , InterpretSnapshot (..)
  , issStack
  , InterpretHistory (..)
  , collectInterpretSnapshots
  ) where

import Data.Conduit (ConduitT, (.|))
import qualified Data.Conduit as C
import qualified Data.Conduit.Lift as CL
import qualified Data.List.NonEmpty as NE
import Data.Vinyl (Rec(..))
import qualified Text.Show

import Michelson.Interpret
import Michelson.Typed

-- | Number of snapshot in interpreter execution.
--
-- The snapshot with no 0 should correspond to the state upon interpretation
-- start.
newtype SnapshotNo = SnapshotNo Word
  deriving stock (Show)

initSnapshotNo :: SnapshotNo
initSnapshotNo = SnapshotNo 0

incSnapshotNo :: SnapshotNo -> SnapshotNo
incSnapshotNo (SnapshotNo i) = SnapshotNo (i + 1)

-- | A meta info for debugger which reports the current execution status.
data InterpretStatus
  = InterpretStarting SomeStack
    -- ^ State at the beginning of contract execution.
  | InterpretRunning SomeStack
    -- ^ Some intermediate state.
  | InterpretTerminatedOk
    -- ^ Last instruction has been executed, contract execution was successful.
  | InterpretFailed MichelsonFailed
    -- ^ Just executed @FAILWITH@.
  deriving stock (Show)

-- | Stack of some type.
--
-- We tend to use this type rather than list of some values because
-- it's easier to achieve sharing this way.
data SomeStack = forall s. SomeStack (Rec Value s)

instance Show SomeStack where
  show (SomeStack v) = case v of
    RNil -> "[]"
    x :& xs -> show x <> " :& " <> show (SomeStack xs)

-- | A snapshot of interpreter state at some execution point.
data InterpretSnapshot = InterpretSnapshot
  { issState :: InterpreterState
    -- ^ Interpreter state.
  , issStatus :: InterpretStatus
    -- ^ Meta info about at which stage of execution we currently are.
  , issSnapNo :: SnapshotNo
    -- ^ Sequence number.
  , issPrevStack :: SomeStack
    -- ^ Stack of the /previous/ snapshot.
    -- For the starting snapshot this is the same as initial stack.
  }
  deriving stock (Show)

-- | The current stack (or at least what would you probably expect it to be).
issStack :: InterpretSnapshot -> SomeStack
issStack InterpretSnapshot{..} = case issStatus of
  InterpretStarting st -> st
  InterpretRunning st -> st
  InterpretTerminatedOk -> issPrevStack
  InterpretFailed{} ->
    -- Cut off the argument we failed with
    case issPrevStack of
      SomeStack RNil -> SomeStack RNil
      SomeStack (_ :& st) -> SomeStack st

-- | A series of snapshots of a single contract execution.
--
-- The first snapshot should correspond to the state before the first instruction
-- executed, and the last one should correspond to the state after the last
-- instruction execution.
--
-- In practice this should share the data between individual snapshots
-- (e.g. common parts of stack and all the recent morley logs), thus
-- the amount of memory required to keep the whole history should be @O(steps)@.
--
-- Also, this list can potentially be (almost) infinite, so it is supposed to be
-- produced in a lazy way.
newtype InterpretHistory = InterpretHistory
  { unInterpretHistory :: NonEmpty InterpretSnapshot
  }
  deriving stock (Show)

-- | 'InterpretSnapshot' with last fields not initialized yet.
type RawInterpretSnapshot = SnapshotNo -> SomeStack -> InterpretSnapshot

-- | Our monadic stack, allows running interpretation and making snapshot
-- records.
type CollectingEvalOp =
  -- Including ConduitT to build snapshots sequence lazily.
  -- Normally ConduitT lies on top of the stack, but here we put it under
  -- ExceptT to make it record things even when a failure occurs.
  ExceptT MichelsonFailed $
  ConduitT () RawInterpretSnapshot $
  ReaderT ContractEnv $
  StateT InterpreterState $
  Identity

-- | Complete raw snapshots, enumerating them and giving each a reference to the
-- previous stack.
fillRawSnapshots
  :: Monad m
  => SomeStack
  -> ConduitT RawInterpretSnapshot InterpretSnapshot m ()
fillRawSnapshots = loop initSnapshotNo
  where
  loop no prevStack =
    whenJustM C.await $ \ris -> do
      let is = ris no prevStack
      C.yield is
      loop (incSnapshotNo no) (issStack is)

-- | Snapshots collector to be embedded into executor.
runInstrCollect :: InstrRunner CollectingEvalOp
runInstrCollect = \instr stack -> do
  newStack <- runInstrImpl runInstrCollect instr stack

  when (needRecord instr) $ do
    issState <- get
    let issStatus = InterpretRunning (SomeStack newStack)
    lift . C.yield $ \issSnapNo issPrevStack -> InterpretSnapshot{..}
    -- Here we could put the previous stack, but in other 'yield' places
    -- we can't do that so easily anyway, so let's pretend that old stack
    -- is unavailable here as well

  return newStack
  where
    needRecord = \case
      InstrWithNotes{} -> False
      InstrWithVarNotes{} -> False
      Nested{} -> False
      DocGroup{} -> False
      Seq{} -> False
      _ -> True

runCollectInterpretSnapshots
  :: CollectingEvalOp a
  -> ContractEnv
  -> SomeStack
  -> InterpretHistory
runCollectInterpretSnapshots act env initStack =
  InterpretHistory $
  -- This is safe because we yield at least two snapshots
  NE.fromList $
  runIdentity $
  C.sourceToList . (.| fillRawSnapshots initStack) $ do
    let initState = initInterpreterState env
    C.yield $ InterpretSnapshot initState (InterpretStarting initStack)
    (outcome, endState) <-
      CL.runStateC initState $ CL.runReaderC env $ runExceptT act
    let endStatus = either InterpretFailed (const InterpretTerminatedOk) outcome
    C.yield $ InterpretSnapshot endState endStatus

-- | Execute contract similarly to 'interpret' function, but in result
-- produce an entire execution history.
collectInterpretSnapshots
  :: forall cp st arg.
     ContractCode cp st
  -> EntrypointCallT cp arg
  -> Value arg
  -> Value st
  -> ContractEnv
  -> InterpretHistory
collectInterpretSnapshots instr epc param initStore env =
  runCollectInterpretSnapshots
    (runInstrCollect instr initStack)
    env
    (SomeStack initStack)
  where
    initStack = mkInitStack (liftCallArg epc param) initStore
