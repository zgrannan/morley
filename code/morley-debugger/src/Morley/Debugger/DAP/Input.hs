-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-FileCopyrightText: 2016 Alan Zimmerman
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ
-- SPDX-License-Identifier: LicenseRef-MIT-alanz

module Morley.Debugger.DAP.Input
  ( inputReader
  ) where

import Control.Concurrent.STM (TChan, writeTChan)
import Data.Aeson
import qualified Data.Attoparsec.ByteString as A
import qualified Data.Attoparsec.ByteString.Char8 as A
import Data.ByteString.Builder.Extra (defaultChunkSize)
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy as BSL

import Morley.Debugger.Util.DAP

-- Based on
-- https://github.com/alanz/haskell-lsp/blob/47d7270c9644941b37a8023bef2a0ddc198a2648/src/Language/Haskell/LSP/Control.hs#L118

separator :: ByteString
separator = "\r\n\r\n"

-- | A parser for one DAP input message.
--
-- It is one header, @Content-Length@ with a numeric value, followed by
-- 'separator' and then that many bytes.
dapParser :: A.Parser ByteString
dapParser = do
  void $ A.string "Content-Length: "
  len <- A.decimal
  void $ A.string separator
  A.take len

-- | Read DAP messages from 'stdin' and write them to a channel.
--
-- When there are no more messages or an unrecoverable error occurs,
-- 'Nothing' is written to the channel.
inputReader :: TChan (Maybe (Either String DAPRequest)) -> IO ()
inputReader ch = go (A.parse dapParser "") >> atomically (writeTChan ch Nothing)
  where
    go = \case
      A.Fail _ ctxt err -> do
        atomically $ writeTChan ch $ Just $ Left $
          "Could not parse input: " <> err <> " in " <> show ctxt
      A.Partial cont -> do
        inp <- BS.hGetSome stdin defaultChunkSize
        if null inp
           then pass
           else go $ cont inp
      A.Done rest msg -> do
        atomically $ writeTChan ch $ Just $ eitherDecode' $ BSL.fromStrict msg
        go $ A.parse dapParser rest
