-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Test.Morley.Debugger.Core
  ( hprop_Execution_history_is_lazy
  , test_History_is_correct
  ) where

import qualified Data.List.NonEmpty as NE
import Hedgehog (Property)
import Test.HUnit (Assertion, assertBool, assertFailure)
import Test.Tasty (TestTree)
import Test.Tasty.HUnit (testCaseSteps)

import Lorentz (( # ))
import qualified Lorentz as L
import Michelson.Interpret
import Michelson.Test (meanTimeUpperBoundProp, sec)
import Michelson.Test.Dummy
import Michelson.Typed (Value'(..), epcPrimitive)

import Morley.Debugger.Core
import Morley.Debugger.Lorentz

infiniteContract :: L.ContractCode () ()
infiniteContract =
  L.drop #
  L.push True # L.loop (L.push True) #
  L.unit # L.nil # L.pair

hprop_Execution_history_is_lazy :: Property
hprop_Execution_history_is_lazy =
  let
    history env = lCollectInterpretSnapshots infiniteContract epcPrimitive
              () () env
    history' env = InterpretHistory $
      NE.fromList (take 1000 (toList $ unInterpretHistory $ history env)) <>
      error "Went too far in execution history"

    run env =
        evaluatingState (mkDebuggerState (history' env)) $ do
          replicateM_ 3 $ move Forward
          curSnapshot

  in meanTimeUpperBoundProp (sec 1) run dummyContractEnv

simpleContract :: L.ContractCode () Natural
simpleContract =
  L.cdr #
  L.push @Natural 1 #
  L.add #
  L.nil # L.pair

failingContract :: L.ContractCode L.MText ()
failingContract =
  L.car # L.failWith

test_History_is_correct :: [TestTree]
test_History_is_correct =
  [ testCaseSteps "simple contract" $ \step -> do
      let his = lCollectInterpretSnapshots simpleContract
                epcPrimitive () 5 dummyContractEnv
      historyTest his $ do
        lift $ step "Start"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 0
            , issStatus = InterpretStarting _
              -- TODO [#93]: compare stacks as well
            } -> pass
          sp -> unexpectedSnapshot sp

        moved1 <- move Forward
        lift $ assertBool "'move' returned False" moved1

        lift $ step "After 1 step"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 1
            , issStatus = InterpretRunning _
            } -> pass
          sp -> unexpectedSnapshot sp

        replicateM_ 5 $ move Forward
        lift $ step "After more steps"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 6
            , issStatus = InterpretTerminatedOk
            } -> pass
          sp -> unexpectedSnapshot sp

        moved3 <- move Forward
        lift $ assertBool "'move' in the end returned True" (not moved3)

        replicateM_ 6 $ move Backward
        lift $ step "Go to the beginning"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 0
            , issStatus = InterpretStarting _
            } -> pass
          sp -> unexpectedSnapshot sp

        replicateM_ 3 $ move Forward
        lift $ step "Go to the middle"
        checkSnapshot $ \case
          InterpretSnapshot
            { issSnapNo = SnapshotNo 3
            , issStatus = InterpretRunning _
            } -> pass
          sp -> unexpectedSnapshot sp

  , testCaseSteps "failing contract" $ \step -> do
      let his = lCollectInterpretSnapshots failingContract
                epcPrimitive [L.mt|aa|] () dummyContractEnv
      historyTest his $ do
        replicateM_ 999 $ move Forward
        lift $ step "Check end"
        checkSnapshot $ \case
          InterpretSnapshot
            { issStatus = InterpretFailed (MichelsonFailedWith
                                          (VString [L.mt|aa|]))
            } -> pass
          sp -> unexpectedSnapshot sp
  ]
  where
    historyTest
      :: InterpretHistory
      -> StateT DebuggerState IO ()
      -> Assertion
    historyTest his = evaluatingStateT (mkDebuggerState his)

    checkSnapshot
      :: (InterpretSnapshot -> Assertion)
      -> StateT DebuggerState IO ()
    checkSnapshot check = curSnapshot >>= lift . check

    unexpectedSnapshot
      :: InterpretSnapshot -> Assertion
    unexpectedSnapshot sp =
      assertFailure $ "Unexpected snapshot:\n" <> show sp
