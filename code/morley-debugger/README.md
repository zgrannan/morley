# Morley debugger

Morley debugger allows running a contract and tracking its execution.

### Exectutables

#### morley-debugger-console

A console application with a minimal feature set.

Run

```sh
stack exec morley-debugger-console -- demo
```

for demo launch.

Example of normal launch:

```sh
stack exec morley-debugger-console -- run --contract contracts/lsl.tz --storage 1 --parameter 1
```

As an alternative to building morley-debugger from scratch, you can use
[`morley-debugger.sh`](../../scripts/morley-debugger.sh) script that wraps docker image
with morley-debugger-console executable.

If you are using MacOS you will need to have `coreutils` installed, in order to do that run:
```
brew install coreutils
```

Example of launch is similar to stack:
```
./morley-debugger.sh -- run --contract contracts/lsl.tz --storage 1 --parameter 1
```
