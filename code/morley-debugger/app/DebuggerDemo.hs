-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module DebuggerDemo
  ( debuggerDemo
  ) where

import Lorentz (( # ))
import qualified Lorentz as L
import Michelson.Interpret (ContractEnv)
import Michelson.Test.Dummy
import Michelson.Typed (ForbidOr, epcPrimitive)

import DebuggerInputProcessor

demoContract :: L.ContractCode (Integer, Natural) Integer
demoContract =
  L.unpair # L.unpair # L.add # L.add #
  L.nil # L.pair

runDebuggerSimpleL
  :: forall cp st.
     (L.NiceParameterFull cp, L.NiceStorage st, ForbidOr (L.ToT cp))
  => L.ContractCode cp st -> cp -> st -> ContractEnv -> IO ()
runDebuggerSimpleL contract arg st env =
  L.withDict (L.niceParameterEvi @cp) $
  runDebugger (L.compileLorentz contract) epcPrimitive
    (L.toVal arg) (L.toVal st) env

debuggerDemo :: IO ()
debuggerDemo = runDebuggerSimpleL demoContract (3, 3) 5 dummyContractEnv
