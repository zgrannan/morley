-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Simple terminal application which demostrates debugger capabilities.
module Main
  ( main
  ) where

import Data.Version (showVersion)
import Fmt (pretty)
import Options.Applicative (execParser, fullDesc, header, helper, info, progDesc)
import qualified Options.Applicative as Opt
import Paths_morley_debugger (version)
import System.IO (utf8)

import Michelson.Runtime (prepareContract)
import Michelson.Test.Dummy (dummyContractEnv)
import Michelson.TypeCheck
  (SomeContract(..), typeCheckContract, typeVerifyParameter, typeVerifyStorage)
import Michelson.Typed (Contract(..))
import qualified Michelson.Typed as T
import qualified Michelson.Untyped as U
import Morley.CLI
import Util.IO
import Util.Named

import DebuggerDemo
import DebuggerInputProcessor

data DbgCmd
  = Run DbgRunOptions
  | Demo

data DbgRunOptions = DbgRunOptions
  { groContractFile :: FilePath
  , groStorageValue :: U.Value
  , groParameterValue :: U.Value
  , groEntrypoint :: U.EpName
  }

cmdParser :: Opt.Parser DbgCmd
cmdParser = Opt.hsubparser $ mconcat
  [ runSubCmd
  , demoSubCmd
  ]
  where
    runSubCmd = Opt.command "run" $ info
      (Run <$> runOptions)
      (progDesc "Debug given contract")

    demoSubCmd = Opt.command "demo" $ info
      (pure Demo)
      (progDesc "Debug demo contract")

    runOptions = do
      groContractFile <- contractFileOption
      groStorageValue <- valueOption Nothing (#name .! "storage")
                         (#help .! "Storage of a running contract")
      groParameterValue <- valueOption Nothing (#name .! "parameter")
                         (#help .! "Parameter for a contract run")
      groEntrypoint <- entrypointOption (#name .! "entrypoint")
                         (#help .! "Call specific entrypoint")
      return DbgRunOptions{..}

programInfo :: Opt.ParserInfo DbgCmd
programInfo = info (helper <*> versionOption <*> cmdParser) $
  mconcat
  [ fullDesc
  , progDesc "Morley-debugger: terminal app with minimal debugger functionality"
  , header "Morley tools"
  ]
  where
    versionOption = Opt.infoOption ("morley-debugger-" <> showVersion version)
      (Opt.long "version" <> Opt.help "Show version.")

main :: IO ()
main = withEncoding stdin utf8 $ do
  hSetTranslit stdout
  hSetTranslit stderr
  cmd <- execParser programInfo

  case cmd of
    Run DbgRunOptions{..} -> do
      uContract <- prepareContract (Just groContractFile)
      SomeContract (contract@Contract{} :: Contract cp st) <-
        either (error . pretty) pure $ typeCheckContract uContract
      epcRes <-
        maybe (error "Specified entrypoint not found") pure $
        T.mkEntrypointCall groEntrypoint (cParamNotes contract)
      case epcRes of
        T.MkEntrypointCallRes (_ :: T.Notes arg) epc -> do
          arg <-
            either (error . pretty) pure $
            typeVerifyParameter @arg mempty groParameterValue
          storage <-
            either (error . pretty) pure $
            typeVerifyStorage groStorageValue
          runDebugger (cCode contract) epc arg storage dummyContractEnv
    Demo ->
      debuggerDemo
