-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Main
  ( main
  ) where

import Control.Concurrent.Async (race_)
import Control.Concurrent.STM (newTChanIO, readTChan)
import qualified Data.ByteString.Char8 as BS
import System.Environment (getArgs)
import System.IO (BufferMode(..), hSetBuffering)

import Morley.Debugger.DAP.Input

-- HLint wrongfully thinks we are not using these things from universum,
-- perhaps because of morley-prelude.

{-# ANN module ("HLint: ignore Use 'stdin' from Universum" :: Text) #-}
{-# ANN module ("HLint: ignore Use 'stdout' from Universum" :: Text) #-}
{-# ANN module ("HLint: ignore Use 'withFile' from Universum" :: Text) #-}
{-# ANN module ("HLint: ignore Use 'WriteMode' from Universum" :: Text) #-}

main :: IO ()
main = do
  args <- getArgs
  let logFile = case args of
        fp : _ -> fp
        [] -> "morley-dap.log"

  -- DAP messages (both input and output) are not newline-terminated, so if
  -- we don't disable buffering, some messages may be not delivered in time.
  hSetBuffering stdin NoBuffering
  hSetBuffering stdout NoBuffering

  withFile logFile WriteMode $ \h -> do
    hSetBuffering h LineBuffering
    ch <- newTChanIO

    race_ (inputReader ch) $ runMaybeT $ forever $ do
      cmd <- atomically $ readTChan ch
      case cmd of
        Nothing -> liftIO (BS.hPutStrLn h "DONE") >> mzero
        Just val -> liftIO $ do
          BS.hPutStrLn h $ show val
          BS.hPutStrLn h ""
