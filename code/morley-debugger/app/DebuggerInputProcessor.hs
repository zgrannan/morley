-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module DebuggerInputProcessor
  ( runDebugger
  ) where

import qualified Data.Text as T
import Fmt (fmt, pretty)
import System.Console.Haskeline

import qualified Michelson.Interpret as T
import qualified Michelson.Typed as T
import Morley.Debugger.Core

availableCommands :: [Text]
availableCommands =
  [ "exit"
  , "status"
  , "next"
  , "next:<steps num>"
  , "prev"
  , "prev:<steps num>"
  ]

-- | 'InputT' does not have mtl instances, so it's not possible to lift it nicer than this.
--
-- 'outputStrLn' advertises "cross-platform output of text that may contain Unicode characters",
-- so we use it instead of 'putTextLn'.
outputTextLn :: Text -> MaybeT (StateT DebuggerState (InputT IO)) ()
outputTextLn = lift . lift . outputStrLn . toString

-- | Execute debugger app.
runDebugger
  :: T.ContractCode cp st
  -> T.EntrypointCallT cp arg
  -> T.Value arg
  -> T.Value st
  -> T.ContractEnv
  -> IO ()
runDebugger instr epc arg st env = do
  let his = collectInterpretSnapshots instr epc arg st env
  putTextLn $
    "Debug started. Available commands:" <>
    mconcat (map ("\n * " <>) availableCommands)
  putTextLn ""

  void . runInputT defaultSettings . evaluatingStateT (mkDebuggerState his) . runMaybeT . forever $ do
    -- Nothing from getInputLine means end of input, e.g. ^D.
    cmd <- (lift $ lift $ getInputLine "morley> ") >>= maybe mzero (return . fromString)
    case cmd of
      "" -> pass
      "exit" -> mzero

      "status" -> do
        is@InterpretSnapshot{..} <- curSnapshot
        let SnapshotNo spNo = issSnapNo
        outputTextLn $ "Current state: #" <> show spNo <> " / " <> case issStatus of
          InterpretStarting _ -> "started"
          InterpretRunning _ -> "running"
          InterpretTerminatedOk -> "execution finished"
          InterpretFailed err -> "failed: " <> pretty err

        outputTextLn "Current stack: "
        forM_ (stackToElems $ issStack is) $ \(SomeStackElem v) ->
          putTextLn . fmt $ "* " <> debugBuild DpmNormal v

        outputTextLn ""

      "next" -> do
        moved <- move Forward
        unless moved $ outputTextLn "Already at end\n"

      (T.stripPrefix "next:" -> Just (readMaybe @Int . toString -> Just num)) -> do
        moved <- replicateM num $ move Forward
        unless (and moved) $ outputTextLn "Reached end\n"

      "prev" -> do
        moved <- move Backward
        unless moved $ outputTextLn "Already at beginning\n"

      (T.stripPrefix "prev:" -> Just (readMaybe @Int . toString -> Just num)) -> do
        moved <- replicateM num $ move Backward
        unless (and moved) $ outputTextLn "Reached beginning\n"

      -- NOTE: When adding new commands, don't forget to update 'availableCommands'

      other -> outputTextLn $ "Unknown command: " <> show other
