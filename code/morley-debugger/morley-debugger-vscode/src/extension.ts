// SPDX-FileCopyrightText: 2020 Tocqueville Group
// SPDX-FileCopyrightText: Microsoft Corporation
//
// SPDX-License-Identifier: LicenseRef-MIT-TQ
// SPDX-License-Identifier: LicenseRef-MIT-Microsoft

'use strict';

import * as vscode from 'vscode';
import { WorkspaceFolder, DebugConfiguration, ProviderResult, CancellationToken } from 'vscode';

export function activate(context: vscode.ExtensionContext) {

    // register a configuration provider for 'michelson' debug type
    const provider = new MichelsonConfigurationProvider();
    context.subscriptions.push(vscode.debug.registerDebugConfigurationProvider('michelson', provider));

    let factory =new DebugAdapterExecutableFactory();

    context.subscriptions.push(vscode.debug.registerDebugAdapterDescriptorFactory('michelson', factory));
    if ('dispose' in factory) {
        context.subscriptions.push(factory);
    }
}

export function deactivate() {
    // nothing to do
}

class MichelsonConfigurationProvider implements vscode.DebugConfigurationProvider {

    /**
     * Massage a debug configuration just before a debug session is being launched,
     * e.g. add all missing attributes to the debug configuration.
     */
    resolveDebugConfiguration(folder: WorkspaceFolder | undefined, config: DebugConfiguration, token?: CancellationToken): ProviderResult<DebugConfiguration> {

        // if launch.json is missing or empty
        if (!config.type && !config.request && !config.name) {
            const editor = vscode.window.activeTextEditor;
            if (editor && editor.document.languageId === 'michelson') {
                config.type = 'michelson';
                config.name = 'Launch';
                config.request = 'launch';
                config.program = '${file}';
                config.stopOnEntry = true;
            }
        }

        if (!config.program) {
            return vscode.window.showInformationMessage("Cannot find a program to debug").then(_ => {
                return undefined;   // abort launch
            });
        }

        return config;
    }
}

class DebugAdapterExecutableFactory implements vscode.DebugAdapterDescriptorFactory {

    createDebugAdapterDescriptor(session: vscode.DebugSession, executable: vscode.DebugAdapterExecutable | undefined): ProviderResult<vscode.DebugAdapterDescriptor> {
        let args = typeof session.workspaceFolder === "undefined" ? [] : [session.workspaceFolder.uri.fsPath + "/morley-dap.log"];
        executable = new vscode.DebugAdapterExecutable(session.configuration.morley_dap_exe, args);

        // make VS Code launch the DA executable
        return executable;
    }
}
