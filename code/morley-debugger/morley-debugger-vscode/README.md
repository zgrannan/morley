# Alpha Morley Debugger VSCode extension

This README describes how to run the extension in _development_ mode, until we set up proper
packaging and distribution for it.

## Prerequisites

- VSCode
- yarn
- [Michelson Syntax](https://marketplace.visualstudio.com/items?itemName=baking-bad.michelson) extension for VSCode

## Using

First, run `yarn` to install dependencies.

Then, open this folder in VSCode, then press `F5`. It should run `Extension` configuration.

A new VSCode window will open. In it, open any `.tz` file, press `F5` and choose `Michelson Morley
Debugger`. Then click `create a launch.json file`. VSCode will create a stub config. In that config
you need to fill `morley_dap_exe` field with an absolute path to `morley-debug-adapter` executable
built from this repo.

Then a debug session will start. Currently, all it does is to log DAP requests it gets to
`morley-dap.log` file in the directory opened in VSCode. You will see something like this:

```haskell
Right (DAPRequest {drSpecific = InitializeRequest (InitializeRequest {seqInitializeRequest = 1, typeInitializeRequest = "request", commandInitializeRequest = "initialize", argumentsInitializeRequest = InitializeRequestArguments {adapterIDInitializeRequestArguments = "michelson", linesStartAt1InitializeRequestArguments = True, columnsStartAt1InitializeRequestArguments = True, pathFormatInitializeRequestArguments = "path"}}), drRaw = Object (fromList [("command",String "initialize"),("arguments",Object (fromList [("clientID",String "vscode"),("supportsVariablePaging",Bool True),("supportsProgressReporting",Bool True),("supportsVariableType",Bool True),("adapterID",String "michelson"),("locale",String "ru"),("columnsStartAt1",Bool True),("supportsRunInTerminalRequest",Bool True),("clientName",String "Visual Studio Code"),("pathFormat",String "path"),("linesStartAt1",Bool True)])),("seq",Number 1.0),("type",String "request")])})

Right (DAPRequest {drSpecific = DisconnectRequest (DisconnectRequest {seqDisconnectRequest = 2, typeDisconnectRequest = "request", commandDisconnectRequest = "disconnect", argumentsDisconnectRequest = Just (DisconnectArguments {restartDisconnectRequestArguments = Just False})}), drRaw = Object (fromList [("command",String "disconnect"),("arguments",Object (fromList [("restart",Bool False)])),("seq",Number 2.0),("type",String "request")])})
```

This means it can successfully parse VSCode's messages.
