-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | 'Expr'essions supported in Indigo language and their compilation to
-- Lorentz code.
--
-- This module contains only basic building blocks that can be used to
-- implement anything else. Other modules provide high level language
-- constructions and standard functions.

module Indigo.Internal.Expr
  ( module Exported
  ) where

import Indigo.Internal.Expr.Compilation as Exported
import Indigo.Internal.Expr.Decompose as Exported
import Indigo.Internal.Expr.Symbolic as Exported
import Indigo.Internal.Expr.Types as Exported
