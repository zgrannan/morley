-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | This module serves the purpose of listing @hiding@ rules of
-- Prelude that conflicts with Indigo exported functions.

module Indigo.Prelude
  ( module Prelude
  ) where

import Prelude hiding
  ( abs, and, concat, div, empty, fromInteger, fst, get, mod, not, or, snd, some
  , unless, when, whenLeft, whenRight, xor, (%~), (&&), (*), (+), (-), (/), (/=)
  , (<), (<=), (<>), (==), (>), (>=), (?:), (^), (^.), (^?), (||)
  )
