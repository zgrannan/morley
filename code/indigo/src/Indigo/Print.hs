-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | Module containing pretty-printing of Indigo contracts

module Indigo.Print
  ( printIndigoContract

  , printAsMichelson
  , saveAsMichelson
  ) where

import Indigo.Compilation
import Indigo.Internal.Object
import Indigo.Lorentz
import Indigo.Prelude

-- | Pretty-print an Indigo contract into Michelson code.
printIndigoContract
  :: forall param st .
     ( IsObject st
     , NiceParameterFull param
     , NiceStorage st
     )
  => Bool -- ^ Force result to be single line
  -> IndigoContract param st
  -> LText
printIndigoContract forceSingleLine ctr = printLorentzContract forceSingleLine $
  defaultContract $
  compileIndigoContract @param @st ctr

-- | Prints the pretty-printed Michelson code of an Indigo contract to
-- the standard output.
--
-- This is intended to be easy to use for newcomers.
printAsMichelson
  :: forall param st m . ( IsObject st
     , NiceParameterFull param, NiceStorage st
     , MonadIO m
     )
  => IndigoContract param st
  -> m ()
printAsMichelson cntr = putStrLn (printIndigoContract @param @st False cntr)

-- | Saves the pretty-printed Michelson code of an Indigo contract to
-- the given file.
--
-- This is intended to be easy to use for newcomers.
saveAsMichelson
  :: forall param st m . ( IsObject st
     , NiceParameterFull param, NiceStorage st
     , MonadIO m, MonadMask m
     )
  => IndigoContract param st
  -> FilePath
  -> m ()
saveAsMichelson cntr filePath =
  withFile filePath WriteMode (`hPutStrLn` (printIndigoContract @param @st False cntr))
