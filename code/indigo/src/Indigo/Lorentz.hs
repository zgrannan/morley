-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

-- | This module is intended to be imported instead of "Lorentz" by Indigo
-- modules.
--
-- The idea is to avoid repeating common @hiding@ rules and to not export any of
-- Lorentz's Instructions and Macros.

module Indigo.Lorentz
  ( module L
  ) where

import Lorentz.ADT as L hiding (HasField, caseT, case_, construct, constructT, setField)
import Lorentz.Annotation as L (HasAnnotation)
import Lorentz.Arith as L
import Lorentz.Base as L
import Lorentz.Coercions as L
import Lorentz.Common as L
import Lorentz.Constraints as L
import Lorentz.Doc as L hiding
  (contractGeneral, contractGeneralDefault, contractName, doc, docGroup, docStorage)
import Lorentz.Entrypoints as L
import Lorentz.Entrypoints.Doc as L hiding
  (entryCase, entryCaseSimple, entryCase_, finalizeParamCallingDoc)
import Lorentz.Errors as L hiding (failCustom, failCustom_)
import Lorentz.Errors.Common as L ()
import Lorentz.Errors.Numeric as L
import Lorentz.Ext as L
import Lorentz.Instr as L (NonZero)
import Lorentz.Macro as L (View, VoidResult, Void_, voidResultTag)
import Lorentz.Pack as L
import Lorentz.Polymorphic as L
import Lorentz.Print as L
import Lorentz.Referenced as L
import Lorentz.Run as L hiding (Contract(..))
import Lorentz.StoreClass as L
import Lorentz.UParam as L
import Lorentz.UStore as L
import Lorentz.Value as L
import Lorentz.Zip as L ()
