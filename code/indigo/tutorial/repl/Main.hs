-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Main (main) where

import Indigo

-- | Executable which is intended to be used to run indigo repl via: `stack ghci :indigo-repl`
main :: IO ()
main = putStrLn @Text
  "Welcome to Indigo REPL.\n\n\
  \For a detailed explanation on how to use the REPL,\n\
  \please refer to the tutorial via this link:\n\
  \https://gitlab.com/morley-framework/morley/-/tree/master/code/indigo/tutorial/README.md"
