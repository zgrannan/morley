-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Tutorial.Expressions.Math
  ( mathContract
  ) where

import Indigo

mathContract :: IndigoContract Integer Integer
mathContract param = defContract do
  zero <- new$ 0 int
  _z <- new$ zero
  five <- new$ zero + 6 int
  five -= 1 int
  storage += abs (param * five - 10 int)

storage :: HasStorage Integer => Var Integer
storage = storageVar
