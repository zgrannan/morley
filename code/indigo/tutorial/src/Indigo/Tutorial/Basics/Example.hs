-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Tutorial.Basics.Example
  ( exampleContract
  , textKeeper
  ) where

import Indigo

exampleContract :: IndigoContract Integer Integer
exampleContract param = defContract do
  a <- new$ 1 int
  storageVar =: param + a

textKeeper :: IndigoContract MText MText
textKeeper param = defContract do
  storageVar =: param
