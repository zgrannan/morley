-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Tutorial.Statements.Control
  ( controlContract
  ) where

import Indigo

data IncrementIf
  = IsZero Integer
  | HasDigitOne Natural
  deriving stock (Generic, Show)
  deriving anyclass (IsoValue)

instance ParameterHasEntrypoints IncrementIf where
  type ParameterEntrypointsDerivation IncrementIf = EpdPlain

controlContract :: IndigoContract IncrementIf Natural
controlContract param = defContract do
  base <- new$ 10 nat
  result <- case_ param $
    ( #cIsZero #= \val -> do
        return (val == 0 int)
    , #cHasDigitOne #= \val -> do
        checkRes <- new$ False
        while (val > base && checkRes == False) do
          val =: val / base
          remainder <- new$ val % base
          checkRes =: remainder == 1 nat
        return checkRes
    )
  if result == False
    then storage =: 0 nat
    else storage += 1 nat

storage :: HasStorage Natural => Var Natural
storage = storageVar
