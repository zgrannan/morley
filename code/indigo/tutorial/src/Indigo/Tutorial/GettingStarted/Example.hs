-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Indigo.Tutorial.GettingStarted.Example
  ( exampleContract
  ) where

import Indigo

exampleContract :: IndigoContract Integer Integer
exampleContract param = defContract do
  a <- new$ 1 int
  storageVar =: param + a

{-
Resulting Michelson Contract:

parameter int;
storage int;
code { NIL operation;
       SWAP;
       DUP;
       CAR;
       DIP { CDR };
       PUSH int 1;
       DUP;
       DIP 2
           { DUP };
       DIG 2;
       ADD;
       DIP 3
           { DROP };
       DUG 2;
       DROP;
       DROP;
       SWAP;
       PAIR };

-}
