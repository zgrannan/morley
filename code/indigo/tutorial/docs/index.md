<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# Indigo eDSL

Indigo eDSL is a high level language for Michelson contract development.

It is meant first and foremost to free you from the burden of manual stack
management and supports common features of imperative languages.

Even with a very small example the difference in ease of use is quite visible:


=== "Indigo"

    {!haskell 11-14 src/Indigo/Tutorial/GettingStarted/Example.hs!}

=== "Michelson"

    {!bash 19-38 src/Indigo/Tutorial/GettingStarted/Example.hs!}

## Overall idea

Michelson contracts are stack-based and often follow the repetitive pattern of
copying the required values on top of the stack to apply an instruction to them.
Indigo can associate variables to values in the stack and it's able to refer to
them in order to handle this pattern automatically.

In addition to this it can override existing variables with new values, allowing
the manipulation of the stack to be automatic.

Leveraging this ability, it also supports features such as: imperative statements
(`if`, `while`, ...), expressions, operators (`+`, `||`, `==`, ...), scoped
functions definition and errors.

Indigo is built on top of `Lorentz`, which in turn is built on top of `Morley`, a
dialect that is a superset of vanilla Michelson.
If you are interested more in the relationships between these projects you can
head over to [the repo on GitLab](https://gitlab.com/morley-framework/morley).

Indigo uses Morley to map values from Michelson to Haskell and to compile to
Michelson code (or to any of the other projects in the chain mentioned above).

## Learning and using Indigo

An introduction to the syntax and the features of the Indigo language, as well as
instructions on how to use it, are available in the form of a step-by-step tutorial.

This aims to be accessible to as many people as possible, but also contains more
technical informations for Haskell developers that are interested in learning more.

For this reason anyone interested in learning more about the language in practice
should start by taking a look at its [Getting started page](getting_started.md).

## Additional documentation

In addition to the tutorial, this documentation contains full reference pages
and, as for the other Morley projects, Indigo has [Haddock documentation](https://hackage.haskell.org/package/indigo).

## Contributing to this website

If you want to report a bug or request changes to this website you can do so by
creating a [new GitLab issue](https://gitlab.com/morley-framework/morley/-/issues/new?issue[title]=Indigo%20website:%20).

If you instead would like to propose changes, you can use the `Edit on GitLab`
links at the top of every page, or open a [new merge request](https://gitlab.com/morley-framework/morley/-/merge_requests/new).

The source for this website can be found [here in the repository](https://gitlab.com/morley-framework/morley/-/tree/master/code/indigo/tutorial)
where the README contains info on how it's built.
