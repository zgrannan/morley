<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: LicenseRef-MIT-TQ
-->

# include_code markdown extension

This directory contains a custom extension for source code inclusion in markdown.

This was inspired by:
- [markdown-include](https://github.com/cmacmackin/markdown-include)
- [markdown-include-lines](https://github.com/simonrenger/markdown-include-lines)

It is intended to be used only for the generation of Indigo's static website because
it is contingent on the use of `codehilite` and adds an HTML element for the title
that needs to be styled with CSS.

The reasons it exists (instead of using one of the above) are that
1. to correctly highlight haskell code the file itself cannot be modified to add
   line numbers nor "source file" headers.
2. some feature promised by the instructions above do not actually work (e.g.
   inclusion of lines list) and as such the syntax is a bit too reductive.
3. code blocks cannot be inside other block (here they work but still need another
   extension to be correctly rendered).

## Installation and usage

The extension can be simply installed with `pip install`, by passing it the path
to this directory (`mardown-ext`).

To use it in you markdown documents you need this syntax:
```
{! <language> <ranges> <path_to_file> !}
```

Note: there cannot be anything else on the line and it can only have an indentation
of spaces at the beginning that is a multiple of 4.

`<language>` will be passed to code block for highlighting.

`<path_to_file>` is the realtive or absolute path of the file you want to include
some lines of.

`<ranges>` has to be one or many (space separated) of the following:
- `<int>` where `int` is a line number, will include that single line
- `*` for the entirety of the file
- `<val>-<val>` where `val` can be either of the 2 above, will include all the
  lines in between the two (inclusive) or from-the-start/to-the-end in case of `*`.
