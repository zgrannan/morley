# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

from setuptools import setup, find_packages

setup(
  name = 'include_code',
  packages = find_packages(),
  version = '0.1',
  description = "",
  long_description = "",
  author = 'Pasquale Pinto (@pasqu4le)',
  author_email = '',
  url = '',
  download_url = '',
  keywords = [],
  install_requires = ['markdown']
)
