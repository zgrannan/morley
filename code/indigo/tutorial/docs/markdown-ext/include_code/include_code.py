# SPDX-FileCopyrightText: 2020 Tocqueville Group
#
# SPDX-License-Identifier: LicenseRef-MIT-TQ

import re
import urllib
from pathlib import Path
from codecs import open
from markdown.extensions import Extension
from markdown.preprocessors import Preprocessor

SYNTAX = re.compile(
    r"(\s{4})*\{!\s*([a-z]+)\s(([0-9]+|\*)(-([0-9]+|\*))?\s)+([0-9A-Za-z./]+)\s*!\}"
)


class MarkdownIncludeCode(Extension):
    def __init__(self, configs={}):
        self.config = {
            "base_path": [".", "Files location to be checked for relative paths."],
            "encoding": ["utf-8", "Encoding of the included files."],
            "base_url": [
                "",
                "Base URL for relative paths file link, no link if empty.",
            ],
        }
        for key, value in configs.items():
            self.setConfig(key, value)

    def extendMarkdown(self, md, md_globals):
        md.preprocessors.add(
            "include_code", IncCodePrep(md, self.getConfigs()), "_begin"
        )


class IncCodePrep(Preprocessor):
    def __init__(self, md, config):
        super(IncCodePrep, self).__init__(md)
        self.base_path = Path(config["base_path"]).resolve()
        self.encoding = config["encoding"]
        self.base_url = config["base_url"]

    def run(self, lines):
        result = []
        for line in lines:
            match = SYNTAX.match(line)
            if match:
                indent_size = max(match.end(1), 0)
                base_indent = " " * indent_size
                block_indent = " " * (indent_size + 4)
                language = match.group(2)
                filepath = Path(match.group(7))

                ranges_start = match.end(2) + 1
                ranges_end = match.start(7) - 1
                ranges_def = line[ranges_start:ranges_end].split(" ")

                result.append(
                    base_indent
                    + '<div class="codehilite-source">Source file: '
                    + self.source_link(filepath)
                    + "</div>"
                )
                result.append("")

                filepath = filepath.expanduser()
                if not filepath.is_absolute():
                    filepath = self.base_path / filepath

                file_lines = []
                with open(filepath, "r", encoding=self.encoding) as file:
                    file_lines = [line.rstrip("\n") for line in file]

                code_block = []
                code_block.append(":::" + language)
                for range_def in ranges_def:
                    code_block.append("")
                    if "-" in range_def:
                        [start, end] = range_def.split("-")
                        if start == "*" and end == "*":
                            code_block.extend(file_lines)
                        elif start == "*":
                            code_block.extend(file_lines[: int(end)])
                        elif end == "*":
                            code_block.extend(file_lines[int(start) - 1 :])
                        else:
                            code_block.extend(file_lines[int(start) - 1 : int(end)])
                    elif range_def == "*":
                        code_block.extend(file_lines)
                    else:
                        file_line = file_lines[int(range_def) - 1]
                        code_block.append(file_line)

                result.extend([block_indent + cl for cl in code_block])
            else:
                result.append(line)
        return result

    def source_link(self, filepath):
        result = str(filepath)
        if self.base_url != "" and not filepath.is_absolute():
            url = urllib.parse.urljoin(self.base_url, "/".join(filepath.parts))
            result = '<a href="' + url + '">' + result + "</a>"
        return result


def makeExtension(*args, **kwargs):
    return MarkdownIncludeCode(kwargs)
