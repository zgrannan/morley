# Indigo eDSL

[![Hackage](https://img.shields.io/hackage/v/indigo.svg)](https://hackage.haskell.org/package/indigo)

Indigo eDSL is a high level language for Michelson contract development.
It is meant first and foremost to free you from the burden of manual stack
management and supports common features of imperative languages.

It is built on top of `Lorentz`, which in turn is built on top of `Morley`, a
dialect that is a superset of vanilla Michelson.
If you are interested more in the relationships between these projects you can
start by taking a look at [the repo on GitLab](https://gitlab.com/morley-framework/morley).

## Overall idea

Michelson contracts are stack-based and often follow the repetitive pattern of
copying the required values on top of the stack to apply an instruction to them.
Indigo can associate variables to values in the stack and it's able to refer to
them in order to handle this pattern automatically.

In addition to this it can override existing variables with new values, allowing
the manipulation of the stack to be automatic.

Leveraging this ability, it also supports features such as: imperative statements
(`if`, `while`, ...), expressions, operators (`+`, `||`, `==`, ...), scoped
functions definition and errors.

Indigo uses Morley to map values from Michelson to Haskell and to compile to
Michelson code (or to any of the other projects in the chain mentioned above).

## Tutorial and documentation

Indigo has a tutorial that you can find at [indigo-lang.gitlab.io](https://indigo-lang.gitlab.io/).

In addition, as for the other Morley projects, it has [Haddock documentation](https://hackage.haskell.org/package/indigo).
