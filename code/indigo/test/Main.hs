-- SPDX-FileCopyrightText: 2020 Tocqueville Group
--
-- SPDX-License-Identifier: LicenseRef-MIT-TQ

module Main
  ( main
  ) where

import Prelude
import Test.Tasty (defaultMainWithIngredients)

import Cleveland.Ingredients (ourIngredients)
import Test.Util.Golden (regenerateTests)
import Tree (tests)

main :: IO ()
main = tests >>= (defaultMainWithIngredients $ regenerateTests:ourIngredients)
