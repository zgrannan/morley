Unreleased
==========
<!-- Append new entries here -->
* [!542](https://gitlab.com/morley-framework/morley/-/merge_requests/542)
  Use `#=` as a synonym for `//->`.

0.1.0.0
=======

Initial release.
